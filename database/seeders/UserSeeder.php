<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\Permission;
use App\Models\Role;
use DB;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
            'name' => 'admin',
            'user_number'=>'9032785653',
            'email' => 'admin@admin.com',
            'password' => bcrypt('admin')
        ]);

        Permission::insert([
          [
            'name'=>'permission create',
            'slug'=>'permission-create',
            'description'=>'Perrmission create'
        ],
        [
            'name'=>'permission edit',
            'slug'=>'permission-edit',
            'description'=>'Perrmission edit'
        ],
        [
            'name'=>'permission show',
            'slug'=>'permission-show',
            'description'=>'Perrmission show'
        ],
        [
            'name'=>'permission delete',
            'slug'=>'permission-delete',
            'description'=>'Perrmission delete'
        ],
        [
            'name'=>'role create',
            'slug'=>'role-create',
            'description'=>'Role create'
        ],
        [
            'name'=>'role edit',
            'slug'=>'role-edit',
            'description'=>'Role edit'
        ],
        [
            'name'=>'role delete',
            'slug'=>'role-delete',
            'description'=>'Role delete'
        ],
        [
            'name'=>'role show',
            'slug'=>'role-show',
            'description'=>'Role show'
        ],
        [
            'name'=>'role permission create',
            'slug'=>'role-permission-create',
            'description'=>'Role Permission create'
        ],
         [
            'name'=>'role permission edit',
            'slug'=>'role-permission-edit',
            'description'=>'Role Permission edit'
        ],
         [
            'name'=>'role permission show',
            'slug'=>'role-permission-show',
            'description'=>'Role Permission show'
        ],
         [
            'name'=>'role permission delete',
            'slug'=>'role-permission-delete',
            'description'=>'Role Permission delete'
        ],
         [
            'name'=>'user role create',
            'slug'=>'user-role-create',
            'description'=>'user role create'
        ],
        [
            'name'=>'user role edit',
            'slug'=>'user-role-edit',
            'description'=>'user role edit'
        ],
        [
            'name'=>'user role show',
            'slug'=>'user-role-show',
            'description'=>'user role show'
        ],
        [
            'name'=>'user role delete',
            'slug'=>'user-role-delete',
            'description'=>'user role delete'
        ],
        [
            'name'=>'user create',
            'slug'=>'user-create',
            'description'=>'user create'
        ],
        [
            'name'=>'user edit',
            'slug'=>'user-edit',
            'description'=>'user edit'
        ],
        [
            'name'=>'user delete',
            'slug'=>'user-delete',
            'description'=>'user delete'
        ],
        [
            'name'=>'user show',
            'slug'=>'user-show',
            'description'=>'user show'
        ],
        [
            'name'=>'village create',
            'slug'=>'village-create',
            'description'=>'Village create'
        ],
        [
            'name'=>'village edit',
            'slug'=>'village-edit',
            'description'=>'Village edit'
        ],
        [
            'name'=>'village show',
            'slug'=>'village-show',
            'description'=>'Village show'
        ],
        [
            'name'=>'village delete',
            'slug'=>'village-delete',
            'description'=>'Village delete'
        ],
        [
        	'name'=>'sector create',
        	'slug'=>'sector-create',
        	'description'=>'Sector create'
        ],
        [
        	'name'=>'sector edit',
        	'slug'=>'sector-edit',
        	'description'=>'Sector edit'

        ],
        [
        	'name'=>'sector show',
        	'slug'=>'sector-show',
        	'description'=>'Sector show'
        ],
        [
        	'name'=>'sector delete',
        	'slug'=>'sector-delete',
        	'description'=>'Sector delete'

        ],
        [
        	'name'=>'subsector create',
        	'slug'=>'subsector-create',
        	'description'=>'Subsector create'
        ],
        [
        	'name'=>'subsector edit',
        	'slug'=>'subsector-edit',
        	'description'=>'Subsector edit'
        ],
        [
        	'name'=>'subsector show',
        	'slug'=>'subsector-show',
        	'description'=>'Subsector show'
        ],
        [
        	'name'=>'subsector delete',
        	'slug'=>'subsector-delete',
        	'description'=>'Subsector delete'
        ],
        [
        	'name'=>'property-type create',
        	'slug'=>'property-type-create',
        	'description'=>'Property-Type create'
        ],
        [
        	'name'=>'property-type edit',
        	'slug'=>'property-type-edit',
        	'description'=>'Property-Type edit'
        ],
        [
        	'name'=>'property-type show',
        	'slug'=>'property-type-show',
        	'description'=>'Property-Type show'
        ],
        [
        	'name'=>'property-type delete',
        	'slug'=>'property-type-delete',
        	'description'=>'Property-Type delete'
        ],
        [
        	'name'=>'property-details create',
        	'slug'=>'property-details-create',
        	'description'=>'Property-Details create'
        ],
        [
        	'name'=>'property-details edit',
        	'slug'=>'property-details-edit',
        	'description'=>'Property-Details edit'
        ],
        [
        	'name'=>'property-details show',
        	'slug'=>'property-details-show',
        	'description'=>'Property-Details show'
        ],

        [
        	'name'=>'property-details delete',
        	'slug'=>'property-details-delete',
        	'description'=>'Property-Details delete'
        ],
        [
            'name'=>'user village create',
            'slug'=>'user village-create',
            'description'=>'User village create'

        ],
        [
            'name'=>'user village edit',
            'slug'=>'user village-edit',
            'description'=>'User village edit'
        ],
        [
            'name'=>'user village show',
            'slug'=>'user village-show',
            'description'=>'User village show'
        ],
        [
            'name'=>'user village delete',
            'slug'=>'user village-delete',
            'description'=>'User village delete'
        ]
      ]);

        Role::insert([
            'name'=>'Super Admin',
            'slug'=>'super-admin',
            'description'=>'it has all permissions'
        ]);

        DB::table('roles_permissions')->insert([
          ['role_id' => 1, 'permission_id' => 1],
          ['role_id' => 1, 'permission_id' => 2],
          ['role_id' => 1, 'permission_id' => 3],
          ['role_id' => 1, 'permission_id' => 4],
          ['role_id' => 1, 'permission_id' => 5],
          ['role_id' => 1, 'permission_id' => 6],
          ['role_id' => 1, 'permission_id' => 7],
          ['role_id' => 1, 'permission_id' => 8],
          ['role_id' => 1, 'permission_id' => 9],
          ['role_id' => 1, 'permission_id' => 10],
          ['role_id' => 1, 'permission_id' => 11],
          ['role_id' => 1, 'permission_id' => 12],
          ['role_id' => 1, 'permission_id' => 13],
          ['role_id' => 1, 'permission_id' => 14],
          ['role_id' => 1, 'permission_id' => 15],
          ['role_id' => 1, 'permission_id' => 16],
          ['role_id' => 1, 'permission_id' => 17],
          ['role_id' => 1, 'permission_id' => 18],
          ['role_id' => 1, 'permission_id' => 19],
          ['role_id' => 1, 'permission_id' => 20],
          ['role_id' => 1, 'permission_id' => 21],
          ['role_id' => 1, 'permission_id' => 22],
          ['role_id' => 1, 'permission_id' => 23],
          ['role_id' => 1, 'permission_id' => 24],
          ['role_id' => 1, 'permission_id' => 25],
          ['role_id' => 1, 'permission_id' => 26],
          ['role_id' => 1, 'permission_id' => 27],
          ['role_id' => 1, 'permission_id' => 28],
          ['role_id' => 1, 'permission_id' => 29],
          ['role_id' => 1, 'permission_id' => 30],
          ['role_id' => 1, 'permission_id' => 31],
          ['role_id' => 1, 'permission_id' => 32],
          ['role_id' => 1, 'permission_id' => 33],
          ['role_id' => 1, 'permission_id' => 34],
          ['role_id' => 1, 'permission_id' => 35],
          ['role_id' => 1, 'permission_id' => 36],
          ['role_id' => 1, 'permission_id' => 37],
          ['role_id' => 1, 'permission_id' => 38],
          ['role_id' => 1, 'permission_id' => 39],
          ['role_id' => 1, 'permission_id' => 40],
          ]);

          DB::table('users_roles')->insert([
            'user_id'=> 1,'role_id'=> 1
          ]);



    }   
}

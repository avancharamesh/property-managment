<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserSubsectorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_subsectors', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id');
             $table->unsignedBigInteger('subsector_id');

              //FOREIGN KEY CONSTRAINTS
             $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
             $table->foreign('subsector_id')->references('id')->on('sub_sectors')->onDelete('cascade');

             //SETTING THE PRIMARY KEYS
             $table->primary(['user_id','subsector_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_subsectors');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVillageSectorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('village_sectors', function (Blueprint $table) {
            $table->unsignedBigInteger('village_id');
             $table->unsignedBigInteger('sector_id');

             //FOREIGN KEY CONSTRAINTS
             $table->foreign('village_id')->references('id')->on('villages')->onDelete('cascade');
             $table->foreign('sector_id')->references('id')->on('sectors')->onDelete('cascade');

             //SETTING THE PRIMARY KEYS
             $table->primary(['village_id','sector_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('village_sectors');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertyDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_details', function (Blueprint $table) {
            $table->id();
             $table->unsignedBigInteger('village_id'); 
            $table->unsignedBigInteger('sector_id');
            $table->unsignedBigInteger('subsector_id')->default(0);
            $table->unsignedBigInteger('property_type_id');
            $table->String('name');
            $table->String('address');
            $table->String('contact');
            $table->String('document_type')->nullable();
            $table->String('document')->nullable();
            $table->String('link1')->nullable();
            $table->String('link2')->nullable();
            $table->String('link3')->nullable();
            $table->String('link4')->nullable();
            $table->String('uploaded_documents')->nullable();
            $table->String('survey_number')->nullable();
            $table->String('document_number')->nullable();
            $table->String('custodian_property')->nullable();
            $table->String('year_of_purchased')->nullable();
            $table->String('sold_year')->nullable();
            $table->tinyInteger('is_document')->default(1);
            $table->tinyInteger('is_litigated')->default(1); 
            $table->tinyInteger('status')->default(1)->comment('1-Available,0-NotAvailable');
            $table->tinyInteger('is_deleted')->default(0)->comment('1-deleted,0-Notdeleted');
            $table->timestamps();
            $table->foreign('village_id')->references('id')->on('villages')->onDelete('cascade');
            $table->foreign('sector_id')->references('id')->on('sectors')->onDelete('cascade');
            
            $table->foreign('property_type_id')->references('id')->on('property_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_details');
    }
}

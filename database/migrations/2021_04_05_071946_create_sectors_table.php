<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSectorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sectors', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('village_id'); 
            $table->String('name');
            $table->String('description');
            $table->String('image')->nullable();
            $table->tinyInteger('status')->default(1)->comment('1-Available,0-NotAvailable');
            $table->tinyInteger('is_deleted')->default(0)->comment('1-deleted,0-Notdeleted');
            $table->timestamps();
            $table->foreign('village_id')->references('id')->on('villages')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sectors');
    }
}

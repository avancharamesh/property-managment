<?php

use Illuminate\Support\Facades\Route;

/****   Admin Controllers  */
use App\Http\Controllers\admin\LoginController;
use App\Http\Controllers\admin\DashboardController;
use App\Http\Controllers\admin\UserController;
use App\Http\Controllers\admin\PermissionController;
use App\Http\Controllers\admin\RoleController;
use App\Http\Controllers\admin\RolePermissionController;
use App\Http\Controllers\admin\UserRoleController;
use App\Http\Controllers\admin\PlanController;
use App\Http\Controllers\admin\ResumeSampleController;
use App\Http\Controllers\admin\BlogController;
use App\Http\Controllers\admin\VillageController;
use App\Http\Controllers\admin\SectorController;
use App\Http\Controllers\admin\SubSectorController;
use App\Http\Controllers\admin\PropertyTypeController;
use App\Http\Controllers\admin\PropertyDetailsController;
use App\Http\Controllers\admin\DocumentTypeController;
use App\Http\Controllers\admin\UserVillageController;
use App\Http\Controllers\admin\LabelController;
use App\Http\Controllers\admin\VillageSectorController;
use App\Http\Controllers\admin\UserSectorController;
use App\Http\Controllers\admin\UserPropertyController;
use App\Http\Controllers\admin\UserSubsectorController;
/***  Admin Controllers End */

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Admin 14:12:2020

Route::get('/admin', function () {
    return redirect('/admin/login');
});

Route::get('admin/login',[LoginController::class,'index']);
Route::post('admin/login',[LoginController::class,'login']);
Route::get('admin/logout',[LoginController::class,'logout']);
Route::middleware(['auth'])->group(function () {
   Route::get('admin/dashboard',[DashboardController::class,'index']);
   Route::get('admin/change-profile',[LoginController::class,'profile']);
   Route::post('admin/change-profile',[LoginController::class,'updateProfile']);
   Route::get('admin/change-password',[LoginController::class,'changePassword']);
   Route::post('admin/change-password',[LoginController::class,'updatePassword']);

   // Users
   Route::get('admin/user-list',[UserController::class,'index']);
   Route::get('admin/users-list',[UserController::class,'list']);
   Route::get('admin/user-create',[UserController::class,'create']);
   Route::post('admin/user-create',[UserController::class,'store']);
   Route::get('admin/user-show/{id}',[UserController::class,'show'])->name('admin.user.show');
   Route::get('admin/user-edit/{id}',[UserController::class,'edit'])->name('admin.user.edit');
   Route::post('admin/user-update/{id}',[UserController::class,'update']);
   Route::get('admin/user-delete/{id}',[UserController::class,'delete'])->name('admin.user.delete');

   // Permission
    Route::get('admin/permission-list',[PermissionController::class,'list']);
    Route::get('admin/permissions-list',[PermissionController::class,'lists']);
    Route::get('admin/permission-create',[PermissionController::class,'add']);
    Route::post('admin/permission-create',[PermissionController::class,'store']);
    Route::get('admin/permission-edit/{id}',[PermissionController::class,'edit'])->name('admin.permission.edit');
    Route::post('admin/permission-update/{id}',[PermissionController::class,'update']);
    Route::get('admin/permission-show/{id}',[PermissionController::class,'show'])->name('admin.permission.show');
    Route::get('admin/permission-delete/{id}',[PermissionController::class,'delete'])->name('admin.permission.delete');

    // Roles
    Route::get('admin/role-list',[RoleController::class,'list']);
    Route::get('admin/roles-list',[RoleController::class,'lists']);
    Route::get('admin/role-create',[RoleController::class,'add']);
    Route::post('admin/role-create',[RoleController::class,'store']);
    Route::get('admin/role-edit/{id}',[RoleController::class,'edit'])->name('admin.role.edit');
    Route::post('admin/role-update/{id}',[RoleController::class,'update']);
    Route::get('admin/role-show/{id}',[RoleController::class,'show'])->name('admin.role.show');
    Route::get('admin/role-delete/{id}',[RoleController::class,'delete'])->name('admin.role.delete');

    // Role Permissions
    Route::get('admin/role-permission-list',[RolePermissionController::class,'list']);
    Route::get('admin/role-permission-create',[RolePermissionController::class,'add']);
    Route::post('admin/role-permission-create',[RolePermissionController::class,'store']);
    Route::get('admin/role-permission-edit/{id}',[RolePermissionController::class,'edit'])->name('admin.role-permission.edit');
    Route::post('admin/role-permission-update/{id}',[RolePermissionController::class,'update']);
    Route::get('admin/role-permission-show/{id}',[RolePermissionController::class,'show'])->name('admin.role-permission-show');
    Route::get('admin/role-permission-delete/{id}',[RolePermissionController::class,'delete'])->name('admin.role-permission.delete');

    //User Village permission
    Route::get('admin/user-village-list',[UserVillageController::class,'index']);
    Route::get('admin/user-village-create',[UserVillageController::class,'create']);
    Route::post('admin/user-village-store',[UserVillageController::class,'store']);
    Route::get('admin/user-village-edit/{id}',[UserVillageController::class,'edit']);
    Route::post('admin/user-village-update/{id}',[UserVillageController::class,'update']);
    Route::get('admin/user-village-show/{id}',[UserVillageController::class,'show']);

    //User Sector Permission

    Route::get('admin/user-sector-list',[UserSectorController::class,'index']);
    Route::get('admin/user-sector-create',[UserSectorController::class,'create']);
    Route::get('admin/user-sector-list/{id}',[UserSectorController::class,'UserCreateList']);
    Route::post('admin/user-sector-store',[UserSectorController::class,'store']);
    Route::get('admin/user-sector-edit/{id}',[UserSectorController::class,'edit']);
    Route::post('admin/user-sector-update/{id}',[UserSectorController::class,'update']);
    Route::get('admin/user-sector-show/{id}',[UserSectorController::class,'show']);


    //User SubSector Permission 

    Route::get('admin/user-subsector-list',[UserSubsectorController::class,'index']);
    Route::get('admin/user-subsector-create',[UserSubsectorController::class,'create']);
    Route::get('admin/user/create-subsector/list/{id}',[UserSubsectorController::class,'UserSubCreateList']);
    Route::post('admin/user-subsector-store',[UserSubsectorController::class,'store']);
    Route::get('admin/user-subsector-edit/{id}',[UserSubsectorController::class,'edit']);
    Route::post('admin/user-subsector-update/{id}',[UserSubsectorController::class,'update']);
    Route::get('admin/user-subsector-show/{id}',[UserSubsectorController::class,'show']);

    //User PropertyType Permission 
    Route::get('admin/user-properties-list',[UserPropertyController::class,'index']);
    Route::get('admin/user-properties-create',[UserPropertyController::class,'create']);
    Route::get('admin/user/create-property/list/{id}',[UserPropertyController::class,'UserPropertyList']);
    Route::post('admin/user-properties-store',[UserPropertyController::class,'store']);
    Route::get('admin/user-properties-edit/{id}',[UserPropertyController::class,'edit']);
    Route::post('admin/user-properties-update/{id}',[UserPropertyController::class,'update']);
    Route::get('admin/user-properties-show/{id}',[UserPropertyController::class,'show']);


    //Village Sector permissions
    Route::get('admin/village-sector-list',[VillageSectorController::class,'index']);
    Route::get('admin/village-sector-create',[VillageSectorController::class,'create']);
    Route::post('admin/village-sector-store',[VillageSectorController::class,'store']);
    Route::get('admin/village-sector-edit/{id}',[VillageSectorController::class,'edit']);
    Route::post('admin/village-sector-update/{id}',[VillageSectorController::class,'update']);
    Route::get('admin/village-sector-show/{id}',[VillageSectorController::class,'show']);

    // User Roles
    Route::get('admin/user-role-list',[UserRoleController::class,'list']);
    Route::get('admin/user-role-create',[UserRoleController::class,'add']);
    Route::post('admin/user-role-create',[UserRoleController::class,'store']);
    Route::get('admin/user-role-edit/{id}',[UserRoleController::class,'edit']);
    Route::post('admin/user-role-update/{id}',[UserRoleController::class,'update']);
    Route::get('admin/user-role-show/{id}',[UserRoleController::class,'show']);
    Route::get('admin/user-role-delete/{id}',[UserRoleController::class,'delete']);


    //Category
    Route::get('admin/village-list',[VillageController::class,'list']);
    Route::get('admin/village-lists',[VillageController::class,'index']);
    Route::get('admin/village-create',[VillageController::class,'create']);
    Route::post('admin/village-store',[VillageController::class,'store']);
    Route::get('admin/village-edit/{id}',[VillageController::class,'edit'])->name('admin.village.edit');
    Route::post('admin/village-update/{id}',[VillageController::class,'update']);
    Route::get('admin/village-show/{id}',[VillageController::class,'show'])->name('admin.village.show');
    Route::get('admin/village-delete/{id}',[VillageController::class,'destroy']);


    //SubCategoryController
    Route::get('admin/sector-list',[SectorController::class,'list']);
    Route::get('admin/sector-lists',[SectorController::class,'index']);
    Route::get('admin/sector-create',[SectorController::class,'create']);
    Route::post('admin/sector-store',[SectorController::class,'store']);
    Route::get('admin/sector-edit/{id}',[SectorController::class,'edit'])->name('admin.sector.edit');
    Route::post('admin/sector-update/{id}',[SectorController::class,'update']);
    Route::get('admin/sector-show/{id}',[SectorController::class,'show'])->name('admin.sector.show');
    Route::get('admin/sector-delete',[SectorController::class,'destroy']);


    //SubSector
    Route::get('admin/subsector-list',[SubSectorController::class,'list']);
    Route::get('admin/subsector-lists',[SubSectorController::class,'index']);
    Route::get('admin/subsector-create',[SubSectorController::class,'create']);
    Route::get('admin/subsector-sector/{id}',[SubSectorController::class,'sector']);
    Route::post('admin/subsector-store',[SubSectorController::class,'store']);
    Route::get('admin/subsector-edit/{id}',[SubSectorController::class,'edit'])->name('admin.subsector.edit');
    Route::post('admin/subsector-update/{id}',[SubSectorController::class,'update']);
    Route::get('admin/subsector-show/{id}',[SubSectorController::class,'show'])->name('admin.subsector.show');
    Route::get('admin/subsector-delete/{id}',[SubSectorController::class,'destroy']);

    //PropertyTypes
    Route::get('admin/property-type-list',[PropertyTypeController::class,'list']);
    Route::get('admin/property-type-lists',[PropertyTypeController::class,'index']);
    Route::get('admin/property-type-create',[PropertyTypeController::class,'create']);
    Route::get('admin/property-type-sector/{id}',[PropertyTypeController::class,'sector']);
    Route::get('admin/property-type-subsector/{id}',[PropertyTypeController::class,'subsector']);
    Route::post('admin/property-type-store',[PropertyTypeController::class,'store']);
    Route::get('admin/property-type-edit/{id}',[PropertyTypeController::class,'edit'])->name('admin.propertytype.edit');
    Route::post('admin/property-type-update/{id}',[PropertyTypeController::class,'update']);
    Route::get('admin/property-type-show/{id}',[PropertyTypeController::class,'show'])->name('admin.propertytype.show');
    Route::get('admin/property-type-delete/{id}',[PropertyTypeController::class,'destroy']);

    //PropertiesDetails
Route::get('admin/properties-details-list/{status?}',[PropertyDetailsController::class,'list']);
Route::get('admin/properties-details-lists/{status?}',[PropertyDetailsController::class,'lists']);
Route::get('admin/properties-details-create',[PropertyDetailsController::class,'create']);
Route::get('admin/properties-details-sector/{id}',[PropertyDetailsController::class,'sector']);
Route::get('admin/properties-details-subsector/{id}',[PropertyDetailsController::class,'propertyDetailSubsector']);
Route::get('admin/properties-details-propertytype/{id}',[PropertyDetailsController::class,'propertytype']);
Route::post('admin/properties-details-store',[PropertyDetailsController::class,'store']);
Route::get('admin/properties-details-edit/{id}',[PropertyDetailsController::class,'edit'])->name('admin.propertiesdetails.edit');
Route::get('admin/properties-details-document/{data}/{id}',[PropertyDetailsController::class,'propertyDocument']);
Route::get('admin/document-remove/{name}/{id}',[PropertyDetailsController::class,'documentDelete']);
Route::post('admin/properties-details-update/{id}',[PropertyDetailsController::class,'update']);
Route::get('admin/properties-details-show/{id}',[PropertyDetailsController::class,'show'])->name('admin.propertiesdetails.show');
Route::get('admin/properties-details-delete/{id}',[PropertyDetailsController::class,'destroy'])->name('admin.propertiesdetails.delete');

    //Documents
Route::get('admin/document-type-list',[DocumentTypeController::class,'list']);
Route::get('admin/document-type-lists',[DocumentTypeController::class,'index']);
Route::get('admin/document-type-create',[DocumentTypeController::class,'create']);
Route::post('admin/document-type-store',[DocumentTypeController::class,'store']);
Route::get('admin/document-type-edit/{id}',[DocumentTypeController::class,'edit'])->name('admin.documenttype.edit');
Route::post('admin/document-type-update/{id}',[DocumentTypeController::class,'update']);
Route::get('admin/document-type-show/{id}',[DocumentTypeController::class,'show'])->name('admin.documenttype.show');
Route::get('admin/document-type-delete/{id}',[DocumentTypeController::class,'destroy'])->name('admin.documenttype.delete');
Route::get('admin/reports-list/{id}',[ReportsController::class,'list']);
Route::get('admin/reports-lists',[ReportsController::class,'lists']);

Route::get('admin/label-list',[LabelController::class,'label']);
Route::get('admin/label-create',[LabelController::class,'create']);
Route::post('admin/label-store',[LabelController::class,'store']);
Route::get('admin/label-edit/{id}',[LabelController::class,'edit'])->name('label.edit');
Route::post('admin/label-update/{id}',[LabelController::class,'update']);
Route::get('admin/label-delete/{id}',[LabelController::class,'delete'])->name('label.del');

Route::get('admin/documents/{id}',[PropertyDetailsController::class,'ZipFile']);

Route::get('admin/propertiesdetails-pdf/{id}',[PropertyDetailsController::class,'genratePdf'])->name('admin.propertiesdetails.property_pdf');
});

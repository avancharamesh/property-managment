<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Permission;
use App\Models\Role;

class RolePermissionController extends Controller
{
    public function list(Request $request){
        if($request->user()->can('role-permission-show')){
        $data = [];
        $roles = DB::table('roles_permissions')
                 ->select('roles_permissions.role_id','roles.name')
                 ->join('roles','roles.id','=','roles_permissions.role_id')
                 ->distinct('roles_permissions.role_id')
                 ->get();
        foreach($roles as $role){
            $permission = DB::table('roles_permissions')
                        ->select('permissions.name')
                        ->join('permissions','roles_permissions.permission_id','=','permissions.id')
                        ->where('roles_permissions.role_id','=',$role->role_id)
                        ->get();
            $temp_data = [
                           "role_name" => $role->name,
                           "role_id"   => $role->role_id,
                           "permission" => $permission
                         ];
            array_push($data, $temp_data);
        }
        return view('admin.role-permission.list',compact('data'));
    }
}

    public function add(Request $request){
        if($request->user()->can('permission-create')){
        $role = Role::all();
        $permission = Permission::all();
        return view('admin.role-permission.add',compact(['role','permission']));
    }else{
        return "You don't have permission please contact to administrator";
    }
}

    public function store(Request $request){
        $validated = $request->validate([
            'role' => 'required',
            'permission' => 'required',
        ]);

        $role = $request->post('role');
        $permission = $request->post('permission');
        if(DB::table('roles_permissions')->where('role_id','=',$role)->count() > 0){
            return redirect()->back()->with('error', 'Role Already exists.'); 
        }else{
            foreach($permission as $perm){
                $roleObject = Role::find($role);
                $roleObject->permissions()->attach($perm);
            }
        }   
        return redirect('admin/role-permission-list')->with('success','Role Permission created Successfully!');
    }

    public function edit(Request $request,$id){
        if($request->user()->can('role-permission-edit')){
        $role = DB::table('roles')->where('id','=',$id)->get();
        $permission = Permission::all();
        $permissionAssign = DB::table('roles_permissions')->where('role_id','=',$id)->get();
        $permissionAssignId = [];
        foreach ($permissionAssign as $pa) {
            array_push($permissionAssignId,$pa->permission_id);
        }
        return view('admin.role-permission.edit',compact(['role','permission','permissionAssignId']));
    }
}
    public function update($id,Request $request){
		$validated = $request->validate([
            'role' => 'required',
            'permission' => 'required',
        ]);
        
        $roleObject = Role::find($request->post('role'));
        $permission = $request->post('permission');
        $AssignedPermission = DB::table('roles_permissions')->where('role_id','=',$id)->get();
        $permissionAssignId = [];
        foreach ($AssignedPermission as $ap) {
            array_push($permissionAssignId,$ap->permission_id);
        }
        $permissionForDetach = array_diff($permissionAssignId,$permission);
        $permissionForAttach = array_diff($permission,$permissionAssignId);

        if(!empty($permissionForAttach)){
            $roleObject->permissions()->attach($permissionForAttach);
        }
        if(!empty($permissionForDetach)){
            $roleObject->permissions()->detach($permissionForDetach);
        }
        
        return redirect('admin/role-permission-list')->with('success','Role Permission updated Successfully!');
	}

    public function show(Request $request,$id){
        if($request->user()->can('role-permission-show')){
        $role = DB::table('roles')->where('id','=',$id)->get();
        $permission = Permission::all();
        $permissionAssign = DB::table('roles_permissions')->where('role_id','=',$id)->get();
        $permissionAssignId = [];
        foreach ($permissionAssign as $pa) {
            array_push($permissionAssignId,$pa->permission_id);
        }
        return view('admin.role-permission.show',compact('role','permission','permissionAssignId'));
    }else{
        return "You don't have permission please contact to administrator";
    }

    //  Delete
    // public function delete($id){
    //     $user = Role::find($id)->delete();
    //     return redirect('admin/role-permission-list')->with('success','Role Deleted Successfully!');
    // }
}
}
<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Village;
use App\Models\Sector;
use App\Models\SubSector;
use App\Models\PropertyType;
use App\Models\PropertyDetails;
use DB;
use DataTables;

class ReportsController extends Controller
{
    public function list(){

        print_r('expression');die;
        $user_id=Auth::User()->id;
     //    $village=DB::select('SELECT v.name,v.status,v.description,u.name as username from villages as v left join user_villages as uv on uv.village_id=v.id left join users as u on uv.user_id=u.id where user_id=$user_id');
     //    print_r($village);die;
    	// return view('admin.reports.list');
    }
    public function lists()
    {
    	if ($request->ajax()) {

               $data = DB::select('SELECT pd.id,pd.name,pd.address,pd.contact, pd.status,pd.is_document,pd.is_litigated,pd.document_type,pd.document,vlg.name as village,sec.name as sector,p.name as property_type,sub.name as subsector from property_types as p LEFT JOIN villages as vlg on vlg.id = p.village_id left join sectors as sec on sec.id=p.sector_id left join property_details as pd on p.id=pd.property_type_id left join sub_sectors as sub on sub.id=pd.subsector_id left join user_villages as uv on uv.village_id=vlg.id left join users as u on uv.user_id=u.id WHERE pd.is_deleted = 0');
           

           
                return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('status',function($row){
                        $sts = ($row->status == 1)?'<span class="badge badge-success bg-green">Active</span>':'<span class="badge badge-danger">In-Active</span>';
                        return $sts;
                    })
                    ->addColumn('is_document',function($row){
                        $document = ($row->is_document==1)?'<span class="badge badge-success bg-green">Yes</span>':'<span class="badge badge-danger">No</span>';
                        return $document;
                    })
                   
                    ->addColumn('is_litigated',function($row){
                            $litigate = ($row->is_litigated==1)?'<span class="badge badge-danger">Yes</span>':'<span class="badge badge-success bg-green">No</span>';
                            return $litigate;
                    })

                     ->addColumn('document',function($row){
                        $document = "<img src='/uploads/property_documents/".$row->document."' height='40px' width='40px'>";
                        return $document;
                    })
                    
                    ->addColumn('action', function($row){
     
                           $btn = " <div class='btn-group'>
                           <a href='". route('admin.propertiesdetails.edit', [$row->id])."' class='bg-green btn-sm'><i class='fa fa-pencil' aria-hidden='true'></i></a>
                           <a href='". route('admin.propertiesdetails.show', [$row->id])."' class='bg-green btn-sm ml-1'><i class='fa fa-eye' aria-hidden='true'></i></a>
                           <a href='". route('admin.propertiesdetails.delete', [$row->id])."' onclick='return confirm(\"Are you sure you want to delete this item?\");' class='bg-green btn-sm ml-1'><i class='fa fa-trash' aria-hidden='true'></i></a>
                           </div>";
       
                            return $btn;
                    })
                    ->rawColumns(['action','status','is_document','is_litigated','document'])
                    ->make(true);
        }
    }
}

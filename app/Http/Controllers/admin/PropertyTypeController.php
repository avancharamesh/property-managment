<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Village;
use App\Models\SubSector;
use App\Models\Sector;
use App\Models\PropertyType;
use DB;
use validate;
use DataTables;
use Auth;
class PropertyTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        
        return view('admin.propertytype.list');
        
    }
    public function index(Request $request)
    {
        if ($request->ajax()) {
             $data = DB::select('SELECT pt.id,pt.name,pt.description,pt.image, pt.status,vlg.name as village,sec.name as sector,sub.name as subsector from property_types as pt LEFT JOIN   villages as vlg  on vlg.id = pt.village_id left join sectors as sec on sec.id=pt.sector_id left join sub_sectors as sub on sub.id=pt.subsector_id  WHERE  pt.is_deleted = 0');
           

           
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('status',function($row){
                        $sts = ($row->status == 1)?'<span class="badge badge-success bg-green">Active</span>':'<span class="badge badge-danger">In-Active</span>';
                        return $sts;
                    })
                    ->addColumn('image',function($row){
                        $image = "<img src='".asset('uploads/properties').'/'.$row->image."' height='40px' width='40px'>";
                        return $image;
                    })
                    ->addColumn('action', function($row){
     
                           $btn = "<div class='btn-group'>
                           <a href='". route('admin.propertytype.edit', [$row->id])."' class='bg-green btn-sm'><i class='fa fa-pencil' aria-hidden='true'></i></a>
                           <a href='". route('admin.propertytype.show', [$row->id])."' class='bg-green btn-sm ml-1'><i class='fa fa-eye' aria-hidden='true'></i></a></div>";
       
                            return $btn;
                    })
                    ->rawColumns(['action','status','image'])
                    ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(Auth::user()->hasRole('super-admin')){
            $data['village'] = DB::table('villages')->where(['is_deleted'=>0])->get();
            $getdata['sector'] = Sector::where(['is_deleted'=>0])->get();
            $getdatas['sub_sector'] = SubSector::where(['is_deleted'=>0])->get();
            return view('admin.propertytype.create',$data,$getdata,$getdatas);
        }else{
        $id=Auth::User()->id;
        $data['village'] = DB::table('villages')->join('user_villages','villages.id','=','user_villages.village_id')->select('villages.*')
         ->where('user_villages.user_id',$id)->where(['is_deleted'=>0])->get();
        $getdata['sector'] = DB::table('sectors')->join('user_sectors','sectors.id','=','user_sectors.sector_id')->select('sectors.*')->where('user_sectors.user_id',$id);
        $getdatas['sub_sector'] = DB::table('sub_sectors')->join('user_subsectors.id','=','user_subsectors.subsector_id')->select('sub_sectors.*')->where('user_subsectors.user_id',$id);
        return view('admin.propertytype.create',$data,$getdata,$getdatas);
    }
        
    }

     public function sector(Request $request,$id)
    {
        if(Auth::user()->hasRole('super-admin')){
            $sector_id = DB::table('sectors')->where(['village_id'=>$id,'is_deleted'=>0])->get();
            return json_encode($sector_id);
        }
        $user_id = Auth::User()->id;
         $sector_id = DB::table('sectors')->join('user_sectors','sectors.id','=','user_sectors.sector_id')->select('sectors.*')->where('user_sectors.user_id',$user_id)
                    ->where("village_id",$id)->get();
        return json_encode($sector_id);
        
    }

    public function subsector(Request $request,$id)
    {
        if(Auth::user()->hasRole('super-admin')){
            $subsector_id = DB::table("sub_sectors")
                    ->where("sector_id",$id)->get();
        return json_encode($subsector_id);
        }else{
                $user_id = Auth::user()->id;
                $subsector_id = DB::table('sub_sectors')->join('user_subsectors','sub_sectors.id','=','user_subsectors.subsector_id')->select('sub_sectors.*')->where('user_subsectors.user_id',$user_id)->where('sector_id',$id)->get();
                return json_encode($subsector_id);
            }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $validateDataField = [ 

         'village_id'=>'required',
         'sector_id'=>'required',
         'subsector_id'=>'required',   
         'name' => 'required',
         'description' => 'required',
         'status' => 'required'

        ];
        if($request->file('image')){
            $validateDataField['image'] = 'required | mimes:jpeg,jpg,bmp,png,gif';
        }
        $request->validate($validateDataField);
        $request->validate($validateDataField);
        $property_type=new PropertyType();
        $property_type->village_id=$request->village_id;
        $property_type->sector_id=$request->sector_id;
        $property_type->subsector_id=$request->subsector_id;
        $property_type->name=$request->name;
        if($request->file('image')){
        $imageFile = time().'.'.$request->file('image')->extension(); 
        $request->file('image')->move(public_path('uploads/properties'), $imageFile);
        $property_type->image = $imageFile;
    }
        $property_type->description=$request->description;
        $property_type->status=$request->status;
        $property_type->save();
        return redirect('admin/property-type-list')->with('success','PropertyType Created Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        
        if(Auth::user()->hasRole('super-admin')){
            $property_type=PropertyType::find($id);
            $village = DB::table('villages')->where(['is_deleted'=>0])->get();
            $sector = Sector::select('*')->where(['is_deleted'=>0])->get();
            $subsector= SubSector::select('*')->where(['is_deleted'=>0])->get();
            return view('admin.propertytype.edit',['property_type'=>$property_type,'village'=>$village,'sector'=>$sector,'subsector'=>$subsector]);
        }else{
       $user_id=Auth::User()->id;
        $property_type=PropertyType::find($id);
        $village = DB::table('villages')->join('user_villages','villages.id','=','user_villages.village_id')->select('villages.*')
         ->where('user_villages.user_id',$user_id)->where(['is_deleted'=>0])->get();
        $sector = DB::table('sectors')->join('user_sectors','sectors.id','=','user_sectors.sector_id')->select('sectors.*')->where('user_sectors.user_id',$user_id);
        $subsector= DB::table('sub_sectors')->join('user_subsectors.id','=','user_subsectors.subsector_id')->select('sub_sectors.*')->where('user_subsectors.user_id',$id);
        return view('admin.propertytype.edit',['property_type'=>$property_type,'village'=>$village,'sector'=>$sector,'subsector'=>$subsector]);
       }
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        if(Auth::user()->hasRole('super-admin')){
            $property_type=PropertyType::find($id);
            $village = DB::table('villages')->where(['is_deleted'=>0])->get();
            $sector = Sector::select('*')->where(['is_deleted'=>0])->get();
            $subsector= SubSector::select('*')->where(['is_deleted'=>0])->get();
            return view('admin.propertytype.edit',['property_type'=>$property_type,'village'=>$village,'sector'=>$sector,'subsector'=>$subsector]);
        }else{
       $user_id=Auth::User()->id;
        $property_type=PropertyType::find($id);
        $village = DB::table('villages')->join('user_villages','villages.id','=','user_villages.village_id')->select('villages.*')
         ->where('user_villages.user_id',$user_id)->where(['is_deleted'=>0])->get();
        $sector = DB::table('sectors')->leftjoin('user_sectors','sectors.id','=','user_sectors.sector_id')->select('sectors.*','user_sectors.*')->where('user_sectors.user_id',$user_id)->get();
        $subsector= DB::table('sub_sectors')->leftjoin('user_subsectors','sub_sectors.id','=','user_subsectors.subsector_id')->select('sub_sectors.*','sub_sectors.id as id')->where('user_subsectors.user_id',$user_id)->get();
        return view('admin.propertytype.edit',['property_type'=>$property_type,'village'=>$village,'sector'=>$sector,'subsector'=>$subsector]);
       }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $request->validate([

         'village_id'=>'required',
         'sector_id'=>'required',
         'subsector_id'=>'required',   
         'name' => 'required',
         'image' => 'nullable | mimes:jpeg,jpg,bmp,png,gif',
         'description' => 'required',
         'status' => 'required'

        ]);
        $property_type = PropertyType::find($id);
        $property_type->village_id=$request->village_id;
        $property_type->sector_id=$request->sector_id;
        $property_type->subsector_id=$request->subsector_id;
        $property_type->name=$request->name;
        if($request->file('image')){
        $imageFile = time().'.'.$request->file('image')->extension(); 
        $request->file('image')->move(public_path('uploads/properties'), $imageFile);
        $property_type->image = $imageFile;
    }
        $property_type->description=$request->description;
        $property_type->status=$request->status;
        $property_type->save();
        return redirect('admin/property-type-list')->with('success','PropertyType Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        if($request->user()->can('property-type-delete')){
         $property_type=PropertyType::find($id);
        $property_type->is_deleted = 1;
        $property_type->save();
        return back()->with('success','SubSector Deleted Successfully');
        }else{
            return "You don't have permission please contact to administrator";
        }
    }
}

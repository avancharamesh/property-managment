<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;

use DataTables;

class UserController extends Controller
{
    
    // Load user List View
    public function index(Request $request){
        if($request->user()->can('user-show')){
        return view('admin.user.user-list');
    }else{
        return "You don't have permission please contact to administrator";
    }
}
    public function list(Request $request){
        if ($request->ajax()) {
            $data = User::select('*');
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->editColumn('created_at', function ($request) {
                        if($request->created_at){
                            return $request->created_at->format('Y-m-d h:i:s'); 
                        }else{
                            return $request->created_at;
                        }
                      })
                    ->editColumn('updated_at', function ($request) {
                        if($request->updated_at){
                            return $request->updated_at->format('Y-m-d h:i:s');
                        }else{
                            return $request->updated_at;
                        }
                      })
                    ->addColumn('action', function($row){
     
                           $btn = "<a href='". route('admin.user.edit', [$row->id])."' class='bg-green btn-sm'><i class='fa fa-pencil' aria-hidden='true'></i></a>
                           <a href='". route('admin.user.show', [$row->id])."' class='bg-green btn-sm ml-1'><i class='fa fa-eye' aria-hidden='true'></i></a>
                           <a href='". route('admin.user.delete', [$row->id])."' onclick='return confirm(\"Are you sure you want to delete this item?\");' class='bg-green btn-sm ml-1'><i class='fa fa-trash' aria-hidden='true'></i></a>";
       
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
    }

    // load user add view
    public function create(Request $request){
        if($request->user()->can('user-create')){
        return view('admin.user.user-create');
    }
}

    // User Store
    public function store(Request $request){
        $validated = $request->validate([
            'user_number' => 'required|digits:10|unique:users',
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $user_number = $request->post('user_number');
        $name = $request->post('name');
        $email = $request->post('email');
        $password = $request->post('password');
        if (User::where('email', '=', $email)->count() > 0) {
            return back()->with('error','Email Already Exists!');
        }else{
            $user = User::create([
                'user_number' => $user_number,
                'name' => $name,
                'email' => $email,
                'password' => bcrypt($password)
            ]);
            return redirect('admin/user-list')->with('success','User created Successfully!');

        }
    
    }
    // Edit user View
    public function edit(Request $request,$id){
        if($request->user()->can('user-edit')){
        $user = User::find($id);
        return view('admin.user.user-edit', compact('user'));
    }
}
    public function update($id,Request $request){
        $validated = $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'nullable'
        ]);
        $user = User::find($id);
        $user->name = $request->post('name');
        $user->email = $request->post('email');
        if($request->password){
        $user->password = bcrypt($request->post('password'));
        }
        $user->save();
        return redirect('admin/user-list')->with('success','User updated Successfully!');

    }
    // User Show
    public function show(Request $request,$id){
        if($request->user()->can('user-show')){
        $user = User::find($id);
        return view('admin.user.user-show',compact('user'));
    }else{
        return "You don't have permission to do this please contact to administrator";
    }
}

    // User Delete
    public function delete(Request $request,$id){
        if($request->user()->can('user-delete')){
        $user = User::find($id)->delete();
        return redirect('admin/user-list')->with('success','User Deleted Successfully!');
      }else{
        return "You don't have permission to do this please contact to administrator";
    }
   }
}

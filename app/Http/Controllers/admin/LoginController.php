<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\User;

class LoginController extends Controller
{
    // Load Login View
    public function index(){
        return view('admin.login');
    }
    // Verify login credentials
    public function login(Request $request){
        $validate = $request->validate([
            'user_number' => 'required|min:10',
            'password' => 'required|min:5'
        ]);
        $user_number = $request->post('user_number');
        $password = $request->post('password');
        if (Auth::attempt(['user_number' => $user_number, 'password' => $password])) {
            return redirect('admin/dashboard');
        }else{
            $request->session()->flash('status', 'Wrong Credentials.');
            return redirect('admin/login');
        }
    }

    //Logout
    public function logout(){
        Auth::logout();
        return redirect('admin/login');
    }

    // Profile
    public function profile(){
        return view('admin.change-profile');
    }

    // Update Profile

    public function updateProfile(Request $request){
        $validated = $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'profile_image' =>'nullable | mimes:jpeg,jpg,bmp,png,gif'
        ]);
        

        $user = User::find(Auth::user()->id);
        $user->name = $request->post('name');
        $user->email = $request->post('email');
        if($request->file('profile_image')){
            $fileName = time().'.'.$request->file('profile_image')->extension(); 
            $request->file('profile_image')->move(public_path('uploads/user-profile'), $fileName);
            $user->profile_image = $fileName;
        }
        $user->save();
        return redirect()->back()->with('success','Profile Updated Successfully');
    }

    // Change Password
    public function changePassword(){
        return view('admin.change-password');
    }
    // Update Password
    public function updatePassword(Request $request){
        $validated = $request->validate([
            'password' => 'required'
        ]);
        $user = User::find(Auth::user()->id);
        $user->password = bcrypt($request->post('password'));
        $user->save();
        return redirect()->back()->with('success','Password Updated Successfully');
    }
}

<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Village;
use App\HasUservillagesTrait;
class UserVillageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $user_role = DB::table('user_villages')
                 ->select('user_villages.user_id','users.name')
                 ->join('users','users.id','=','user_villages.user_id')
                 ->distinct('user_villages.user_id')
                 ->get();
        foreach($user_role as $user){
            $permission = DB::table('user_villages')
                        ->select('villages.name')
                        ->join('villages','user_villages.village_id','=','villages.id')
                        ->where('user_villages.user_id','=',$user->user_id)->where('is_deleted',0)
                        ->get();
            $temp_data = [
                           "user_name" => $user->name,
                           "user_id"   => $user->user_id,
                           "permission" => $permission
                         ];
            array_push($data, $temp_data);
        }
        return view('admin.user_villages.list',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
       
         $user = User::all();
        $village = Village::all()->where('is_deleted',0);
        return view('admin.user_villages.create',compact(['user','village']));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         $validated = $request->validate([
            'user' => 'required',
            'village' => 'required',
        ]);

        $user = $request->post('user');
        $village = $request->post('village');
        if(DB::table('user_villages')->where('user_id','=',$user)->count() > 0){
            return redirect()->back()->with('error', 'Role Already exists.'); 
        }else{
            foreach($village as $perm){
               DB::table('user_villages')->insert([
                        'user_id' => $user,
                        'village_id' => $perm
                    ]);
            }
        }   
        return redirect('admin/user-village-list')->with('success','User Village Permission Created Successfully!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        
        $user = DB::table('users')->where('id','=',$id)->get();
        $village = Village::all()->where('is_deleted',0);
        $permissionAssign = DB::table('user_villages')->where('user_id','=',$id)->get();
        $permissionAssignId = [];
        foreach ($permissionAssign as $pa) {
            array_push($permissionAssignId,$pa->village_id);
        }
        return view('admin.user_villages.show',compact('user','village','permissionAssignId'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        
         $user = DB::table('users')->where('id','=',$id)->get();
        $village = Village::all()->where('is_deleted',0);
        $villageAssign = DB::table('user_villages')->where('user_id','=',$id)->get();
        $villageAssignId = [];
        foreach ($villageAssign as $pa) {
            array_push($villageAssignId,$pa->village_id);
        }
        return view('admin.user_villages.edit',compact(['user','village','villageAssignId']));
        
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'user' => 'required',
            
        ]);

        $userObject=User::find($request->post('user'));
        $village=$request->post('village');

        if(!$village){
            DB::table('user_villages')->where('user_id','=',$id)->delete();            
            return redirect('admin/user-village-list')->with('success','User Village Permission Deleted Successfully!');
        }
        
        $AssignedVillage = DB::table('user_villages')->where('user_id','=',$id)->get();
        $villageAssignId = [];
            foreach($AssignedVillage as $vill){
            
                array_push($villageAssignId, $vill->village_id);
            }

            $villageForDetach = array_diff($villageAssignId, $village);
            $villageForAttach = array_diff($village, $villageAssignId);
            // print_r($villageForAttach);
            // dd($villageForDetach);
            if(!empty($villageForAttach)){
                foreach ($villageForAttach as $vill) {   
                  DB::table('user_villages')->insert([
                    "user_id" => $id,
                    "village_id" => $vill
                  ]);
           }
        }

        if(!empty($villageForDetach)){
               DB::table('user_villages')->whereIn('village_id',$villageForDetach)->where('user_id','=',$id)->delete();            
        }

           
        return redirect('admin/user-village-list')->with('success','User Village Permission Updated Successfully!');
    
   }

}



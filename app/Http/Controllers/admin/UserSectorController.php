<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Sector;
use App\HasUservillagesTrait;
use Auth;

class UserSectorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $data = [];
        $user_role = DB::table('user_sectors')
                 ->select('user_sectors.user_id','users.name')
                 ->join('users','users.id','=','user_sectors.user_id')
                 ->distinct('user_sectors.user_id')
                 ->get();
        foreach($user_role as $user){
            $permission = DB::table('user_sectors')
                        ->select('sectors.name')
                        ->join('sectors','user_sectors.sector_id','=','sectors.id')
                        // ->where('user_villages.user_id','=',$user->user_id)
                        ->where('user_sectors.user_id','=',$user->user_id)->where('is_deleted',0)
                        ->get();
            $temp_data = [
                           "user_name" => $user->name,
                           "user_id"   => $user->user_id,
                           "permission" => $permission
                         ];
            array_push($data, $temp_data);
        }
        return view('admin.user_sectors.list',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
         $user = User::all();
       
        return view('admin.user_sectors.create',compact(['user']));
        
    }
    public function UserCreateList($id)
    {

         $sector = Sector::join('villages','villages.id','=','sectors.village_id')
                    ->join('user_villages','user_villages.village_id','=','villages.id')
                    ->join('users','users.id','=','user_villages.user_id')
                    ->select('sectors.*','villages.name as village_name')
                    ->where('users.id',$id)
                    ->where('sectors.is_deleted',0)
                    ->get();
         return json_encode($sector);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $validated = $request->validate([
            'user' => 'required',
            'sector' => 'required',
        ]);

        $user = $request->post('user');
        $sector = $request->post('sector');
        if(DB::table('user_sectors')->where('user_id','=',$user)->count() > 0){
            return redirect()->back()->with('error', 'Role Already exists.'); 
        }else{
            foreach($sector as $perm){
               DB::table('user_sectors')->insert([
                        'user_id' => $user,
                        'sector_id' => $perm
                    ]);
            }
        }   
        return redirect('admin/user-sector-list')->with('success','User Sector Permission Created Successfully!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = DB::table('users')->where('id','=',$id)->get();
       $sector = Sector::join('villages','villages.id','=','sectors.village_id')
                    ->join('user_villages','user_villages.village_id','=','villages.id')
                    ->join('users','users.id','=','user_villages.user_id')
                    ->select('sectors.*','villages.name as village_name')
                    ->where('users.id',$id)
                    ->where('sectors.is_deleted',0)
                    ->get();
        $sectorAssign = DB::table('user_sectors')->where('user_id','=',$id)->get();
        $sectorAssignId = [];
        foreach ($sectorAssign as $pa) {
            array_push($sectorAssignId,$pa->sector_id);
        }
        return view('admin.user_sectors.show',compact('user','sector','sectorAssignId'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         
        $user = DB::table('users')->where('id','=',$id)->get();
        $sector = Sector::join('villages','villages.id','=','sectors.village_id')
                    ->join('user_villages','user_villages.village_id','=','villages.id')
                    ->join('users','users.id','=','user_villages.user_id')
                    ->select('sectors.*','villages.name as village_name')
                    ->where('users.id',$id)
                    ->where('sectors.is_deleted',0)
                    ->get();
        $sectorAssign = DB::table('user_sectors')->where('user_id','=',$id)->get();
        $sectorAssignId = [];
        foreach ($sectorAssign as $pa) {
            array_push($sectorAssignId,$pa->sector_id);
        }
        return view('admin.user_sectors.edit',compact('user','sector','sectorAssignId'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $validated = $request->validate([
            'user' => 'required',
            
        ]);
        $userObject=User::find($request->post('user'));

        $sector=$request->post('sector');

         if(!$sector){
            DB::table('user_sectors')->where('user_id','=',$id)->delete();            
            return redirect('admin/user-sector-list')->with('success','User Sector Permission Deleted Successfully!');
        }

        $AssignedSector = DB::table('user_sectors')->where('user_id','=',$id)->get();
        $sectorAssignId = [];
            foreach($AssignedSector as $sec){
            
                array_push($sectorAssignId, $sec->sector_id);
            }

            $sectorForDetach = array_diff($sectorAssignId, $sector);
            $sectorForAttach = array_diff($sector, $sectorAssignId);
            // print_r($villageForAttach);
            // dd($villageForDetach);
            if(!empty($sectorForAttach)){
                foreach ($sectorForAttach as $sec) {   
                  DB::table('user_sectors')->insert([
                    "user_id" => $id,
                    "sector_id" => $sec
                  ]);
           }
        }

        if(!empty($sectorForDetach)){
               DB::table('user_sectors')->whereIn('sector_id',$sectorForDetach)->where('user_id','=',$id)->delete();            
        }

           
        return redirect('admin/user-sector-list')->with('success','User Sector Permission Updated Successfully!');
    
   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

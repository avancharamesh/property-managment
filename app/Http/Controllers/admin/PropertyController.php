<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use  App\Models\village;
use App\Models\property;
use App\Models\Sector;
use DB;
use validate;
use DataTables;

class PropertyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
          if($request->user()->can('property-type-show')){
        return view('admin.properties.list');
    }else{
        return "You don't have permission please contact to administrator";
    }
    public function index(Request $request)
    {
        if ($request->ajax()) {
             $data = DB::select('SELECT p.id,p.name,p.description,p.image, p.status,vlg.name as village,sec.name as sector from properties as p LEFT JOIN   villages as vlg  on vlg.id = p.village_id left join sectors as sec on sec.id=p.sector_id  WHERE p.is_deleted = 0');
           

           
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('status',function($row){
                        $sts = ($row->status == 1)?'<span class="badge badge-success bg-green">Active</span>':'<span class="badge badge-danger">In-Active</span>';
                        return $sts;
                    })
                    ->addColumn('image',function($row){
                        $image = "<img src='/uploads/properties/".$row->image."' height='40px' width='40px'>";
                        return $image;
                    })
                    ->addColumn('action', function($row){
     
                           $btn = "<a href='/admin/property-type-edit/$row->id' class='bg-green btn-sm'><i class='fa fa-pencil' aria-hidden='true'></i></a>
                           <a href='/admin/property-type-show/$row->id' class='bg-green btn-sm ml-1'><i class='fa fa-eye' aria-hidden='true'></i></a>
                           <a href='/admin/property-type-delete/$row->id' onclick='return confirm(\"Are you sure you want to delete this item?\");' class='bg-green btn-sm ml-1'><i class='fa fa-trash' aria-hidden='true'></i></a>";
       
                            return $btn;
                    })
                    ->rawColumns(['action','status','image'])
                    ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if($request->user()->can('property-type-create')){
        $data['village'] = Village::where(['status'=>1])->get();
        $getdata['sector'] = Sector::where(['status'=>1])->get();
        return view('admin.properties.create',$data,$getdata);
        }else{
            return "You don't have permission please contact to administrator";
        }
    }

    public function sector(Request $request,$id)
    {
        $user_id = Auth::User()->id;
         $sector_id = DB::table('sectors')->join('user_sectors','sectors.id','=','user_sectors.sector_id')->select('sectors.*')->where('user_sectors.user_id',$user_id)
                    ->where("village_id",$id)->get();
        return json_encode($sector_id);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

         'village_id'=>'required',
         'sector_id'=>'required',   
         'name' => 'required',
         'image' => 'required | mimes:jpeg,jpg,bmp,png,gif',
         'description' => 'required',
         'status' => 'required'

        ]);
        $property=new Property();
        $property->village_id=$request->village_id;
        $property->sector_id=$request->sector_id;
        $property->name=$request->name;
        $imageFile = time().'.'.$request->file('image')->extension(); 
        $request->file('image')->move(public_path('uploads/properties'), $imageFile);
        $property->image = $imageFile;
        $property->description=$request->description;
        $property->status=$request->status;
        $property->save();
        return redirect('admin/property-type-list')->with('success','Property created Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        if($request->user()->can('property-type-show')){
        $properties=Property::find($id);
        $village = Village::select('*')->where(['status'=>1])->get();
        $sector = Sector::select('*')->where(['status'=>1])->get();
        return view('admin.properties.show',['properties'=>$properties,'village'=>$village,'sector'=>$sector]);
      }else{
        return "You don't have permission please contact to administrator";
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        if($request->user()->can('property-type-edit')){
        $properties=Property::find($id);
        $village = Village::select('*')->where(['status'=>1])->get();
        $sector = Sector::select('*')->where(['status'=>1])->get();
        return view('admin.properties.edit',['properties'=>$properties,'village'=>$village,'sector'=>$sector]);
        }else{
            return "You don't have permission please contact to administrator";
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

         'village_id'=>'required',
         'sector_id'=>'required',   
         'name' => 'required',
         'image' => 'nullable | mimes:jpeg,jpg,bmp,png,gif',
         'description' => 'required',
         'status' => 'required'

        ]);

        $property=Property::find($id);
        $property->village_id=$request->village_id;
        $property->sector_id=$request->sector_id;
        $property->name=$request->name;
        if( $request->file('image')){
        $imageFile = time().'.'.$request->file('image')->extension(); 
        $request->file('image')->move(public_path('uploads/properties'), $imageFile);
        $property->image = $imageFile;
    }
        $property->description=$request->description;
        $property->status=$request->status;
        $property->save();
        return redirect('admin/property-type-list')->with('success','Property Updated Successfully!');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($request->user()->can('property-type-delete'))
         $property=Property::find($id);
        $property->is_deleted = 1;
        $property->save();
        return back()->with('success','Property Deleted Successfully');
    }
}

<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Village;
use App\Models\Sector;
use App\Models\SubSector;
use App\Models\PropertyType;
use App\Models\PropertyDetails;
use DB;
use DataTables;
use Auth;
use File;
use ZipArchive; 
use PDF;
use App\Models\LableName;
use App\Models\VillageSector;
use App\Models\DocumentType;
use App\Models\DocumentUpload;
class PropertyDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request,$status=null)
    {

       return view('admin.property_details.list',['status'=>$status??'']);
   }
   public function lists(Request $request)
   {


       if ($request->ajax()) {
        $no_of_document = DB::table('document_types')->where(['is_deleted'=>0,'status'=>1])->count();

        if(Auth::user()->hasRole('super-admin')){

            if($request->get('status')==''){

             $data = DB::select('SELECT pd.id,pd.name,pd.address,pd.contact, pd.status,pd.is_document,pd.is_litigated,dp.document_type,dp.document,pd.link1,pd.link2,pd.link3,pd.link4,pd.survey_number,pd.document_number,pd.sold_year,pd.year_of_purchased,pd.custodian_property,pd.uploaded_documents,vlg.name as village,sec.name as sector,p.name as property_type,sub.name as subsector from property_types as p LEFT JOIN property_details as pd on pd.property_type_id = p.id left join sectors as sec on sec.id=pd.sector_id left join sub_sectors as sub  on sub.id=pd.subsector_id left join villages as vlg on vlg.id=pd.village_id  left join document_uploads as dp on pd.id=dp.property_id WHERE  pd.is_deleted = 0 '); 
         }else{
           if($request->get('status')==0){
            
               $data = DB::select('SELECT pd.id,pd.name,pd.address,pd.contact, pd.status,pd.is_document,pd.is_litigated,dp.document_type,dp.document,pd.link1,pd.link2,pd.link3,pd.link4,pd.survey_number,pd.document_number,pd.sold_year,pd.year_of_purchased,pd.custodian_property,pd.uploaded_documents,vlg.name as village,sec.name as sector,p.name as property_type,sub.name as subsector from property_types as p LEFT JOIN property_details as pd on pd.property_type_id = p.id left join sectors as sec on sec.id=pd.sector_id left join sub_sectors as sub  on sub.id=pd.subsector_id left join villages as vlg on vlg.id=pd.village_id  left join document_uploads as dp on pd.id=dp.property_id WHERE  pd.is_deleted = 0 AND (pd.uploaded_documents="" OR pd.uploaded_documents =0)');
           }
           if($request->get('status')==1){
                 // DB::enableQueryLog();
               $data = DB::select('SELECT pd.id,pd.name,pd.address,pd.contact, pd.status,pd.is_document,pd.is_litigated,dp.document_type,dp.document,pd.link1,pd.link2,pd.link3,pd.link4,pd.survey_number,pd.document_number,pd.sold_year,pd.year_of_purchased,pd.custodian_property,pd.uploaded_documents,vlg.name as village,sec.name as sector,p.name as property_type,sub.name as subsector from property_types as p LEFT JOIN property_details as pd on pd.property_type_id = p.id left join sectors as sec on sec.id=pd.sector_id left join sub_sectors as sub  on sub.id=pd.subsector_id left join villages as vlg on vlg.id=pd.village_id  left join document_uploads as dp on pd.id=dp.property_id WHERE  pd.is_deleted = 0 AND pd.uploaded_documents <'. $no_of_document .' AND (pd.uploaded_documents != 0 AND pd.uploaded_documents !="")');
//                  $query = DB::getQueryLog();
// dd($query);
           }
           if($request->get('status')==2){
            
               $data = DB::select('SELECT pd.id,pd.name,pd.address,pd.contact, pd.status,pd.is_document,pd.is_litigated,dp.document_type,dp.document,pd.link1,pd.link2,pd.link3,pd.link4,pd.survey_number,pd.document_number,pd.sold_year,pd.year_of_purchased,pd.custodian_property,pd.uploaded_documents,vlg.name as village,sec.name as sector,p.name as property_type,sub.name as subsector from property_types as p LEFT JOIN property_details as pd on pd.property_type_id = p.id left join sectors as sec on sec.id=pd.sector_id left join sub_sectors as sub  on sub.id=pd.subsector_id left join villages as vlg on vlg.id=pd.village_id  left join document_uploads as dp on pd.id=dp.property_id WHERE  pd.is_deleted = 0 AND pd.uploaded_documents >= '.$no_of_document);
           }
           if($request->get('status')==3){
            
               $data = DB::select('SELECT pd.id,pd.name,pd.address,pd.contact, pd.status,pd.is_document,pd.is_litigated,dp.document_type,dp.document,pd.link1,pd.link2,pd.link3,pd.link4,pd.survey_number,pd.document_number,pd.sold_year,pd.year_of_purchased,pd.custodian_property,pd.uploaded_documents,vlg.name as village,sec.name as sector,p.name as property_type,sub.name as subsector from property_types as p LEFT JOIN property_details as pd on pd.property_type_id = p.id left join sectors as sec on sec.id=pd.sector_id left join sub_sectors as sub  on sub.id=pd.subsector_id left join villages as vlg on vlg.id=pd.village_id  left join document_uploads as dp on pd.id=dp.property_id WHERE  pd.is_deleted = 0 AND pd.is_litigated= 1');
           }
           if($request->get('status')==4){
            
               $data = DB::select('SELECT pd.id,pd.name,pd.address,pd.contact, pd.status,pd.is_document,pd.is_litigated,dp.document_type,dp.document,pd.link1,pd.link2,pd.link3,pd.link4,pd.survey_number,pd.document_number,pd.sold_year,pd.year_of_purchased,pd.custodian_property,pd.uploaded_documents,vlg.name as village,sec.name as sector,p.name as property_type,sub.name as subsector from property_types as p LEFT JOIN property_details as pd on pd.property_type_id = p.id left join sectors as sec on sec.id=pd.sector_id left join sub_sectors as sub  on sub.id=pd.subsector_id left join villages as vlg on vlg.id=pd.village_id  left join document_uploads as dp on pd.id=dp.property_id WHERE  pd.is_deleted = 0 AND pd.is_litigated= 0');
           }

       }
       
   }else{
    if($request->get('status')==''){
     $user_id = Auth::user()->id;
     $where = ['user_id' => $user_id];
     $village = DB::table('user_villages')
     ->select('village_id')
     ->where($where)->get();
     $villageArray = [];
     foreach ($village as $v) {
        array_push($villageArray, $v->village_id);

    }
     $user_id = Auth::user()->id;
     $where = ['user_id' => $user_id];
     $sector = DB::table('user_sectors')
     ->select('sector_id')
     ->where($where)->get();
     $sectorArray = [];
     foreach ($sector as $sec) {
        array_push($sectorArray, $sec->sector_id);
        
    }
    $user_id = Auth::user()->id;
     $where = ['user_id' => $user_id];
     $subsector = DB::table('user_subsectors')
     ->select('subsector_id')
     ->where($where)->get();
     $subsectorArray = [];
     foreach ($subsector as $sub) {
        array_push($subsectorArray, $sub->subsector_id);
        
    }
     $user_id = Auth::user()->id;
     $where = ['user_id' => $user_id];
     $properties = DB::table('user_properties')
     ->select('property_type_id')
     ->where($where)->get();
     $propertyArray = [];
     foreach ($properties as $prop) {
        array_push($propertyArray, $prop->property_type_id);
        
    }
    $vill_ids = join("','",$villageArray);
    $sec_ids=join("','",$sectorArray);
    $sub_ids=join("','",$subsectorArray);
    $prop_ids=join("','",$propertyArray);
    $data = DB::SELECT("SELECT pd.id,pd.name,pd.address,pd.contact, pd.status,pd.is_document,pd.is_litigated,dp.document_type,dp.document,pd.link1,pd.link2,pd.link3,pd.link4,pd.survey_number,pd.document_number,pd.sold_year,pd.year_of_purchased,pd.custodian_property,pd.uploaded_documents,vlg.name as village,sec.name as sector,p.name as property_type,sub.name as subsector from property_types as p LEFT JOIN property_details as pd on pd.property_type_id = p.id left join sectors as sec on sec.id=pd.sector_id left join sub_sectors as sub  on sub.id=pd.subsector_id left join villages as vlg on vlg.id=pd.village_id  left join document_uploads as dp on pd.id=dp.property_id WHERE  pd.is_deleted = 0 AND pd.village_id IN('$vill_ids') AND pd.sector_id IN('$sec_ids') AND pd.subsector_id IN('$sub_ids') AND pd.property_type_id IN('$prop_ids')");  
}else{
  if($request->get('status')==0){
    $user_id = Auth::user()->id;
    $where = ['user_id' => $user_id];
    $village = DB::table('user_villages')
    ->select('village_id')
    ->where($where)->get();
    $villageArray = [];
    foreach ($village as $v) {
        array_push($villageArray, $v->village_id);
    }

    $user_id = Auth::user()->id;
     $where = ['user_id' => $user_id];
     $sector = DB::table('user_sectors')
     ->select('sector_id')
     ->where($where)->get();
     $sectorArray = [];
     foreach ($sector as $sec) {
        array_push($sectorArray, $sec->sector_id);
        
    }
    $user_id = Auth::user()->id;
     $where = ['user_id' => $user_id];
     $subsector = DB::table('user_subsectors')
     ->select('subsector_id')
     ->where($where)->get();
     $subsectorArray = [];
     foreach ($subsector as $sub) {
        array_push($subsectorArray, $sub->subsector_id);
        
    }
     $user_id = Auth::user()->id;
     $where = ['user_id' => $user_id];
     $properties = DB::table('user_properties')
     ->select('property_type_id')
     ->where($where)->get();
     $propertyArray = [];
     foreach ($properties as $prop) {
        array_push($propertyArray, $prop->property_type_id);
        
    }
    $vill_ids = join("','",$villageArray);
    $sec_ids=join("','",$sectorArray);
    $sub_ids=join("','",$subsectorArray);
    $prop_ids=join("','",$propertyArray);
    $data = DB::SELECT("SELECT pd.id,pd.name,pd.address,pd.contact, pd.status,pd.is_document,pd.is_litigated,dp.document_type,dp.document,pd.link1,pd.link2,pd.link3,pd.link4,pd.survey_number,pd.document_number,pd.sold_year,pd.year_of_purchased,pd.custodian_property,pd.uploaded_documents,vlg.name as village,sec.name as sector,p.name as property_type,sub.name as subsector from property_types as p LEFT JOIN property_details as pd on pd.property_type_id = p.id left join sectors as sec on sec.id=pd.sector_id left join sub_sectors as sub  on sub.id=pd.subsector_id left join villages as vlg on vlg.id=pd.village_id  left join document_uploads as dp on pd.id=dp.property_id WHERE  pd.is_deleted = 0 AND pd.village_id IN('$vill_ids') AND pd.sector_id IN('$sec_ids') AND pd.subsector_id IN('$sub_ids') AND pd.property_type_id IN('$prop_ids') AND (pd.uploaded_documents='' OR pd.uploaded_documents =0)"); 
}
if($request->get('status')==1){
    $user_id = Auth::user()->id;
    $where = ['user_id' => $user_id];
    $village = DB::table('user_villages')
    ->select('village_id')
    ->where($where)->get();
    $villageArray = [];
    foreach ($village as $v) {
        array_push($villageArray, $v->village_id);
    }
   $user_id = Auth::user()->id;
     $where = ['user_id' => $user_id];
     $sector = DB::table('user_sectors')
     ->select('sector_id')
     ->where($where)->get();
     $sectorArray = [];
     foreach ($sector as $sec) {
        array_push($sectorArray, $sec->sector_id);
        
    }
    $user_id = Auth::user()->id;
     $where = ['user_id' => $user_id];
     $subsector = DB::table('user_subsectors')
     ->select('subsector_id')
     ->where($where)->get();
     $subsectorArray = [];
     foreach ($subsector as $sub) {
        array_push($subsectorArray, $sub->subsector_id);
        
    }
     $user_id = Auth::user()->id;
     $where = ['user_id' => $user_id];
     $properties = DB::table('user_properties')
     ->select('property_type_id')
     ->where($where)->get();
     $propertyArray = [];
     foreach ($properties as $prop) {
        array_push($propertyArray, $prop->property_type_id);
        
    }
    $vill_ids = join("','",$villageArray);
    $sec_ids=join("','",$sectorArray);
    $sub_ids=join("','",$subsectorArray);
    $prop_ids=join("','",$propertyArray);
    $data = DB::SELECT("SELECT pd.id,pd.name,pd.address,pd.contact, pd.status,pd.is_document,pd.is_litigated,dp.document_type,dp.document,pd.link1,pd.link2,pd.link3,pd.link4,pd.survey_number,pd.document_number,pd.sold_year,pd.year_of_purchased,pd.custodian_property,pd.uploaded_documents,vlg.name as village,sec.name as sector,p.name as property_type,sub.name as subsector from property_types as p LEFT JOIN property_details as pd on pd.property_type_id = p.id left join sectors as sec on sec.id=pd.sector_id left join sub_sectors as sub  on sub.id=pd.subsector_id left join villages as vlg on vlg.id=pd.village_id  left join document_uploads as dp on pd.id=dp.property_id WHERE  pd.is_deleted = 0 AND pd.village_id IN('$vill_ids') AND pd.sector_id IN('$sec_ids') AND pd.subsector_id IN('$sub_ids') AND pd.property_type_id IN('$prop_ids') AND pd.uploaded_documents <'. $no_of_document .' AND (pd.uploaded_documents != 0 AND pd.uploaded_documents !='')"); 
}
if($request->get('status')==2){
    $user_id = Auth::user()->id;
    $where = ['user_id' => $user_id];
    $village = DB::table('user_villages')
    ->select('village_id')
    ->where($where)->get();
    $villageArray = [];
    foreach ($village as $v) {
        array_push($villageArray, $v->village_id);
    }
    $user_id = Auth::user()->id;
     $where = ['user_id' => $user_id];
     $sector = DB::table('user_sectors')
     ->select('sector_id')
     ->where($where)->get();
     $sectorArray = [];
     foreach ($sector as $sec) {
        array_push($sectorArray, $sec->sector_id);
        
    }
    $user_id = Auth::user()->id;
     $where = ['user_id' => $user_id];
     $subsector = DB::table('user_subsectors')
     ->select('subsector_id')
     ->where($where)->get();
     $subsectorArray = [];
     foreach ($subsector as $sub) {
        array_push($subsectorArray, $sub->subsector_id);
        
    }
     $user_id = Auth::user()->id;
     $where = ['user_id' => $user_id];
     $properties = DB::table('user_properties')
     ->select('property_type_id')
     ->where($where)->get();
     $propertyArray = [];
     foreach ($properties as $prop) {
        array_push($propertyArray, $prop->property_type_id);
        
    }
    $vill_ids = join("','",$villageArray);
    $sec_ids=join("','",$sectorArray);
    $sub_ids=join("','",$subsectorArray);
    $prop_ids=join("','",$propertyArray);
    $data = DB::SELECT("SELECT pd.id,pd.name,pd.address,pd.contact, pd.status,pd.is_document,pd.is_litigated,dp.document_type,dp.document,pd.link1,pd.link2,pd.link3,pd.link4,pd.survey_number,pd.document_number,pd.sold_year,pd.year_of_purchased,pd.custodian_property,pd.uploaded_documents,vlg.name as village,sec.name as sector,p.name as property_type,sub.name as subsector from property_types as p LEFT JOIN property_details as pd on pd.property_type_id = p.id left join sectors as sec on sec.id=pd.sector_id left join sub_sectors as sub  on sub.id=pd.subsector_id left join villages as vlg on vlg.id=pd.village_id  left join document_uploads as dp on pd.id=dp.property_id WHERE  pd.is_deleted = 0 AND pd.village_id IN('$vill_ids') AND pd.sector_id IN('$sec_ids') AND pd.subsector_id IN('$sub_ids') AND pd.property_type_id IN('$prop_ids') AND pd.uploaded_documents >= $no_of_document"); 
}
if($request->get('status')==3){
 $user_id = Auth::user()->id;
 $where = ['user_id' => $user_id];
 $village = DB::table('user_villages')
 ->select('village_id')
 ->where($where)->get();
 $villageArray = [];
 foreach ($village as $v) {
    array_push($villageArray, $v->village_id);
}

$user_id = Auth::user()->id;
     $where = ['user_id' => $user_id];
     $sector = DB::table('user_sectors')
     ->select('sector_id')
     ->where($where)->get();
     $sectorArray = [];
     foreach ($sector as $sec) {
        array_push($sectorArray, $sec->sector_id);
        
    }
    $user_id = Auth::user()->id;
     $where = ['user_id' => $user_id];
     $subsector = DB::table('user_subsectors')
     ->select('subsector_id')
     ->where($where)->get();
     $subsectorArray = [];
     foreach ($subsector as $sub) {
        array_push($subsectorArray, $sub->subsector_id);
        
    }
     $user_id = Auth::user()->id;
     $where = ['user_id' => $user_id];
     $properties = DB::table('user_properties')
     ->select('property_type_id')
     ->where($where)->get();
     $propertyArray = [];
     foreach ($properties as $prop) {
        array_push($propertyArray, $prop->property_type_id);
        
    }
    $vill_ids = join("','",$villageArray);
    $sec_ids=join("','",$sectorArray);
    $sub_ids=join("','",$subsectorArray);
    $prop_ids=join("','",$propertyArray);
    $data = DB::SELECT("SELECT pd.id,pd.name,pd.address,pd.contact, pd.status,pd.is_document,pd.is_litigated,dp.document_type,dp.document,pd.link1,pd.link2,pd.link3,pd.link4,pd.survey_number,pd.document_number,pd.sold_year,pd.year_of_purchased,pd.custodian_property,pd.uploaded_documents,vlg.name as village,sec.name as sector,p.name as property_type,sub.name as subsector from property_types as p LEFT JOIN property_details as pd on pd.property_type_id = p.id left join sectors as sec on sec.id=pd.sector_id left join sub_sectors as sub  on sub.id=pd.subsector_id left join villages as vlg on vlg.id=pd.village_id  left join document_uploads as dp on pd.id=dp.property_id WHERE  pd.is_deleted = 0 AND pd.village_id IN('$vill_ids') AND pd.sector_id IN('$sec_ids') AND pd.subsector_id IN('$sub_ids') AND pd.property_type_id IN('$prop_ids') AND pd.is_litigated=1 "); 
}
if($request->get('status')==4){
 $user_id = Auth::user()->id;
 $where = ['user_id' => $user_id];
 $village = DB::table('user_villages')
 ->select('village_id')
 ->where($where)->get();
 $villageArray = [];
 foreach ($village as $v) {
    array_push($villageArray, $v->village_id);
}

$user_id = Auth::user()->id;
     $where = ['user_id' => $user_id];
     $sector = DB::table('user_sectors')
     ->select('sector_id')
     ->where($where)->get();
     $sectorArray = [];
     foreach ($sector as $sec) {
        array_push($sectorArray, $sec->sector_id);
        
    }
    $user_id = Auth::user()->id;
     $where = ['user_id' => $user_id];
     $subsector = DB::table('user_subsectors')
     ->select('subsector_id')
     ->where($where)->get();
     $subsectorArray = [];
     foreach ($subsector as $sub) {
        array_push($subsectorArray, $sub->subsector_id);
        
    }
     $user_id = Auth::user()->id;
     $where = ['user_id' => $user_id];
     $properties = DB::table('user_properties')
     ->select('property_type_id')
     ->where($where)->get();
     $propertyArray = [];
     foreach ($properties as $prop) {
        array_push($propertyArray, $prop->property_type_id);
        
    }
    $vill_ids = join("','",$villageArray);
    $sec_ids=join("','",$sectorArray);
    $sub_ids=join("','",$subsectorArray);
    $prop_ids=join("','",$propertyArray);
    $data = DB::SELECT("SELECT pd.id,pd.name,pd.address,pd.contact, pd.status,pd.is_document,pd.is_litigated,dp.document_type,dp.document,pd.link1,pd.link2,pd.link3,pd.link4,pd.survey_number,pd.document_number,pd.sold_year,pd.year_of_purchased,pd.custodian_property,pd.uploaded_documents,vlg.name as village,sec.name as sector,p.name as property_type,sub.name as subsector from property_types as p LEFT JOIN property_details as pd on pd.property_type_id = p.id left join sectors as sec on sec.id=pd.sector_id left join sub_sectors as sub  on sub.id=pd.subsector_id left join villages as vlg on vlg.id=pd.village_id  left join document_uploads as dp on pd.id=dp.property_id WHERE  pd.is_deleted = 0 AND pd.village_id IN('$vill_ids') AND pd.sector_id IN('$sec_ids') AND pd.subsector_id IN('$sub_ids') AND pd.property_type_id IN('$prop_ids') AND pd.is_litigated=0 ");   
}
}
}


return Datatables::of($data)
->addIndexColumn()
->addColumn('status',function($row){
    $sts = ($row->status == 1)?'<span class="badge badge-success bg-green">Active</span>':'<span class="badge badge-danger">In-Active</span>';
    return $sts;
})

->addColumn('is_document',function($row){
    $document = ($row->is_document==1)?'<span class="badge badge-success bg-green">Yes</span>':'<span class="badge badge-danger">No</span>';
    return $document;
})

->addColumn('is_litigated',function($row){
    $litigate = ($row->is_litigated==1)?'<span class="badge badge-danger">Yes</span>':'<span class="badge badge-success bg-green">No</span>';
    return $litigate;
})

->addColumn('document',function($row){
    $document = ($row->document==NULL)?"<span class='badge badge-danger'>No Documents</span>":"<a href='".asset('admin/documents').'/'.$row->id."'  class='btn  btn-primary bg-green'><i class='glyphicon glyphicon-download'></i></a>";
    return $document;
})

->addColumn('uploaded_documents',function($row){
 $documentsCount = DB::table('document_types')->where('is_deleted',0)->count();
 $uploaded_documents = '';

 if($row->uploaded_documents == $documentsCount)
 {
    $uploaded_documents = 'Fully Documented';
}
if($row->uploaded_documents < $documentsCount and $row->uploaded_documents!=0)
{
 $uploaded_documents = 'Partially Documented';
}
if(!$row->uploaded_documents)
{
 $uploaded_documents = 'No Documented';
}
return $uploaded_documents;
})

->addColumn('action', function($row){

 $btn = " <div class='btn-group'>
 <a href='". route('admin.propertiesdetails.edit', [$row->id])."' class='bg-green btn-sm'><i class='fa fa-pencil' aria-hidden='true'></i></a>&nbsp;&nbsp;&nbsp;<a href='".route('admin.propertiesdetails.property_pdf',[$row->id])."' class='bg-green btn-sm'><i class='fa fa-file-pdf-o' aria-hidden='true'></i></a>

 </div>";

 return $btn;
})
->rawColumns(['action','status','is_document','is_litigated','document'])
->make(true);
}
}

public function ZipFile($id){      
    $zip = new ZipArchive;
    
    $myfile = DB::table('document_uploads as dp')->select('dp.document')->where('property_id',$id)->first();
    if($myfile){
        $mydoc = explode(',', $myfile->document);
        $fileArray = [];
        foreach ($mydoc as $value) {
            if(file_exists(public_path('uploads/property_documents').'/'.$value)){
               array_push($fileArray, public_path('uploads/property_documents').'/'.$value); 
           }
           
       }
       $fileName = time().'myzip.zip';
       if ($zip->open(public_path($fileName), ZipArchive::CREATE) === TRUE)
       {
                // $files = File::files(public_path('uploads/property_documents/'.json_encode($fileArray)));
                // print_r($fileArray);die;
        foreach ($fileArray as $key => $value) {
            $relativeNameInZipFile = basename($value);
            $zip->addFile($value, $relativeNameInZipFile);
        }


        $zip->close();
    }
    if(file_exists(public_path($fileName))){
     return response()->download(public_path($fileName));   
 }else{
    return json_encode(["message"=>"File doesn't Exist on Server."]);
}

}else{
    echo "No document Found.";
}

}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $no_of_document = DB::table('document_types')->where(['is_deleted'=>0,'status'=>1])->count();
        if(Auth::user()->hasRole('super-admin')){
            $label=LableName::all()->where('id',1);
            $document=DocumentType::all()->where('is_deleted',0)->where('status',1);
            $data = DB::table('villages')->where(['is_deleted'=>0])->get();
            $sector = DB::table('sectors')->where(['is_deleted'=>0])->get();
            $subsector = DB::table('sub_sectors')->where(['is_deleted'=>0])->get();
            $property_type = PropertyType::where(['is_deleted'=>0])->get();
            return view('admin.property_details.create',['village'=>$data,'sector'=>$sector,'subsector'=>$subsector,'property'=>$property_type,'label'=>$label,'document'=>$document,'no_of_document'=>$no_of_document]);
        }else{
            $id=Auth::User()->id;
            $label=LableName::all()->where('id',1);
            $document=DocumentType::all()->where('is_deleted',0)->where('status',1);
            $data = DB::table('villages')->join('user_villages','villages.id','=','user_villages.village_id')->select('villages.*')
            ->where('user_villages.user_id',$id)->where('is_deleted',0)->get();
            $sector = DB::table('sectors')->join('user_sectors','sectors.id','=','user_sectors.sector_id')->select('sectors.*')->where('user_sectors.user_id',$id)->get();
            $subsector = DB::table('sub_sectors')->join('user_subsectors','sub_sectors.id','=','user_subsectors.subsector_id')->select('sub_sectors.*')->where('user_subsectors.user_id',$id)->get();
            $property_type = DB::table('property_types')->join('user_properties','property_types.id','=','user_properties.property_type_id')->select('property_types.*')->where('user_properties.user_id',$id)->get();
            return view('admin.property_details.create',['village'=>$data,'sector'=>$sector,'subsector'=>$subsector,'property'=>$property_type,'label'=>$label,'document'=>$document,'no_of_document'=>$no_of_document]);
        }

    }
    public function sector(Request $request,$id)
    {   if(Auth::user()->hasRole('super-admin')){
        $sector_id = DB::table('sectors')
        ->where('village_id',$id)
        ->where('is_deleted',0)
        ->get();
        return json_encode($sector_id);
    }else{
        $user_id=Auth::User()->id;
        $sector_id = DB::table('sectors') 
        ->join('user_sectors','sectors.id','=','user_sectors.sector_id') 
        ->select('sectors.*') 
        ->where('user_sectors.user_id',$user_id)
        ->where("village_id",$id)
        ->where('is_deleted',0) 
        ->get();
        return json_encode($sector_id);
    }

}

public function propertyDetailSubsector(Request $request,$id){

    if(Auth::user()->hasRole('super-admin')){

        $subsector_id = DB::table('sub_sectors')
        ->where('sector_id',$id)
        ->where('is_deleted',0)
        ->get();
        return json_encode($subsector_id);
    }else{
        $user_id=Auth::User()->id;
        $subsector_id = DB::table('sub_sectors')
        ->join('user_subsectors','sub_sectors.id','=','user_subsectors.subsector_id')
        ->select('sub_sectors.*') 
        ->where('user_subsectors.user_id',$user_id)
        ->where("sector_id",$id)
        ->where('is_deleted',0)
        ->get();
        return json_encode($subsector_id);
    }
}
public function propertytype(Request $request,$id)
{
    if(Auth::user()->hasRole('super-admin')){
        $property_type_id = DB::table('property_types')
        ->where('subsector_id',$id)
        ->where('is_deleted',0)
        ->get();
    }else{
        $user_id=Auth::User()->id;
        $property_type_id = DB::table('property_types')
        ->join('user_properties','property_types.id','=','user_properties.property_type_id')
        ->select('property_types.*')
        ->where('user_properties.user_id',$user_id)
        ->where("subsector_id",$id)
        ->where('is_deleted',0)
        ->get();
        
    }
    return json_encode($property_type_id);
}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        
        $count = $request->document_type;
        if($request->is_document==1){
            $mycount = count($count);
            
        }
        $vaidateDataField = [
           'village_id'=>'required',
           'sector_id'=>'required',
           'property_type_id'=>'required',
           'name'=>'required',
           'address'=>'required|',
           'contact'=>'required',
           'status'=>'required',
           'is_document'=>'required',
           'is_litigated'=>'required'

       ];

        // if($request->is_document == 1){
        //     $vaidateDataField['document_type'] = 'required';
        //     $vaidateDataField['document'] = 'required|mimes:jpeg,png,jpg,doc,docx,pdf|max:2048';
        // }

       $request->validate($vaidateDataField);

       if($request->year_of_purchased>$request->sold_year){
        return redirect('admin/properties-details-create')->with('error','Sold Year Must be grater than Purchased Year');
    }

    $property_details = new PropertyDetails();
    $property_details->village_id=$request->village_id;
    $property_details->sector_id=$request->sector_id;
    $property_details->subsector_id=($request->subsector_id)??'0';
    $property_details->property_type_id=$request->property_type_id;
    $property_details->name=$request->name;
    $property_details->address=$request->address;
    $property_details->contact=$request->contact;
    $property_details->survey_number=($request->survey_number)??'';
    $property_details->document_number=($request->document_number)??'';
    $property_details->custodian_property=($request->custodian_property)??'';
    $property_details->year_of_purchased=($request->year_of_purchased)??''; 
    $property_details->sold_year=($request->sold_year)??'';
    $property_details->link1=($request->link1)??'';
    $property_details->link2=($request->link2)??'';
    $property_details->link3=($request->link3)??'';
    $property_details->link4=($request->link4)??'';   
    $property_details->status=$request->status;
    $property_details->is_document=$request->is_document;
    $property_details->is_litigated=$request->is_litigated;
    $property_details->uploaded_documents = $mycount??'';

        // if($request->is_document==1){
        //     $imageFile = time().'.'.$request->file('document')->extension(); 
        // $request->file('document')->move(public_path('uploads/property_documents'), $imageFile);
        // $property_details->document = $imageFile;
        // $property_details->document_type = $request->document_type;

    $property_details->save();

    $file = [];
    $type_document = [];
    if($request->is_document==1){
        $request->validate([
           'document_type' => 'required',
           'document.*' => 'required|mimes:jpeg,png,jpg,doc,docx,pdf'
       ]);
        if($request->hasFile('document')){
            foreach ($request->document as  $documents) {
                $imageFile = time().rand(1,100).'.'.$documents->extension(); 
                $documents->move(public_path('uploads/property_documents'), $imageFile);
        // $request->file('document')->move(public_path('uploads/property_documents'), $imageFile);
                array_push($file, $imageFile);
            }
        }
        foreach ($request->document_type as $value) {

            array_push($type_document, $value);
        }

    }

    $document_upload = new DocumentUpload();
    $document_upload->document = implode(',', $file);
    $document_upload->document_type = implode(',',$type_document);
    $document_upload->property_id = $property_details->id;
    $document_upload->save();

    return redirect('admin/properties-details-list')->with('success','PropertyDetails Created Successfully!');
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {

        if(Auth::user()->hasRole('super-admin')){
           $document=DocumentType::all()->where('is_deleted',0);
           $property_details=PropertyDetails::find($id);
           $village = DB::table('villages')->where(['is_deleted'=>0])->get();
           $sector =  DB::table('sectors')->where(['is_deleted'=>0])->get();
           $subsector =  DB::table('sub_sectors')->where(['is_deleted'=>0])->get();
           $property_type = DB::table('property_types')->where(['is_deleted'=>0])->get();
           return view('admin.property_details.edit',['property_details'=>$property_details,'village'=>$village,'sector'=>$sector,'subsector'=>$subsector,'document'=>$document,'property_type'=>$property_type]);
       }else{
        $user_id = Auth::User()->id;
        $document=DocumentType::all()->where('is_deleted',0);
        $property_details=PropertyDetails::find($id);
        $village = DB::table('villages')->join('user_villages','villages.id','=','user_villages.village_id')->select('villages.*')
        ->where('user_villages.user_id',$user_id)->get();
        $sector =  DB::table('sectors')->join('user_sectors','sectors.id','=','user_sectors.sector_id')->select('sectors.*')->where('user_sectors.user_id',$user_id)
        ->where("village_id",$id)->get();
        $subsector =  DB::table('sub_sectors')->join('user_subsectors','sub_sectors.id','=','user_subsectors.subsector_id')->select('sub_sectors.*')->where('user_subsectors.user_id',$id)->get();
        $property_type = DB::table('property_types')->join('user_properties','property_types.id','=','user_properties.property_type_id')->select('property_types.*')->where('user_properties.user_id',$user_id)->where("sector_id",$id)->get();
        return view('admin.property_details.edit',['property_details'=>$property_details,'village'=>$village,'sector'=>$sector,'subsector'=>$subsector,'document'=>$document,'property_type'=>$property_type]);
    }   
}    


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {   
        //  $document_upload = DocumentUpload::all()->where('property_id',$id);
        // $mydata = [];
        // foreach ($document_upload as  $value) {
        //     array_push($mydata,$value->document_type);
        // }
        // $string = implode(',', $mydata);
        // $str_arr = explode (",", $string); 
        // print_r($str_arr);
        $no_of_document = DB::table('document_types')->where(['is_deleted'=>0,'status'=>1])->count();
        $data = [];
        if(Auth::user()->hasRole('super-admin')){
           $document=DocumentType::all()->where('is_deleted',0)->where('status',1);
           $property_details=PropertyDetails::find($id);
           $result = [];
           if($property_details->is_document==1){
            $documentData = DB::table('property_details')
            ->join('document_uploads','document_uploads.property_id','=','property_details.id')
            ->select('property_details.*','document_uploads.document_type','document_uploads.document')
            ->where('document_uploads.property_id',$id)
            ->where('property_details.is_deleted',0)
            ->get();
            $myArray = [];
            $myImage = [];
            foreach ($documentData as  $value) {
             $myArray = explode(",", $value->document_type);
               // $myArray['documentData'] = explode(",", $value->document);
         }
         foreach ($documentData as  $values) {
             $myImage = explode(",", $values->document);
         }
        
         $result =array_combine($myArray, $myImage);
        
         $data = [];
         foreach ($result as $key => $value) {
             $result[$key] = asset('uploads/property_documents').'/'.$value;
               // array_push($data, $d);
         }

     }
     $document_upload = DocumentUpload::all()->where('property_id',$id);
     $village = DB::table('villages')->get();
     $sector =  DB::table('sectors')->get();
     $subsector =  DB::table('sub_sectors')->get();
     $property_type = DB::table('property_types')->get();

     return view('admin.property_details.edit',['property_details'=>$property_details,'village'=>$village,'sector'=>$sector,'subsector'=>$subsector,'document'=>$document,'property_type'=>$property_type,'document_upload'=>$document_upload,'data'=>$result,'no_of_document'=>$no_of_document]);
 }else{
    $user_id = Auth::User()->id;
    $document=DocumentType::all()->where('is_deleted',0)->where('status',1);
    $property_details=PropertyDetails::find($id);
    $result = [];
    if($property_details->is_document==1){
        $documentData = DB::table('property_details')
        ->join('document_uploads','document_uploads.property_id','=','property_details.id')
        ->select('property_details.*','document_uploads.document_type','document_uploads.document')
        ->where('document_uploads.property_id',$id)
        ->get();
        $myArray = [];
        $myImage = [];
        foreach ($documentData as  $value) {
         $myArray = explode(",", $value->document_type);
               // $myArray['documentData'] = explode(",", $value->document);
     }
     foreach ($documentData as  $values) {
         $myImage = explode(",", $values->document);
     }
     $result =array_combine($myArray, $myImage);
     $data = [];
     foreach ($result as $key => $value) {
         $d[$key] = asset('uploads/property_documents').'/'.$value;

         array_push($data, $d);
     }

            // return  json_encode(['data'=>$data]);
 }
 $document_upload = DocumentUpload::all()->where('property_id',$id);
 $village = DB::table('villages')
 ->join('user_villages','villages.id','=','user_villages.village_id')
 ->select('villages.*')
 ->where('user_villages.user_id',$user_id)->get();
 $sector =  DB::table('sectors')
 ->leftjoin('user_sectors','sectors.id','=','user_sectors.sector_id')
 ->select('sectors.*','user_sectors.*')
 ->where('user_sectors.user_id',$user_id)
 ->get();
 $subsector =  DB::table('sub_sectors')
 ->join('user_subsectors','sub_sectors.id','=','user_subsectors.subsector_id')
 ->select('sub_sectors.*','user_subsectors.*')
 ->where('user_subsectors.user_id',$user_id)
 ->get();
 $property_type = DB::table('property_types')
 ->join('user_properties','property_types.id','=','user_properties.property_type_id')
 ->select('property_types.*')
 ->where('user_properties.user_id',$user_id)
 ->get();
 return view('admin.property_details.edit',['property_details'=>$property_details,'village'=>$village,'sector'=>$sector,'subsector'=>$subsector,'document'=>$document,'property_type'=>$property_type,'document_upload'=>$document_upload,'data'=>$result,'no_of_document'=>$no_of_document]);
}

}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if($request->document_type){
            $count = array_filter($request->document_type);
            if($request->is_document==1){
                $mycount = count($count);
            }
        }
        $vaidateDataField = [
           'village_id'=>'required',
           'sector_id'=>'required',
           'property_type_id'=>'required',
           'name'=>'required',
           'address'=>'required|',
           'contact'=>'required',
           'status'=>'required',
           'is_document'=>'required',
           'is_litigated'=>'required'

       ];
        // if($request->is_document == 1){
        //     $vaidateDataField['document_type'] = 'required';
        //     $vaidateDataField['document'] = 'nullable|mimes:jpeg,png,jpg,doc,docx,pdf';
        // }
       $request->validate($vaidateDataField);

       if($request->year_of_purchased>$request->sold_year){
        return redirect('admin/properties-details-edit/{id}')->with('error','Sold Year Must be grater than Purchased Year');
    }


    $property_details=PropertyDetails::find($id);
    $property_details->village_id=$request->village_id;
    $property_details->sector_id=$request->sector_id;
    $property_details->subsector_id=($request->subsector_id)??'0';
    $property_details->property_type_id=$request->property_type_id;
    $property_details->name=$request->name;
    $property_details->address=$request->address;
    $property_details->contact=$request->contact;
    $property_details->survey_number=($request->survey_number)??'';
    $property_details->document_number=($request->document_number)??'';
    $property_details->custodian_property=($request->custodian_property)??'';
    $property_details->year_of_purchased=($request->year_of_purchased)??''; 
    $property_details->sold_year=($request->sold_year)??'';
    $property_details->link1=($request->link1)??'';
    $property_details->link2=($request->link2)??'';        
    $property_details->link3=($request->link3)??'';
    $property_details->link4=($request->link4)??'';
    $property_details->status=$request->status;
    $property_details->is_document=$request->is_document;
    $property_details->is_litigated=$request->is_litigated;
    if($property_details->uploaded_documents==NULL){
        $property_details->uploaded_documents = $mycount;
    }
    if($request->document_type){

        $property_details->uploaded_documents = $property_details->uploaded_documents + $mycount??'';
    }

        // if($request->is_document==1){
        //     $imageFile = time().rand(1,100).'.'.$request->file('document')->extension(); 
        // $request->file('document')->move(public_path('uploads/property_documents'), $imageFile);
        // $property_details->document = $imageFile;
        // $property_details->document_type = $request->document_type;
        // }
    $property_details->save();
    $file = [];
    $type_document = [];
    if($request->is_document==1){

        $request->validate([
           'document_type' => 'nullable',
           'document.*' => 'nullable|mimes:jpeg,png,jpg,doc,docx,pdf'
       ]);
        if($request->document_type){
           if($request->hasFile('document')){

            foreach ($request->document as  $documents) {
                $imageFile = time().rand(1,100).'.'.$documents->extension(); 
                $documents->move(public_path('uploads/property_documents'), $imageFile);
        // $request->file('document')->move(public_path('uploads/property_documents'), $imageFile);
                array_push($file, $imageFile);
            }
        }
        foreach ($request->document_type as $value) {

            array_push($type_document, $value);
        }

    }
}

if($request->document){     
 $document_upload =  DocumentUpload::find($property_id=$id);
 $result = DB::table('document_uploads')->where(['property_id'=>$id])->first();
 if(empty($result)){
     $document_upload = new DocumentUpload();
     $document_upload->document = implode(',', $file);
     $document_upload->document_type = implode(',',$type_document);
     $document_upload->property_id = $property_details->id;
     $document_upload->save();
 }
 if($result){
     $docType = explode(',', $result->document_type);
     $docType = array_filter($docType);
     $docType =  array_merge($docType, array_filter($type_document));
     $doc = explode(',', $result->document);
     $doc = array_filter($doc);
     $doc = array_merge($doc, $file);
     DB::table('document_uploads')
     ->where(['property_id'=>$id])
     ->update(['document_type'=>implode(",",$docType ),
        'document'=>implode(",", $doc)]);

 }

}
return redirect('admin/properties-details-list')->with('success','PropertyDetails Updated Successfully!');
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function propertyDocument(Request $request,$data,$id)
    {

        $documentData = DB::table('property_details')
        ->join('document_uploads','document_uploads.property_id','=','property_details.id')
        ->select('document_uploads.document_type')
        ->where('document_uploads.property_id',$id)
        ->get();
        $myData = [];
        $myData = explode(",", $documentData[0]->document_type??'0');
        // $string = implode(",", $myData);
        $flag = 0;
        
        $data = explode(',', $data);
        foreach ($data as $d) {
            if(in_array($d,$myData)){
              $flag = 1;
          }

      }


      if($flag==1){
         return json_encode(['status'=>false]);
     }else{
         return json_encode(['status'=>true]);
     }


 }
 public function documentDelete($name,$id)
 {

    $result = DB::table('document_uploads')
    ->where(['property_id'=>$id])
    ->first();
        // if(empty($result)){
        //     DB::table('property_details')->where(['id'=>$id])->update(['is_document'=>0]);
        // }             
    if($result){
     $docName = explode(',', $result->document_type);
     $docFile = explode(',', $result->document);
     

     $index = array_search($name, $docName);
              // unset($docName[$index]);
              // unset($docFile[$index]);
     array_splice($docName, $index, 1);
     array_splice($docFile, $index, 1);
             // print_r(implode(',', $docFile));die;
     $dcoCount = DB::table('property_details')->select('property_details.uploaded_documents')->where(['id'=>$id])->first();
     $docValue = $dcoCount->uploaded_documents;
     
     if(isset($docName)){
        $docNa = implode(",", $docName);
        if(empty($docNa)){
         DB::table('property_details')
         ->where(['id'=>$id])
         ->update(['is_document'=>'0']);
     }
 }

 $updateData = array(
    'document_type' => implode(',', $docName),
    'document' => implode(',', $docFile)
);
 $myresult = DB::table('property_details')
 ->where(['id'=>$id])
 ->update(['uploaded_documents'=>$docValue-1]);
 $result = DB::table('document_uploads')
 ->where(['property_id'=>$id])
 ->update($updateData);


 return true;


}else{
    return false;
}
}
public function destroy(Request $request,$id)
{

    $property_details=PropertyDetails::find($id);
    $property_details->is_deleted = 1;
    $property_details->save();
    return back()->with('success','PropertyDetails Deleted Successfully');

}
public function genratePdf(Request $request,$id){
    $property_details = PropertyDetails::find($id);
    $village = PropertyDetails::join('villages','villages.id','=','property_details.village_id')->where('property_details.id',$id)->select('villages.name')->first();
    $sector = PropertyDetails::join('sectors','sectors.id','=','property_details.sector_id')->where('property_details.id',$id)->select('sectors.name')->first();
    $subsector = PropertyDetails::join('sub_sectors','sub_sectors.id','=','property_details.subsector_id')->where('property_details.id',$id)->select('sub_sectors.name')->first();
    $property_type = PropertyDetails::join('property_types','property_types.id','=','property_details.property_type_id')->where('property_details.id',$id)->select('property_types.name')->first();
       $document = DocumentUpload::find($property_id=$id); 
     $data = ['village' => $village->name,'sector'=>$sector->name,'subsector'=>$subsector->name,'property_type'=>$property_type->name,'property_details'=>$property_details,'document'=>$document];
         $pdf = PDF::loadView('admin.property_details.mypdf', $data);
           $property_name = $property_details->name; 
         return $pdf->download('property_name.pdf');
}
}

           // $myArray = [];
           //              $myImage = [];
           // foreach ($documentData as  $value) {
           //     $myArray = explode(",", $value->document_type);
           //     // $myArray['documentData'] = explode(",", $value->document);
           // }
           // foreach ($documentData as  $values) {
           //     $myImage = explode(",", $values->document);
           // }
           // $result =array_combine($myArray, $myImage);
           // $data = [];
           // foreach ($result as $key => $value) {
           //     $d[$key] = asset('uploads/property_documents').'/'.$value;

           //     array_push($data, $d);
           // }
           // return  json_encode(['data'=>$data]);

<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Role;

use DataTables;

class RoleController extends Controller
{
    //

    public function list(Request $request){
        if($request->user()->can('role-show')){
        return view('admin.role.list');
    }else{
        return "You don't have permission please contact to administrator";
    }
}
    public function lists(Request $request){
        if ($request->ajax()) {
            $data = Role::select('*');
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
     
                           $btn = "<a href='". route('admin.role.edit', [$row->id])."' class='bg-green btn-sm'><i class='fa fa-pencil' aria-hidden='true'></i></a>
                           <a href='". route('admin.role.show', [$row->id])."' class='bg-green btn-sm ml-1'><i class='fa fa-eye' aria-hidden='true'></i></a>
                           <a href='". route('admin.role.delete', [$row->id])."' onclick='return confirm(\"Are you sure you want to delete this item?\");' class='bg-green btn-sm ml-1'><i class='fa fa-trash' aria-hidden='true'></i></a>";
       
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }

    }

    public function add(Request $request){
        if($request->user()->can('role-create')){
        return view('admin.role.add');
    }else{
        return "You don't have permission please contact to administrator";
    }
}

    public function store(Request $request){
        $validated = $request->validate([
            'name' => 'required',
            'slug' => 'required',
            'description' => 'required'
        ]);
        $name = $request->post('name');
        $slug = $request->post('slug');
        $description = $request->post('description');
        $permission = Role::create([
                'name' => $name,
                'slug' => $slug,
                'description' => $description
            ]);
        return redirect('admin/role-list')->with('success','Role created Successfully!');
    }

    public function edit(Request $request,$id){
        if($request->user()->can('role-edit')){
        $role = Role::find($id);
        return view('admin.role.edit',compact('role'));
    }
}

    public function update($id,Request $request){
		$validated = $request->validate([
            'name' => 'required',
            'slug' => 'required',
            'description' => 'required'
        ]);
        $permission = Role::find($id);
        $permission->name = $request->post('name');
        $permission->slug = $request->post('slug');
        $permission->description = $request->post('description');
        $permission->save();
        return redirect('admin/role-list')->with('success','Role updated Successfully!');
	}

    public function show(Request $request,$id){
        if($request->user()->can('role-show')){
        $role = Role::find($id);
        return view('admin.role.show',compact('role'));
    }else{
        return "You don't have permission please contact to administrator";
    }
}

    //  Delete
    public function delete(Request $request,$id){
        if($request->user()->can('role-delete')){
        $user = Role::find($id)->delete();
        return redirect('admin/role-list')->with('success','Role Deleted Successfully!');
    }else{
        return "You don't have permission please contact to administrator";
    }
}
}

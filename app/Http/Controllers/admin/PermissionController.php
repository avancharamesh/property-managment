<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;

use DataTables;

class PermissionController extends Controller
{
	// List
    public function list(Request $request){
        if($request->user()->can('permission-show')){
           return view('admin.permission.list'); 
        }else{
            return "You don't have permission please contact to administrator";
        }
		
	}
	public function lists(Request $request){
        if($request->user()->can('permission-show')){
		if ($request->ajax()) {
            $data = Permission::select('*');
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
     
                           $btn = "<a href='". route('admin.permission.edit', [$row->id])."' class='bg-green btn-sm'><i class='fa fa-pencil' aria-hidden='true'></i></a>
                           <a href='". route('admin.permission.show', [$row->id])."' class='bg-green btn-sm ml-1'><i class='fa fa-eye' aria-hidden='true'></i></a>
                           <a href='". route('admin.permission.delete', [$row->id])."' onclick='return confirm(\"Are you sure you want to delete this item?\");' class='bg-green btn-sm ml-1'><i class='fa fa-trash' aria-hidden='true'></i></a>";
       
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
       }
	}

	// add

	public function add(Request $request){
         if($request->user()->can('permission-create')){
		return view('admin.permission.add');
    }else{
        return "You don't have permission please contact to administrator";
    }
	}
	public function store(Request $request){
        
		$validated = $request->validate([
            'name' => 'required',
            'slug' => 'required',
            'description' => 'required'
        ]);
        $name = $request->post('name');
        $slug = $request->post('slug');
        $description = $request->post('description');
        $permission = Permission::create([
                'name' => $name,
                'slug' => $slug,
                'description' => $description
            ]);
        return redirect('admin/permission-list')->with('success','Permission created Successfully!');
	
}

	// Edit
	public function edit(Request $request,$id){
		$permission = Permission::find($id);
        if($request->user()->can('permission-edit')){
		return view('admin.permission.edit',compact('permission'));
	}else{
        return "You don't have permission please contact to administrator";
    }
}

	public function update($id,Request $request){
		$validated = $request->validate([
            'name' => 'required',
            'slug' => 'required',
            'description' => 'required'
        ]);
        $permission = Permission::find($id);
        $permission->name = $request->post('name');
        $permission->slug = $request->post('slug');
        $permission->description = $request->post('description');
        $permission->save();
        return redirect('admin/permission-list')->with('success','Permission updated Successfully!');
	}

	// Show

	public function show(Request $request,$id){
        if($request->user()->can('permission-show')){
		$permission = Permission::find($id);
		return view('admin.permission.show',compact('permission'));
	}else{
        return "You don't have permission please contact to administrator";
    }
}
	//  Delete
    public function delete(Request $request,$id){
        if($request->user()->can('permission-delete')){
        $user = Permission::find($id)->delete();
        return redirect('admin/permission-list')->with('success','Permission Deleted Successfully!');
    }else{
        return "You don't have permission please contact to administrator";
    }
}
}

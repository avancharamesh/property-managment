<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\DocumentType;
use DataTables;
use Auth;
use DB;
class DocumentTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        return view('admin.documenttype.list');
    }
    public function index(Request $request)
    {
        if ($request->ajax()) {
            
            $data = DocumentType::select('*')->where(['is_deleted'=>'0'])->get();
           
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('status',function($row){
                        $sts = ($row->status == 1)?'<span class="badge badge-success bg-green">Active</span>':'<span class="badge badge-danger">In-Active</span>';
                        return $sts;
                    })
                   
                    ->addColumn('action', function($row){
     
                     $btn = "<a href='". route('admin.documenttype.edit', [$row->id])."' class='bg-green btn-sm'><i class='fa fa-pencil' aria-hidden='true'></i></a>
                         <a href='". route('admin.documenttype.show', [$row->id])."' class='bg-green btn-sm ml-1'><i class='fa fa-eye' aria-hidden='true'></i></a>
                           <a href='". route('admin.documenttype.delete', [$row->id])."' onclick='return confirm(\"Are you sure you want to delete this item?\");' class='bg-green btn-sm ml-1'><i class='fa fa-trash' aria-hidden='true'></i></a>";
       
                            return $btn;
                    })
                    ->rawColumns(['action','status'])
                    ->make(true);
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->hasRole('super-admin')){
        return view('admin.documenttype.create');
    }else{
        "<h1>You don't have permission please contact to administrator</h1>";

    }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

        'name'=>'required|unique:document_types',
        'status'=>'required'    
        ]);

        $document=new DocumentType();
        $document->name=$request->name;
        $document->status=$request->status;
        $document->save();
        return redirect('admin/document-type-list')->with('success','Document Created Successfully');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Auth::user()->hasRole('super-admin')){
        $document=DocumentType::find($id);
        return view('admin.documenttype.show',['document'=>$document]);
    }else{
        "<h1>You don't have permission please contact to administrator</h1>";
    }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->hasRole('super-admin')){
        $document=DocumentType::find($id);
        return view('admin.documenttype.edit',['document'=>$document]);
        }else{
            return "<h1>You don't have permission please contact to administrator</h1>";
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

        'name'=>'required',
        'status'=>'required'
        ]);

        $count = DB::table('document_types')
                        ->where('name','=',$request->name)
                        ->where('id','!=',$id)
                        ->where('is_deleted','=',0)
                        ->count();
            if($count > 0){
                return redirect()->back()->with('error', 'DocumentType Already exist'); 
            }

        $document=DocumentType::find($id);
        $document->name=$request->name;
        $document->status=$request->status;
        $document->save();
        return redirect('admin/document-type-list')->with('success','Document Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $document=DocumentType::find($id);
        $document->is_deleted = 1;
        $document->save();
        return back()->with('success','Document Deleted Successfully');
    }
}

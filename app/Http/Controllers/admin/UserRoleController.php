<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\User;
use App\Models\Role;

class UserRoleController extends Controller
{
    public function list(){
        $data = DB::table('users_roles')
                    ->select('users_roles.user_id','users.name as user_name','roles.name as role_name')
                    ->join('roles','roles.id','=','users_roles.role_id')
                    ->join('users', 'users.id','=','users_roles.user_id')
                    ->get();
        return view('admin.user-role.list',compact('data'));
    }
    
    public function add(){
        $user = User::all();
        $role = Role::all();
        return view('admin.user-role.add',compact(['user','role']));
    }

    public function store(Request $request){
        $validated = $request->validate([
            'user_id' => 'required',
            'role_id' => 'required'
        ]);
        $user_id = $request->post('user_id');
        $role_id = $request->post('role_id');
        if(DB::table('users_roles')->where('user_id','=',$user_id)->count() > 0){
            return redirect()->back()->with('error','Role already assigned');
        }else{
            $role_permission = DB::table('users_roles')->insert([
                                  'user_id' => $user_id, 
                                  'role_id' => $role_id
                                ]);
        }
        
        return redirect('admin/user-role-list')->with('success','Role assigned Successfully!');
    }

    public function edit($id){
        $user = User::where('id','=',$id)->get();
        $role = Role::all();
        $user_role = DB::table('users_roles')->where('user_id','=',$id)->get();
        return view('admin.user-role.edit',compact(['user','role','user_role']));
    }

    public function update($id,Request $request){
		$validated = $request->validate([
            'user_id' => 'required',
            'role_id' => 'required'
        ]);
        $user_id = $request->post('user_id');
        $role_id = $request->post('role_id');
        $rolePermission = DB::table('users_roles')
                              ->where('user_id', $user_id)
                              ->update(['role_id' => $role_id]); 
        return redirect('admin/user-role-list')->with('success','Role updated Successfully!');
	}

    public function show($id){
        $user = User::where('id','=',$id)->get();
        $role = Role::all();
        $user_role = DB::table('users_roles')->where('user_id','=',$id)->get();
        return view('admin.user-role.show',compact(['role','user_role','user']));
    }

    //  Delete
    // public function delete($id){
    //     $user = Role::find($id)->delete();
    //     return redirect('admin/user-role-list')->with('success','Role Deleted Successfully!');
    // }
}

<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PropertyDetails;
use App\Models\User;
use DB;
use Auth;
class DashboardController extends Controller
{
    //
    public function index(){
        if(Auth::user()->hasRole('super-admin')){
            $no_of_document = DB::table('document_types')->where(['is_deleted'=>0,'status'=>1])->count();
            $result=DB::SELECT ("SELECT (SELECT COUNT(id) FROM property_details where property_details.is_deleted=0 AND property_details.village_id ) as total_properties, (SELECT COUNT('id') FROM property_details where property_details.is_document=1 AND property_details.village_id AND property_details.uploaded_documents >=$no_of_document ) as document_count, (SELECT COUNT(id) FROM property_details where property_details.uploaded_documents < $no_of_document AND property_details.uploaded_documents!=0 ) as partail_document_count,(SELECT COUNT(id) FROM property_details where property_details.uploaded_documents=0 OR property_details.uploaded_documents='') as non_document_count,(SELECT COUNT(id) FROM property_details where property_details.is_litigated=1 AND property_details.village_id ) as litigate_count,(SELECT  COUNT(id) FROM property_details  where property_details.is_litigated=0 AND property_details.village_id ) as nonlitigate_count;");
        }else{
            $no_of_document = DB::table('document_types')->where(['is_deleted'=>0,'status'=>1])->count();
         $user_id = Auth::user()->id;
            $where = ['user_id' => $user_id];
            $village = DB::table('user_villages')
                          ->select('village_id')
                          ->where($where)->get();
            $villageArray = [];
            foreach ($village as $v) {
                array_push($villageArray, $v->village_id);
            }
             $user_id = Auth::user()->id;
     $where = ['user_id' => $user_id];
     $sector = DB::table('user_sectors')
     ->select('sector_id')
     ->where($where)->get();
     $sectorArray = [];
     foreach ($sector as $sec) {
        array_push($sectorArray, $sec->sector_id);
        
    }
    $user_id = Auth::user()->id;
     $where = ['user_id' => $user_id];
     $subsector = DB::table('user_subsectors')
     ->select('subsector_id')
     ->where($where)->get();
     $subsectorArray = [];
     foreach ($subsector as $sub) {
        array_push($subsectorArray, $sub->subsector_id);
        
    }
     $user_id = Auth::user()->id;
     $where = ['user_id' => $user_id];
     $properties = DB::table('user_properties')
     ->select('property_type_id')
     ->where($where)->get();
     $propertyArray = [];
     foreach ($properties as $prop) {
        array_push($propertyArray, $prop->property_type_id);
        
    }
    
    
            $ids=join("','",$villageArray);
            $sec_ids=join("','",$sectorArray);
            $sub_ids=join("','",$subsectorArray);
            $prop_ids=join("','",$propertyArray);
    	$result=DB::SELECT ("SELECT (SELECT COUNT(id) FROM property_details where property_details.is_deleted=0 AND property_details.village_id IN('$ids') AND property_details.sector_id IN('$sec_ids') AND property_details.subsector_id IN('$sub_ids') AND property_details.property_type_id IN('$prop_ids')) as total_properties, (SELECT COUNT('id') FROM property_details where property_details.uploaded_documents>=$no_of_document AND property_details.village_id IN('$ids') AND property_details.sector_id IN('$sec_ids') AND property_details.subsector_id IN('$sub_ids') AND property_details.property_type_id IN('$prop_ids')) as document_count, (SELECT COUNT(id) FROM property_details where property_details.uploaded_documents<$no_of_document AND property_details.uploaded_documents!=0 AND property_details.village_id IN('$ids') AND property_details.sector_id IN('$sec_ids') AND property_details.subsector_id IN('$sub_ids') AND property_details.property_type_id IN('$prop_ids')) as partail_document_count,(SELECT COUNT(id) FROM property_details where  property_details.uploaded_documents=0 AND property_details.village_id IN('$ids') AND property_details.sector_id IN('$sec_ids') AND property_details.subsector_id IN('$sub_ids') AND property_details.property_type_id IN('$prop_ids')) as non_document_count,(SELECT COUNT(id) FROM property_details where property_details.is_litigated=1 AND property_details.village_id IN('$ids') AND property_details.sector_id IN('$sec_ids') AND property_details.subsector_id IN('$sub_ids') AND property_details.property_type_id IN('$prop_ids')) as litigate_count,(SELECT  COUNT(id) FROM property_details  where property_details.is_litigated=0 AND property_details.village_id IN('$ids') AND property_details.sector_id IN('$sec_ids') AND property_details.subsector_id IN('$sub_ids') AND property_details.property_type_id IN('$prop_ids')) as nonlitigate_count;");
    }

    	$chartData="";
        $mychartData="";
    	$total_properties="Total Properties";
    	$document_count=" fully Documented";
    	$partail_document_count="Partially Documented";
        $nondocument_count = "No Documented";
    	$litigate_count="Litigated";
    	$nonlitigate_count="Not Litigated";
    	foreach ($result as $list) {

    		$chartData.="['".$document_count."', ".$list->document_count."],['".$partail_document_count."', ".$list->partail_document_count."],['".$nondocument_count."', ".$list->non_document_count."],";
            $mychartData.="['".$litigate_count."',".$list->litigate_count."],['".$nonlitigate_count."',".$list->nonlitigate_count."],";
    		 // print_r($chartData);die;
    		$arr['chartData']=rtrim($chartData,",");
            
    	}
        return view('admin.dashboard',$arr,['dashboard'=>$result,'mychartData'=>$mychartData]);
    }
    public function pieChart(Request $request){
    	

    }
}

<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\LableName;
use DB;
use Auth;
class LabelController extends Controller
{
    public function label()
    {
    	$data=DB::table('lable_names')->where('id',1)->get();
    	return view('admin.label.list',['label_list'=>$data]);
    }
    public function create()
    {
        if(Auth::user()->hasRole('super-admin')){
    	return view('admin.label.create');
        }else{
            "<h1>You don't have permission please contact to administrator</h1>";
        }
    }
    public function store(Request $request)
    {
    	$request->validate([

    		'village'=>'required',
    		'sector'=>'required',
    		'sub_sector'=>'required',
    		'property_type'=>'required',
    		'property_name'=>'required'
    	]);

    	$label=new LableName();
    	$label->village=$request->village;
    	$label->sector=$request->sector;
    	$label->sub_sector=$request->sub_sector;
    	$label->property_type=$request->property_type;
    	$label->property_name=$request->property_name;
    	$label->save();
    	return redirect('admin/label-list')->with('success','Label created Successfully');
    }

    public function edit(Request $request,$id)
    {
    	$label=LableName::find($id);
        return view('admin.label.edit',compact('label'));
    }
    public function update(Request $request,$id)
    {
    	$request->validate([

    		'village'=>'required',
    		'sector'=>'required',
    		'sub_sector'=>'required',
    		'property_type'=>'required',
    		'property_name'=>'required'
    	]);

    	$label=LableName::find($id);
    	$label->village=$request->village;
    	$label->sector=$request->sector;
    	$label->sub_sector=$request->sub_sector;
    	$label->property_type=$request->property_type;
    	$label->property_name=$request->property_name;
    	$label->save();
    	return redirect('admin/label-list')->with('success','Label Updated Successfully');
    }
}

<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use  App\Models\Village;
use App\Models\SubSector;
use App\Models\Sector;
use DB;
use validate;
use DataTables;
use App\Models\VillageSector;
use Auth;

class SubSectorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {   
        
        return view('admin.subsector.list');
        
    }
    public function index(Request $request)
    {
        if ($request->ajax()) {
             $data = DB::select('SELECT sub.id,sub.name,sub.description,sub.image, sub.status,vlg.name as village,sec.name as sector from sub_sectors as sub LEFT JOIN   villages as vlg  on vlg.id = sub.village_id left join sectors as sec on sec.id=sub.sector_id  WHERE   sub.is_deleted = 0');
           

           
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('status',function($row){
                        $sts = ($row->status == 1)?'<span class="badge badge-success bg-green">Active</span>':'<span class="badge badge-danger">In-Active</span>';
                        return $sts;
                    })
                    ->addColumn('image',function($row){
                        $image = "<img src='".asset('uploads/subsectors').'/'.$row->image."' height='40px' width='40px'>";
                        return $image;
                    })
                    ->addColumn('action', function($row){
     
                           $btn = "<div class='btn-group'>
                           <a href='". route('admin.subsector.edit', [$row->id])."' class='bg-green btn-sm'><i class='fa fa-pencil' aria-hidden='true'></i></a>
                           <a href='". route('admin.subsector.show', [$row->id])."' class='bg-green btn-sm ml-1'><i class='fa fa-eye' aria-hidden='true'></i></a></div>";
       
                            return $btn;
                    })
                    ->rawColumns(['action','status','image'])
                    ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(Auth::user()->hasRole('super-admin')){

            $data['village'] = DB::table('villages')->where(['is_deleted'=>0])->get();
            $getdata['sector'] = DB::table('sectors')->where(['is_deleted'=>0])->get();
            return view('admin.subsector.create',$data,$getdata);
        }else{
        $id=Auth::User()->id;
        $data['village'] = DB::table('villages')->join('user_villages','villages.id','=','user_villages.village_id')->select('villages.*')
         ->where('user_villages.user_id',$id)->where(['is_deleted'=>0])->get();
        $getdata['sector'] = DB::table('sectors')->join('user_sectors','sectors.id','=','user_sectors.sector_id')->select('sectors.*')
        ->where('user_sectors.user_id',$id)->where(['is_deleted'=>0])->get();
        return view('admin.subsector.create',$data,$getdata);
    }
        
    }
    public function sector(Request $request,$id)
    {
        if(Auth::user()->hasRole('super-admin')){
            $sector_id = DB::table('sectors')->where('village_id',$id)->get();
            return json_encode($sector_id);
        }else{
        $user_id = Auth::User()->id;
         $sector_id = DB::table('sectors')->join('user_sectors','sectors.id','=','user_sectors.sector_id')->select('sectors.*')->where('user_sectors.user_id',$user_id)
                    ->where("village_id",$id)->get();
        return json_encode($sector_id);
    }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateDataField = [

         'village_id'=>'required',
         'sector_id'=>'required',   
         'name' => 'required',
         'description' => 'required',
         'status' => 'required'

        ];
        if($request->file('image')){
            $validateDataField['image'] = 'required | mimes:jpeg,jpg,bmp,png,gif';
        }
        $request->validate($validateDataField);
        $sub_sector=new SubSector();
        $sub_sector->village_id=$request->village_id;
        $sub_sector->sector_id=$request->sector_id;
        $sub_sector->name=$request->name;
        if($request->file('image')){
        $imageFile = time().'.'.$request->file('image')->extension(); 
        $request->file('image')->move(public_path('uploads/subsectors'), $imageFile);
        $sub_sector->image = $imageFile;
    }
        $sub_sector->description=$request->description;
        $sub_sector->status=$request->status;
        $sub_sector->save();
        return redirect('admin/subsector-list')->with('success','SubSector created Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {   
       if(Auth::user()->hasRole('super-admin')){
            $sub_sector=SubSector::find($id);
            $village = DB::table('villages')->where(['is_deleted'=>0])->get();
            $sector = Sector::select('*')->where(['is_deleted'=>0])->get();
            return view('admin.subsector.show',['sub_sector'=>$sub_sector,'village'=>$village,'sector'=>$sector]);
        }else{
        $user_id=Auth::User()->id;
        $sub_sector=SubSector::find($id);
        $village = DB::table('villages')->join('user_villages','villages.id','=','user_villages.village_id')->select('villages.*')
         ->where('user_villages.user_id',$user_id)->where(['is_deleted'=>0])->get();
        $sector = Sector::select('*')->where(['is_deleted'=>0])->get();
        return view('admin.subsector.show',['sub_sector'=>$sub_sector,'village'=>$village,'sector'=>$sector]);

       } 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        if(Auth::user()->hasRole('super-admin')){
            $sub_sector=SubSector::find($id);
            $village = DB::table('villages')->where(['is_deleted'=>0])->get();
            $sector = Sector::select('*')->where(['is_deleted'=>0])->get();
            return view('admin.subsector.edit',['sub_sector'=>$sub_sector,'village'=>$village,'sector'=>$sector]);
        }else{
        $user_id=Auth::User()->id;
        $sub_sector=SubSector::find($id);
        $village = DB::table('villages')->join('user_villages','villages.id','=','user_villages.village_id')->select('villages.*')
         ->where('user_villages.user_id',$user_id)->where(['is_deleted'=>0])->get();
        $sector = DB::table('sectors')->join('user_sectors','sectors.id','=','user_sectors.sector_id')->select('sectors.*')->where('user_sectors.user_id',$user_id)->where(['is_deleted'=>0])->get();
        return view('admin.subsector.edit',['sub_sector'=>$sub_sector,'village'=>$village,'sector'=>$sector]);

    }
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

         'village_id'=>'required',
         'sector_id'=>'required',   
         'name' => 'required',
         'image' => 'nullable | mimes:jpeg,jpg,bmp,png,gif',
         'description' => 'required',
         'status' => 'required'

        ]);

        $sub_sector=SubSector::find($id);
        $sub_sector->village_id=$request->village_id;
        $sub_sector->sector_id=$request->sector_id;
        $sub_sector->name=$request->name;
        if( $request->file('image')){
        $imageFile = time().'.'.$request->file('image')->extension(); 
        $request->file('image')->move(public_path('uploads/subsectors'), $imageFile);
        $sub_sector->image = $imageFile;
    }
        $sub_sector->description=$request->description;
        $sub_sector->status=$request->status;
        $sub_sector->save();
        return redirect('admin/subsector-list')->with('success','SubSector Updated Successfully!');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
                $sub_sector=SubSector::find($id);
        $sub_sector->is_deleted = 1;
        $sub_sector->save();
        return back()->with('success','SubSector Deleted Successfully');
       
    }
}

<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\PropertyType;

class UserPropertyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $data = [];
        $user_role = DB::table('user_properties')
                 ->select('user_properties.user_id','users.name')
                 ->join('users','users.id','=','user_properties.user_id')
                 ->distinct('user_properties.user_id')
                 ->get();
        foreach($user_role as $user){
            $permission = DB::table('user_properties')
                        ->select('property_types.name')
                        ->join('property_types','user_properties.property_type_id','=','property_types.id')
                        ->where('user_properties.user_id','=',$user->user_id)->where('is_deleted',0)
                        ->get();
            $temp_data = [
                           "user_name" => $user->name,
                           "user_id"   => $user->user_id,
                           "permission" => $permission
                         ];
            array_push($data, $temp_data);
        }
        return view('admin.user_properties.list',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $user = User::all();
        $property = PropertyType::join('villages','villages.id','=','property_types.village_id')
                                 ->join('sectors','sectors.id','=','property_types.sector_id')
                                 ->join('sub_sectors','sub_sectors.id','=','property_types.subsector_id')
                                 ->select('property_types.*','villages.name as village_name','sectors.name as sector_name','sub_sectors.name as subsector_name')
                                 ->get();

        return view('admin.user_properties.create',compact(['user','property']));
        
    }

    public function UserPropertyList($id)
    {
         $property = PropertyType::join('sub_sectors','sub_sectors.id','=','property_types.subsector_id')
                    ->join('user_subsectors','user_subsectors.subsector_id','=','sub_sectors.id')
                    ->join('villages','villages.id','=','property_types.village_id')
                    ->join('sectors','sectors.id','=','property_types.sector_id')
                    ->join('users','users.id','=','user_subsectors.user_id')
                    ->select('property_types.*','sub_sectors.name as subsector_name','villages.name as village_name','sectors.name as sector_name')
                    ->where('users.id',$id)
                    ->where('property_types.is_deleted',0)
                    ->get();
                    return json_encode($property);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'user' => 'required',
            'property' => 'required',
        ]);

        $user = $request->post('user');
        $property = $request->post('property');
        if(DB::table('user_properties')->where('user_id','=',$user)->count() > 0){
            return redirect()->back()->with('error', 'Role Already exists.'); 
        }else{
            foreach($property as $perm){
               DB::table('user_properties')->insert([
                        'user_id' => $user,
                        'property_type_id' => $perm
                    ]);
            }
        }   
        return redirect('admin/user-properties-list')->with('success','User Properties Permission Created Successfully!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $user = DB::table('users')->where('id','=',$id)->get();
       $property = PropertyType::join('sub_sectors','sub_sectors.id','=','property_types.subsector_id')
                    ->join('user_subsectors','user_subsectors.subsector_id','=','sub_sectors.id')
                    ->join('villages','villages.id','=','property_types.village_id')
                    ->join('users','users.id','=','user_subsectors.user_id')
                    ->select('property_types.*','sub_sectors.name as subsector_name','villages.name as village_name')
                    ->where('users.id',$id)
                    ->where('property_types.is_deleted',0)
                    ->get();
        $propertyAssign = DB::table('user_properties')->where('user_id','=',$id)->get();
        $propertyAssignId = [];
        foreach ($propertyAssign as $pa) {
            array_push($propertyAssignId,$pa->property_type_id);
        }
        return view('admin.user_properties.show',compact('user','property','propertyAssignId'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $user = DB::table('users')->where('id','=',$id)->get();
       $property = PropertyType::join('sub_sectors','sub_sectors.id','=','property_types.subsector_id')
                    ->join('user_subsectors','user_subsectors.subsector_id','=','sub_sectors.id')
                    ->join('villages','villages.id','=','property_types.village_id')
                    ->join('sectors','sectors.id','=','property_types.sector_id')
                    ->join('users','users.id','=','user_subsectors.user_id')
                    ->select('property_types.*','sub_sectors.name as subsector_name','villages.name as village_name','sectors.name as sector_name')
                    ->where('users.id',$id)
                    ->where('property_types.is_deleted',0)
                    ->get();
        $propertyAssign = DB::table('user_properties')->where('user_id','=',$id)->get();
        $propertyAssignId = [];
        foreach ($propertyAssign as $pa) {
            array_push($propertyAssignId,$pa->property_type_id);
        }
        return view('admin.user_properties.edit',compact('user','property','propertyAssignId'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $validated = $request->validate([
            'user' => 'required',
            
        ]);
        $userObject=User::find($request->post('user'));

        $property=$request->post('property');
        if(!$property){
            DB::table('user_properties')->where('user_id','=',$id)->delete();            
            return redirect('admin/user-properties-list')->with('success','User Properties Permission Deleted Successfully!');
        }
        $AssignedProperty = DB::table('user_properties')->where('user_id','=',$id)->get();
        $propertyAssignId = [];
            foreach($AssignedProperty as $pro){
                
                array_push($propertyAssignId, $pro->property_type_id);
            }

            $propertyForDetach = array_diff($propertyAssignId, $property);
            $propertyForAttach = array_diff($property, $propertyAssignId);
            // print_r($villageForAttach);
            // dd($villageForDetach);
            if(!empty($propertyForAttach)){
                foreach ($propertyForAttach as $pro) {   
                  DB::table('user_properties')->insert([
                    "user_id" => $id,
                    "property_type_id" => $pro
                  ]);
           }
        }

        if(!empty($propertyForDetach)){
               DB::table('user_properties')->whereIn('property_type_id',$propertyForDetach)->where('user_id','=',$id)->delete();            
        }

           
        return redirect('admin/user-properties-list')->with('success','User Properties Permission Updated Successfully!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

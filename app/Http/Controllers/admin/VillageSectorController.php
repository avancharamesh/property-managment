<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Village;
use App\Models\Sector;
use DB;

class VillageSectorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $village_role = DB::table('village_sectors')
                 ->select('village_sectors.village_id','villages.name')
                 ->join('villages','villages.id','=','village_sectors.village_id')
                 ->distinct('villages.village_id')
                 ->get();
        foreach($village_role as $village){
            $permission = DB::table('village_sectors')
                        ->select('sectors.name')
                        ->join('sectors','village_sectors.sector_id','=','sectors.id')
                        ->where('village_sectors.village_id','=',$village->village_id)
                        ->get();
            $temp_data = [
                           "name" => $village->name,
                           "village_id"   => $village->village_id,
                           "permission" => $permission
                         ];
            array_push($data, $temp_data);
        }
        return view('admin.village_sectors.list',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $village = Village::all();
        $sector = Sector::all();
        return view('admin.village_sectors.create',compact(['village','sector']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
         $validated = $request->validate([
            'village' => 'required',
            'sector' => 'required',
        ]);

        $village = $request->post('village');
        $sector = $request->post('sector');
        if(DB::table('village_sectors')->where('village_id','=',$village)->count() > 0){
            return redirect()->back()->with('error', 'Role Already exists.'); 
        }else{
            foreach($sector as $perm){
               DB::table('village_sectors')->insert([
                        'village_id' => $village,
                        'sector_id' => $perm
                    ]);
            }
        }   
        return redirect('admin/village-sector-list')->with('success','User Village Permission Created Successfully!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $village = DB::table('villages')->where('id','=',$id)->get();
        $sector = Sector::all();
        $sectorAssign = DB::table('village_sectors')->where('village_id','=',$id)->get();
        $sectorAssignId = [];
        foreach ($sectorAssign as $pa) {
            array_push($sectorAssignId,$pa->sector_id);
        }
        return view('admin.village_sectors.show',compact(['village','sector','sectorAssignId']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
         $village = DB::table('villages')->where('id','=',$id)->get();
        $sector = Sector::all();
        $sectorAssign = DB::table('village_sectors')->where('village_id','=',$id)->get();
        $sectorAssignId = [];
        foreach ($sectorAssign as $pa) {
            array_push($sectorAssignId,$pa->sector_id);
        }
        return view('admin.village_sectors.edit',compact(['village','sector','sectorAssignId']));
        
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'village' => 'required',
            'sector' => 'required',
        ]);
        $VillageObject=Village::find($request->post('village'));

        $sector=$request->post('sector');
        $AssignedSector = DB::table('village_sectors')->where('village_id','=',$id)->get();
        $sectorAssignId = [];
            foreach($AssignedSector as $sec){
            
                array_push($sectorAssignId, $sec->sector_id);
            }

            $sectorForDetach = array_diff($sectorAssignId, $sector);
            $sectorForAttach = array_diff($sector, $sectorAssignId);
            // print_r($villageForAttach);
            // dd($villageForDetach);
            if(!empty($sectorForAttach)){
                foreach ($sectorForAttach as $sec) {   
                  DB::table('village_sectors')->insert([
                    "village_id" => $id,
                    "sector_id" => $sec
                  ]);
           }
        }

        if(!empty($sectorForDetach)){
            
               DB::table('village_sectors')->whereIn('sector_id',$sectorForDetach)->where('village_id','=',$id)->delete();            
        }

           
        return redirect('admin/village-sector-list')->with('success','Village Sector Permission Updated Successfully!');
    
   }

}

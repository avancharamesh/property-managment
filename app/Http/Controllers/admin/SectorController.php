<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use  App\Models\Village;
use App\Models\Sector;
use DB;
use validate;
use DataTables;
use Auth;
class SectorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request){
        
        return view('admin.sector.list');
       
    }
    public function index(Request $request)
    {
        if ($request->ajax()) {
            if(Auth::user()->hasRole('super-admin')){
                $data = DB::select('SELECT cat.id,cat.name,cat.description,cat.image, cat.status,cats.name as category from sectors as cat LEFT JOIN   villages as cats on cats.id = cat.village_id WHERE  cat.is_deleted = 0');
            }else{
             $data = DB::select('SELECT cat.id,cat.name,cat.description,cat.image, cat.status,cats.name as category from sectors as cat LEFT JOIN   villages as cats on cats.id = cat.village_id WHERE  cat.is_deleted = 0');
            }
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('status',function($row){
                        $sts = ($row->status == 1)?'<span class="badge badge-success bg-green">Active</span>':'<span class="badge badge-danger">In-Active</span>';
                        return $sts;
                    })
                    ->addColumn('image',function($row){
                        $image = "<img src='".asset('uploads/subcategory').'/'.$row->image."' height='40px' width='40px'>";
                        return $image;
                    })
                    ->addColumn('action', function($row){
     
                           $btn = "<div class='btn-group'>
                           <a href='". route('admin.sector.edit', [$row->id])."' class='bg-green btn-sm'><i class='fa fa-pencil' aria-hidden='true'></i></a>
                           <a href='". route('admin.sector.show', [$row->id])."' class='bg-green btn-sm ml-1'><i class='fa fa-eye' aria-hidden='true'></i></a></div>";
       
                            return $btn;
                    })
                    ->rawColumns(['action','status','image'])
                    ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(Auth::user()->hasRole('super-admin')){
            $data['village'] = DB::table('villages')->where(['is_deleted'=>0])->get();
             return view('admin.sector.create',$data);
        }else{
        $id=Auth::User()->id;
        $data['village'] = DB::table('villages')->join('user_villages','villages.id','=','user_villages.village_id')->select('villages.*')
         ->where('user_villages.user_id',$id)->where(['is_deleted'=>0])->get();   
        return view('admin.sector.create',$data);
    }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $requeste
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $validateDatafield = [

         'village_id'=>'required',   
         'name' => 'required',
         'description' => 'required',
         'status' => 'required'

        ];
        if($request->file('image')){
            $validateDatafield['image'] = 'required | mimes:jpeg,jpg,bmp,png,gif';
        }

        $sector=new Sector();
        $sector->village_id=$request->village_id;
        $sector->name=$request->name;
        if($request->file('image')){
        $imageFile = time().'.'.$request->file('image')->extension(); 
        $request->file('image')->move(public_path('uploads/subcategory'), $imageFile);
        $sector->image = $imageFile;
    }
        $sector->description=$request->description;
        $sector->status=$request->status;
        $sector->save();
        return redirect('admin/sector-list')->with('success','Sector created Successfully!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        
        if(Auth::user()->hasRole('super-admin')){
            $sector = Sector::find($id);
            $village = DB::table('villages')->where(['is_deleted'=>0])->get();
            return view('admin.sector.edit',['sector'=>$sector,'village'=>$village]);
        }else{
        $user_id=Auth::User()->id; 
        $sector = Sector::find($id);
        $village = DB::table('villages')->join('user_villages','villages.id','=','user_villages.village_id')->select('villages.*')
         ->where('user_villages.user_id',$user_id)->where(['is_deleted'=>0])->get();
        return view('admin.sector.edit',['sector'=>$sector,'village'=>$village]);
    }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        if(Auth::user()->hasRole('super-admin')){
            $sector = Sector::find($id);
            $village = DB::table('villages')->where(['is_deleted'=>0])->get();
            return view('admin.sector.edit',['sector'=>$sector,'village'=>$village]);
        }else{
        $user_id=Auth::User()->id; 
        $sector = Sector::find($id);
        $village = DB::table('villages')->join('user_villages','villages.id','=','user_villages.village_id')->select('villages.*')
         ->where('user_villages.user_id',$user_id)->where(['is_deleted'=>0])->get();
        return view('admin.sector.edit',['sector'=>$sector,'village'=>$village]);
    }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
        'village_id' => 'required',   
         'name' => 'required',
         'image' => 'nullable | mimes:jpeg,jpg,bmp,png,gif',
         'description' => 'required',
         'status' => 'required'
       ]);
            $sector=Sector::find($id);
            $sector->village_id=$request->village_id;
            $sector->name=$request->name;
            if( $request->file('image')){
            $imageFile = time().'.'.$request->file('image')->extension(); 
            $request->file('image')->move(public_path('uploads/subcategory'), $imageFile);
            $sector->image = $imageFile;
        }

            $sector->description=$request->description;
            $sector->status=$request->status;
            $sector->save();
             return redirect('admin/sector-list')->with('success','Sector Updated Successfully');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
       
         $sector=Sector::find($id);
        $sector->is_deleted = 1;
        $sector->save();
        return back()->with('success','Sector Deleted Successfully');
       
    }
}

<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Models\User;
use App\Models\SubSector;
use App\Models\UserSubsector;
class UserSubsectorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $user_role = DB::table('user_subsectors')
                 ->select('user_subsectors.user_id','users.name')
                 ->join('users','users.id','=','user_subsectors.user_id')
                 ->distinct('user_subsectors.user_id')
                 ->get();
        foreach($user_role as $user){
            $permission = DB::table('user_subsectors')
                        ->select('sub_sectors.name')
                        ->join('sub_sectors','user_subsectors.subsector_id','=','sub_sectors.id')
                        ->where('user_subsectors.user_id','=',$user->user_id)->where('is_deleted',0)
                        ->get();
            $temp_data = [
                           "user_name" => $user->name,
                           "user_id"   => $user->user_id,
                           "permission" => $permission
                         ];
            array_push($data, $temp_data);
        }
        return view('admin.user_subsectors.list',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $user = User::all();
        $subsector = SubSector::join('villages','villages.id','=','sub_sectors.village_id')->join('sectors','sectors.id','=','sub_sectors.sector_id')->select('sub_sectors.*','villages.name as village_name','sectors.name as sector_name')->get();
        return view('admin.user_subsectors.create',compact(['user','subsector']));
        
    }

    public function UserSubCreateList($id)
    {
        
        $subsector = SubSector::join('sectors','sectors.id','=','sub_sectors.sector_id')
                    ->join('user_sectors','user_sectors.sector_id','=','sectors.id')
                    ->join('villages','villages.id','=','sub_sectors.village_id')
                    ->join('users','users.id','=','user_sectors.user_id')
                    ->select('sub_sectors.*','sectors.name as sector_name','villages.name as village_name')
                    ->where('users.id',$id)
                    ->where('sub_sectors.is_deleted',0)
                    ->get();
         return json_encode($subsector);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
           $validated = $request->validate([
            'user' => 'required',
            'subsector' => 'required',
        ]);

        $user = $request->post('user');
        $subsector = $request->post('subsector');
        if(DB::table('user_subsectors')->where('user_id','=',$user)->count() > 0){
            return redirect()->back()->with('error', 'Role Already exists.'); 
        }else{
            foreach($subsector as $perm){
               DB::table('user_subsectors')->insert([
                        'user_id' => $user,
                        'subsector_id' => $perm
                    ]);
            }
        }   
        return redirect('admin/user-subsector-list')->with('success','User SubSector Permission Created Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $user = DB::table('users')->where('id','=',$id)->get();
        $subsector = SubSector::join('sectors','sectors.id','=','sub_sectors.sector_id')
                    ->join('user_sectors','user_sectors.sector_id','=','sectors.id')
                    ->join('villages','villages.id','=','sub_sectors.village_id')
                    ->join('users','users.id','=','user_sectors.user_id')
                    ->select('sub_sectors.*','sectors.name as sector_name','villages.name as village_name')
                    ->where('users.id',$id)
                    ->where('sub_sectors.is_deleted',0)
                    ->get();
        $subsectorAssign = DB::table('user_subsectors')->where('user_id','=',$id)->get();
        $subsectorAssignId = [];
        foreach ($subsectorAssign as $pa) {
            array_push($subsectorAssignId,$pa->subsector_id);
        }
        return view('admin.user_subsectors.show',compact('user','subsector','subsectorAssignId'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $user = DB::table('users')->where('id','=',$id)->get();
       $subsector = SubSector::join('sectors','sectors.id','=','sub_sectors.sector_id')
                    ->join('user_sectors','user_sectors.sector_id','=','sectors.id')
                    ->join('villages','villages.id','=','sub_sectors.village_id')
                    ->join('users','users.id','=','user_sectors.user_id')
                    ->select('sub_sectors.*','sectors.name as sector_name','villages.name as village_name')
                    ->where('users.id',$id)
                    ->where('sub_sectors.is_deleted',0)
                    ->get();
        $subsectorAssign = DB::table('user_subsectors')->where('user_id','=',$id)->get();
        $subsectorAssignId = [];
        foreach ($subsectorAssign as $pa) {
            array_push($subsectorAssignId,$pa->subsector_id);
        }
        return view('admin.user_subsectors.edit',compact('user','subsector','subsectorAssignId'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

           $validated = $request->validate([
            'user' => 'required',
            
        ]);
        $userObject=User::find($request->post('user'));
        $subsector=$request->post('subsector');

         if(!$subsector){
            DB::table('user_subsectors')->where('user_id','=',$id)->delete();            
            return redirect('admin/user-subsector-list')->with('success','User SubSector Permission Deleted Successfully!');
        }

        $AssignedSubSector = DB::table('user_subsectors')->where('user_id','=',$id)->get();
        $subsectorAssignId = [];
            foreach($AssignedSubSector as $sub){
            
                array_push($subsectorAssignId, $sub->subsector_id);
            }

            $subsectorForDetach = array_diff($subsectorAssignId, $subsector);
            $subsectorForAttach = array_diff($subsector, $subsectorAssignId);
            // print_r($villageForAttach);
            // dd($villageForDetach);

            if(!empty($subsectorForAttach)){
                
                foreach ($subsectorForAttach as $sub) {   

                  DB::table('user_subsectors')->insert([
                    "user_id" => $id,
                    "subsector_id" => $sub
                  ]);
           }
        }

        if(!empty($subsectorForDetach)){
               DB::table('user_subsectors')->whereIn('subsector_id',$subsectorForDetach)->where('user_id','=',$id)->delete();            
        }

           
        return redirect('admin/user-subsector-list')->with('success','User SubSector Permission Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

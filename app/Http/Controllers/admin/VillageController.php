<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Village;
use DB;
use Exceptions;
use DataTables;
use validate;
use Auth;
class VillageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
           
        return view('admin.village.list');
        
            
        
    }
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $id=Auth::User()->id;
            if(Auth::user()->hasRole('super-admin')){
                $data = DB::table('villages')->where(['is_deleted'=>0])->get();
            }else{
            $data = DB::table('villages')->join('user_villages','villages.id','=','user_villages.village_id')->select('villages.*')
         ->where('user_villages.user_id',$id)->where(['is_deleted'=>0])->get();
           }
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('status',function($row){
                        $sts = ($row->status == 1)?'<span class="badge badge-success bg-green">Active</span>':'<span class="badge badge-danger">In-Active</span>';
                        return $sts;
                    })
                    ->addColumn('image',function($row){
                        $image = "<img src='".asset('uploads/category').'/'.$row->image."' height='40px' width='40px'>";
                        return $image;
                    })
                    ->addColumn('action', function($row){
     
                     $btn = "<a href='". route('admin.village.edit', [$row->id])."' class='bg-green btn-sm'><i class='fa fa-pencil' aria-hidden='true'></i></a>
                         <a href='". route('admin.village.show', [$row->id])."' class='bg-green btn-sm ml-1'><i class='fa fa-eye' aria-hidden='true'></i></a>";
       
                            return $btn;
                    })
                    ->rawColumns(['action','status','image'])
                    ->make(true);
        }
        
    }//'{{ route("admin.stocks.edit", ":id") }}' {{ route('category_path/show', [$category->slug]) }}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        
        $data=Village::select('*')->where(['is_deleted'=>0])->get();
        return view('admin.village.create',['village_list'=>$data]);
    
   
}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
       $validateDataField = [
            
         'name' => 'required|unique:villages',
         'description' => 'required',
         'image'=> 'nullable | mimes:jpeg,jpg,png,gif|max:10000',
         'status' => 'required'
        ];

        if($request->file('image')!=''){
            $validateDataField['image'] =  'required | mimes:jpeg,jpg,bmp,png,gif'; 
        }
        $request->validate($validateDataField);

        $village = new Village();
        if($request->file('image')!=''){
        $imageFile = time().'.'.$request->file('image')->extension(); 
        $request->file('image')->move(public_path('uploads/category'), $imageFile);
        $village->image = $imageFile; 
        }       
        $village->name=$request->name;
        $village->description = $request->description;
        $village->status=$request->status;
        $village->save();
         return redirect('admin/village-list')->with('success','Village created Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {   
        if($request->user()->can('village-show')){
        $village=Village::find($id);
        $parent_village = Village::select('*')->where(['is_deleted'=>0])->get();
        return view('admin.village.show',['village'=>$village,'village_list'=>$parent_village]);
        }else{
            return "You don't have permission please contact to administrator";
        }
   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
       
        
        $village=Village::find($id);
        $parent_village = Village::select('*')->where(['is_deleted'=>0])->get();
        return view('admin.village.edit',['village'=>$village,'category_list'=>$parent_village]);
       
    
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

         $validateDataField = [
            
         'name' => 'required',
         'image'=> 'nullable | mimes:jpeg,jpg,png,gif|max:10000',
         'description' => 'required',
         'status' => 'required'
        ];
         if($request->file('image')!=''){
            $validateDataField['image'] =  'required | mimes:jpeg,jpg,bmp,png,gif'; 
        }

        $request->validate($validateDataField);
                
            // $whereData = array('name'=>$request->name, 'id'=>$id);
            $count = DB::table('villages')
                        ->where('name','=',$request->name)
                        ->where('id','!=',$id)
                        ->where('is_deleted','=',0)
                        ->count();
            if($count > 0){
                return redirect()->back()->with('error', 'Village Already exist'); 
            }
            
            $village=Village::find($id);    
            $village->name=$request->name;
            $village->description = $request->description;
            $village->status=$request->status; 
            if($request->file('image')){
                $imageFile = time().'.'.$request->file('image')->extension(); 
                $request->file('image')->move(public_path('uploads/category'), $imageFile);
                $village->image = $imageFile;
            }    
            $village->save();
         return redirect('admin/village-list')->with('success','Village Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        if($request->user()->can('village-delete')){
        $category=Village::find($id);
        $category->is_deleted = 1;
        $category->save();
        return back()->with('success','Village Deleted Successfully');
        }else{
            return "You don't have permission please contact to administrator";
        }
}
}

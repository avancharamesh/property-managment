<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sector extends Model
{
    use HasFactory;
     function village()
		    {
		    	return $this->belongsTo(Village::class);
		    }
		    public function property(){
    	      return $this->hasMany(property::class);
    }
}

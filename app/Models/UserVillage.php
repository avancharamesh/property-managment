<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserVillage extends Model
{
    use HasFactory;
    use HasUservillagesTrait;
    function village()
		    {
		    	return $this->belongsTo(Village::class);
		    }
		    function sector()
		    {
		    	return $this->belongsTo(Sector::class);
		    }
		    function subsector(){

		    	return $this->belongsTo(subsector::class);
		    }
		    function propertytype(){

		    	return $this->belongsTo(PropertyType::class);
		    }
}

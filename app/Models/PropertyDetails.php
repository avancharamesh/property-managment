<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PropertyDetails extends Model
{
	use HasFactory;

	 protected $fillable = [
        'village_id',
        'sector_id',
        'property_type_id',
        'name',
        'contact',
        'address',
        'status',
    ];
	function village()
	{
		return $this->belongsTo(Village::class);
	}
	function sector()
	{
		return $this->belongsTo(Sector::class);
	}
	function subsector()
	{
		return $this->belongsTo(SubSector::class);
	}
	function PropertyType()
	{
		return $this->belongsTo(PropertyType::class);
	}
	 

}

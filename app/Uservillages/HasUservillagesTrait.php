<?php

namespace App\Uservillages;

use App\Models\Villages;
use App\Models\User;

trait HasUservillagesTrait {

   public function giveVillageTo(... $villages) {

    $villages = $this->getAllVillages($villages);
    dd($villages);
    if($villages === null) {
      return $this;
    }
    $this->villages()->saveMany($villages);
    return $this;
  }

  public function withdrawVillageTo( ... $villages ) {

    $villages = $this->getAllVillages($villages);
    $this->villages()->detach($villages);
    return $this;

  }

  public function refreshVillages( ... $villages ) {

    $this->villages()->detach();
    return $this->giveVillageTo($villages);
  }

  public function hasVillageTo($villages) {

    return $this->hasVillageThroughUser($villages) || $this->hasVillage($villages);
  }

  public function hasVillageThroughRole($villages) {

    foreach ($villages->users as $users){
      if($this->users->contains($user)) {
        return true;
      }
    }
    return false;
  }

  public function hasUser( ... $users ) {

    foreach ($users as $user) {
      if ($this->users->contains('slug', $user)) {
        return true;
      }
    }
    return false;
  }

  public function users() {

    return $this->belongsToMany(User::class,'users_roles');

  }
  public function permissions() {

    return $this->belongsToMany(Villages::class,'users_permissions');

  }
  protected function hasPermission($villages) {

    return (bool) $this->villages->where('slug', $permission->slug)->count();
  }

  protected function getAllPermissions(array $permissions) {

    return Villages::whereIn('slug',$villages)->get();
    
  }

}
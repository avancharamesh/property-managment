@extends('admin.master')
@section('title', 'Document List | Admin')
@section('content')

<div class="right_col" role="main">
   <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>List <small>Document</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                    {{-- @if (Auth::user()->can('permission-create')) --}}
                      <a href="{{url('admin/document-type-create')}}" class="bg-green btn-sm"><i class="fa fa-plus" aria-hidden="true"></i></a>
                    {{-- @endif --}}
                   </ul>
                    <div class="clearfix"></div>
                  </div>
                  @include('flash-message')
                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">
                    <table id="datatable-category" class="table table-striped table-bordered" style="width:100%">
                      <thead>
                        <tr>
                          <th>#SNO.</th>
                          <th>Document Name</th>
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                      </thead>


                      <tbody>
                       
                      </tbody>

                      <tfoot>
                        <tr>
                          <th>#SNO.</th>
                          <th>Document Name</th>
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                      </tfoot>

                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>
</div>
@endsection

@section('script')

<script>
  $(document).ready(function(){
    $("#datatable-category").DataTable({
                "dom": '<"pull-left"l><"pull-right"f>tip',
                responsive: true,
                processing: true,
                serverSide: true,
                ajax: "{{ url('admin/document-type-lists') }}",
                columns: [
                    {"data": 'DT_RowIndex',orderable: false, searchable: false},
                    {data: 'name', name: 'name'},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
  });
</script>
@endsection

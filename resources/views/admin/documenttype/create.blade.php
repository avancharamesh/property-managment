@extends('admin.master')
@section('title', 'Document Add | Admin')
@section('content')
<!-- page content -->

<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2 class="section">Add <small>Document</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <a href="{{url('admin/document-type-list')}}" class="btn btn-sm bg-green">
                               <i class="fa fa-reply" aria-hidden="true"></i>
                           </a>

                       </ul>
                       <div class="clearfix"></div>
                   </div>
                   <!-- @include('flash-message') -->
                    @if ($errors->any())
                      <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>    
                         <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>

                      </div>
                    @endif

                   <div class="x_content">
                    <form class="" action="{{url('admin/document-type-store')}}" method="post" novalidate enctype="multipart/form-data">
                      @csrf
                      <!-- <span class="section">Create Dcoument</span> -->
                      <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">Document Name<span class="required">*</span></label>
                          <div class="col-md-6 col-sm-6">
                            <input  id="name" class="form-control" name="name" value="{{ old('name') }}"  required="required" type="text" class="@error('name') is-invalid @enderror" />
                            @error('name')
                              <span class="alert alert-danger">{{ $message }}</span>
                            @enderror
                          </div>
                      </div>
                        
                        <div class="field item form-group">
                            <label class="col-form-label col-md-3 col-sm-3  label-align">Status<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6">
                             <select class="form-control" name="status" required="required">
                              <option value="">Select Status</option>
                              <option value="1">Active</option>
                              <option value="0">In-Active</option>
                          </select>    
                      </div>
                  </div>


                  <div class="ln_solid">
                    <div class="form-group">
                        <div class="offset-md-3">
                            <button type='submit' class="btn btn-primary">Submit</button>
                            <button type='reset' class="btn btn-success">Reset</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
</div>
</div>
<!-- /page content -->
@endsection

@section('script')

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
 $('.select2').select2();
</script>
<script>


  $(document).ready(function(){
     // initialize a validator instance from the "FormValidator" constructor.
        // A "<form>" element is optionally passed as an argument, but is not a must
        var validator = new FormValidator({
            "events": ['blur', 'input', 'change']
        }, document.forms[0]);
        // on form "submit" event
        document.forms[0].onsubmit = function(e) {
            var submit = true,
            validatorResult = validator.checkAll(this);
            console.log(validatorResult);
            return !!validatorResult.valid;
        };
        // on form "reset" event
        document.forms[0].onreset = function(e) {
            validator.reset();
        };
        // stuff related ONLY for this demo page:
        $('.toggleValidationTooltips').change(function() {
            validator.settings.alerts = !this.checked;
            if (this.checked)
                $('form .alert').remove();
        }).prop('checked', false);

        
    });
</script>
@endsection
@extends('admin.master')
@section('title', 'Village Sector List | Admin')
@section('content')

<div class="right_col" role="main">
   <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>List <small>User Villages</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <a href="{{url('admin/village-sector-create')}}" class="bg-green btn-sm"><i class="fa fa-plus" aria-hidden="true"></i></a>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  @include('flash-message')
                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">
                    <table id="datatable-users" class="table table-striped table-bordered" style="width:100%">
                      <thead>
                        <tr>
                          <th>#NO.</th>
                          <th>Village</th>
                          <th>Sector</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                          <?php $count = 0; ?>
                           @foreach($data as $village)

                           <tr>
                             <td>{{++$count}}</td>
                             <td>{{$village['name']}}</td>
                             <td>
                             @foreach($village['permission'] as $permission)
                             <span class="badge bg-green m-1 p-1">{{$permission->name}}</span>
                             @endforeach
                            </td>
                             <td>
                             <div class="btn-group" role="group">
                               <a href="{{url('admin/village-sector-edit')}}/{{$village['village_id']}}" class='bg-green btn-sm'><i class='fa fa-pencil' aria-hidden='true'></i></a>
                               <a href="{{url('admin/village-sector-show')}}/{{$village['village_id']}}" class='bg-green btn-sm ml-1'><i class='fa fa-eye' aria-hidden='true'></i></a>
                              </div>
                              </td>
                            </tr>
                           @endforeach
                      </tbody>
                      
                      <tfoot>
                        <tr>
                          <th>#NO.</th>
                          <th>Village</th>
                          <th>Sector</th>
                          <th>Action</th>
                        </tr>
                      </tfoot>

                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>
</div>
@endsection

@section('script')

<script>
  $(document).ready(function(){
    $("#datatable-users").DataTable({
      "dom": '<"pull-left"l><"pull-right"f>tip'        
    });
  });
</script>
@endsection
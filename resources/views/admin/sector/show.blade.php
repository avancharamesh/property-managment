@extends('admin.master')
@section('title', 'Sector Add | Admin')
@section('content')
<!-- page content -->

<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Show <small>Sector</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <a href="{{url('admin/sector-list')}}" class="btn btn-sm bg-green">
               <i class="fa fa-reply" aria-hidden="true"></i>
             </a>

           </ul>
           <div class="clearfix"></div>
         </div>
         @include('flash-message')
         <div class="x_content">
          <!-- <form class="" action="{{url('admin/subcategory-update',$sector->id)}}" method="post" novalidate enctype="multipart/form-data"> -->
            @csrf
            <!-- <span class="section">Show Sector</span> -->
            <div class="field item form-group">
              <label class="col-form-label col-md-3 col-sm-3  label-align">Select Village<span class="required">*</span></label>
              <div class="col-md-6 col-sm-6">
               <select class="form-control select2" disabled name="village_id" >
                <option>Select</option>
                @foreach($village as $list)
                dd{{($list)}}
                <option @if($list->id==$sector->village_id) selected @endif value="{{$list->id}}">{{$list->name}}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="field item form-group">
            <label class="col-form-label col-md-3 col-sm-3  label-align">Sector Name<span class="required">*</span></label>
            <div class="col-md-6 col-sm-6">
              <input  id="name" class="form-control" disabled name="name"  required="required" value="{{$sector->name}}" type="text" /></div>
            </div>

            <div class="field item form-group">
             <label class="col-form-label col-md-3 col-sm-3  label-align">SubCategory Image (jpeg,bmp,png,gif)<span class="required">*</span></label>
             <div class="col-md-6 col-sm-6">
              <input class="form-control" name="image" disabled type="file"   onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])" />
            </div>
          </div>

          <div class="field item form-group">
            <label class="col-form-label col-md-3 col-sm-3  label-align">Image Preview<span class="required">*</span></label>
            <div class="col-md-6 col-sm-6">
              @if($sector->image)
              <img src="{{ URL::to('uploads/subcategory',$sector->image) }}" alt="Preview" width="100px" height="150px" id="blah">
              @else
              <img src="{{asset('assets/production/images/avatar-default-icon.png')}}" alt="Preview" width="100px" height="150px" id="blah">
              @endif
            </div>
          </div>

          <div class="field item form-group">
            <label class="col-form-label col-md-3 col-sm-3  label-align">Description<span class="required">*</span></label>
            <div class="col-md-6 col-sm-6">
              <textarea required="required" disabled name='description'>{{$sector->description}}</textarea></div>
            </div>

            <div class="field item form-group">
              <label class="col-form-label col-md-3 col-sm-3  label-align">Status<span class="required">*</span></label>
              <div class="col-md-6 col-sm-6">
               <select class="form-control" disabled name="status" required="required">
                <option value="">Select Status</option>
                <option value="1" @if ($sector->status=='1') selected @endif >Active</option>
                <option value="0" @if($sector->status=='0') selected @endif>In-Active</option>
              </select>    
            </div>
          </div>


          <div class="ln_solid">
            <div class="form-group">
              <div class="offset-md-3">
                <!-- <button type='submit' disabled class="btn btn-primary">Submit</button>
                <button type='reset' disabled class="btn btn-success">Reset</button> -->
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<!-- /page content -->
@endsection

@section('script')

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
 $('.select2').select2();
</script>
<script>


  $(document).ready(function(){
     // initialize a validator instance from the "FormValidator" constructor.
        // A "<form>" element is optionally passed as an argument, but is not a must
        var validator = new FormValidator({
          "events": ['blur', 'input', 'change']
        }, document.forms[0]);
        // on form "submit" event
        document.forms[0].onsubmit = function(e) {
          var submit = true,
          validatorResult = validator.checkAll(this);
          console.log(validatorResult);
          return !!validatorResult.valid;
        };
        // on form "reset" event
        document.forms[0].onreset = function(e) {
          validator.reset();
        };
        // stuff related ONLY for this demo page:
        $('.toggleValidationTooltips').change(function() {
          validator.settings.alerts = !this.checked;
          if (this.checked)
            $('form .alert').remove();
        }).prop('checked', false);

        
      });
    </script>
    @endsection
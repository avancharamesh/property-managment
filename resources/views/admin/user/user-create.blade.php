@extends('admin.master')
@section('title', 'User Add | Admin')
@section('content')
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Add <small>User</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <a href="{{url('admin/user-list')}}" class="btn btn-sm bg-green">
                               <i class="fa fa-reply" aria-hidden="true"></i>
                           </a>

                       </ul>
                       <div class="clearfix"></div>
                   </div>
                   <div class="x_content">
                    <!-- @include('flash-message') -->
                    @if ($errors->any())
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>    
                        <ul>
                          @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                          @endforeach
                      </ul>

                  </div>
                  @endif
                  <form class="" action="{{url('admin/user-create')}}" method="post" novalidate>
                   @csrf
                   <!-- <span class="section">Create User</span> -->

                   <div class="field item form-group">
                    <label class="col-form-label col-md-3 col-sm-3  label-align">UserId<span class="required">*</span></label>
                    <div class="col-md-6 col-sm-6">
                        <input value="{{old('user_number')}}" class="form-control" name="user_number" value="{{ old('user_number') }}" placeholder="Enter User Id"  required="required" />
                         @error('user_number')
                        <span class="alert alert-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="field item form-group">
                    <label class="col-form-label col-md-3 col-sm-3  label-align">Name<span class="required">*</span></label>
                    <div class="col-md-6 col-sm-6">
                        <input value="{{old('name')}}" class="form-control" data-validate-length-range="3"  name="name" placeholder="Enter Your Name" required="required" />
                    </div>
                </div>

                <div class="field item form-group">
                    <label class="col-form-label col-md-3 col-sm-3  label-align">email<span class="required">*</span></label>
                    <div class="col-md-6 col-sm-6">
                        <input value="{{old('email')}}" class="form-control" name="email" placeholder="Enter Your Email" class='email' required="required" type="email" /></div>
                    </div>



                    <div class="field item form-group">
                     <label class="col-form-label col-md-3 col-sm-3  label-align">Password<span class="required">*</span></label>
                     <div class="col-md-6 col-sm-6">
                        <input class="form-control" type="password" id="password1" name="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*]).{8,}" title="Minimum 8 Characters Including An Upper And Lower Case Letter, A Number And A Unique Character" placeholder="Enter Your Password" required />

                        <span style="position: absolute;right:15px;top:7px;" onclick="hideshow()" >
                           <i id="slash" class="fa fa-eye-slash"></i>
                           <i id="eye" class="fa fa-eye"></i>
                       </span>
                   </div>
               </div>


               <div class="field item form-group">
                <label class="col-form-label col-md-3 col-sm-3  label-align">Repeat password<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6">
                    <input class="form-control" type="password" name="password2" data-validate-linked='password' placeholder="Repeat Password" required='required' /></div>
                </div>

                <div class="ln_solid">
                    <div class="form-group">
                        <div class="offset-md-3">
                            <button type='submit' class="btn btn-primary">Submit</button>
                            <button type='reset' class="btn btn-success">Reset</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
</div>
</div>
<!-- /page content -->
@endsection

@section('script')

<script>
    function hideshow(){
     var password = document.getElementById("password1");
     var slash = document.getElementById("slash");
     var eye = document.getElementById("eye");

     if(password.type === 'password'){
        password.type = "text";
        slash.style.display = "block";
        eye.style.display = "none";
    }
    else{
        password.type = "password";
        slash.style.display = "none";
        eye.style.display = "block";
    }

}
$(document).ready(function(){
     // initialize a validator instance from the "FormValidator" constructor.
        // A "<form>" element is optionally passed as an argument, but is not a must
        var validator = new FormValidator({
            "events": ['blur', 'input', 'change']
        }, document.forms[0]);
        // on form "submit" event
        document.forms[0].onsubmit = function(e) {
            var submit = true,
            validatorResult = validator.checkAll(this);
            console.log(validatorResult);
            return !!validatorResult.valid;
        };
        // on form "reset" event
        document.forms[0].onreset = function(e) {
            validator.reset();
        };
        // stuff related ONLY for this demo page:
        $('.toggleValidationTooltips').change(function() {
            validator.settings.alerts = !this.checked;
            if (this.checked)
                $('form .alert').remove();
        }).prop('checked', false);
    });
</script>
@endsection
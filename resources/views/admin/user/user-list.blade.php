@extends('admin.master')
@section('title', 'User List | Admin')
@section('content')

<div class="right_col" role="main">
   <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>List <small>Users</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <a href="{{url('admin/user-create')}}" class="bg-green btn-sm"><i class="fa fa-plus" aria-hidden="true"></i></a>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  @include('flash-message')
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">
                    <table id="datatable-users" class="table table-striped table-bordered" style="width:100%">
                      <thead>
                        <tr>
                          <th>#NO.</th>
                          <th>Name</th>
                          <th>UserId</th>
                          <th>Email</th>
                          <th>Created_at</th>
                          <th>Updated_at</th>
                          <th>Action</th>
                        </tr>
                      </thead>


                      <tbody>
                        <!-- <tr>
                          <td>1</td>
                          <td>admin</td>
                          <td>admin@admin.com</td>
                          <td>12-15-2020</td>
                          <td>12-15-2020</td>
                          <td>
                             <a href="{{url('admin/user-edit')}}" class="bg-green btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                             <a href="{{url('admin/user-show')}}" class="bg-green btn-sm ml-1"><i class="fa fa-eye" aria-hidden="true"></i></a>
                             <a href="#" onclick="return confirm('Are you sure you want to delete this item?');" class="bg-green btn-sm ml-1"><i class="fa fa-trash" aria-hidden="true"></i></a>
                          </td>
                        </tr> -->
                      </tbody>
                      
                      <tfoot>
                        <tr>
                          <th>#NO.</th>
                          <th>Name</th>
                          <th>UserId</th>
                          <th>Email</th>
                          <th>Created_at</th>
                          <th>Updated_at</th>
                          <th>Action</th>
                        </tr>
                      </tfoot>

                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>
</div>
@endsection

@section('script')

<script>
  $(document).ready(function(){
    $("#datatable-users").DataTable({
                "dom": '<"pull-left"l><"pull-right"f>tip',
                processing: true,
                serverSide: true,
                ajax: "{{ url('admin/users-list') }}",
                columns: [
                    {"data": 'DT_RowIndex',orderable: false, searchable: false},
                    {data: 'name', name: 'name'},
                    {data: 'user_number',name:'user_number'},
                    {data: 'email', name: 'email'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'updated_at', name: 'updated_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
  });
</script>
@endsection
@extends('admin.master')
@section('title', 'Role Add | Admin')
@section('content')
<!-- page content -->
<div class="right_col" role="main">
                <div class="">
                    <div class="clearfix"></div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Add <small>Role</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <a href="{{url('admin/role-list')}}" class="btn btn-sm bg-green">
                                             <i class="fa fa-reply" aria-hidden="true"></i>
                                        </a>
                                        
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                @include('flash-message')
                                <div class="x_content">
                                    <form class="" action="{{url('admin/role-create')}}" method="post" novalidate>
                                     @csrf
                                        <!-- <span class="section">Create Role</span> -->
                                        <div class="field item form-group">
                                            <label class="col-form-label col-md-3 col-sm-3  label-align">Name<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6">
                                                <input id="name" class="form-control" data-validate-length-range="2"  name="name" required="required" />
                                            </div>
                                        </div>
                            
                                        <div class="field item form-group">
                                            <label class="col-form-label col-md-3 col-sm-3  label-align">Slug<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6">
                                                <input readonly id="slug" class="form-control" name="slug" class='slug' required="required" type="text" /></div>
                                        </div>
                                        <div class="field item form-group">
                                            <label class="col-form-label col-md-3 col-sm-3  label-align">Description<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6">
                                                <textarea required="required" name='description'></textarea></div>
                                        </div>
                                        
                                        <div class="ln_solid">
                                            <div class="form-group">
                                                <div class="offset-md-3">
                                                    <button type='submit' class="btn btn-primary">Submit</button>
                                                    <button type='reset' class="btn btn-success">Reset</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /page content -->
@endsection

@section('script')

<script>
    function hideshow(){
			var password = document.getElementById("password1");
			var slash = document.getElementById("slash");
			var eye = document.getElementById("eye");
			
			if(password.type === 'password'){
				password.type = "text";
				slash.style.display = "block";
				eye.style.display = "none";
			}
			else{
				password.type = "password";
				slash.style.display = "none";
				eye.style.display = "block";
			}

		}
  $(document).ready(function(){
     // initialize a validator instance from the "FormValidator" constructor.
        // A "<form>" element is optionally passed as an argument, but is not a must
        var validator = new FormValidator({
            "events": ['blur', 'input', 'change']
        }, document.forms[0]);
        // on form "submit" event
        document.forms[0].onsubmit = function(e) {
            var submit = true,
                validatorResult = validator.checkAll(this);
            console.log(validatorResult);
            return !!validatorResult.valid;
        };
        // on form "reset" event
        document.forms[0].onreset = function(e) {
            validator.reset();
        };
        // stuff related ONLY for this demo page:
        $('.toggleValidationTooltips').change(function() {
            validator.settings.alerts = !this.checked;
            if (this.checked)
                $('form .alert').remove();
        }).prop('checked', false);

        // slug call
        $('#name').on('change',function(){
            var name = $('#name').val();
            var res = name.split(" ").join("-").toLowerCase();
             $('#slug').val(res);
        });
  });
</script>
@endsection
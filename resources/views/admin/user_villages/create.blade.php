@extends('admin.master')
@section('title', 'User Village Add | Admin')
@section('content')
<!-- page content -->
<div class="right_col" role="main">
                <div class="">
                    <div class="clearfix"></div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Add <small>User Village</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <a href="{{url('admin/user-village-list')}}" class="btn btn-sm bg-green">
                                             <i class="fa fa-reply" aria-hidden="true"></i>
                                        </a>
                                        
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                @include('flash-message')
                                <div class="x_content">
                                    <form class="" action="{{url('admin/user-village-store')}}" method="post" novalidate>
                                     @csrf
                                        <!-- <span class="section">Create User Village</span> -->
                                        <div class="field item form-group">
                                            <label class="col-form-label col-md-2 col-sm-2">User<span class="required">*</span></label>
                                            <div class="col-md-7 col-sm-7">
                                                <select class="form-control" name="user" required="required" >
                                                        <option value=""> *** SELECT ROLE *** </option>
                                                        @foreach($user as $r)
                                                         <option value="{{$r->id}}"> {{$r->name}} </option>
                                                        @endforeach
                                                </select>
                                            </div>
                                        </div>
                            
                                        <div class="field item form-group">
                                            <label class="col-form-label col-md-2 col-sm-2">Village<span class="required">*</span></label>
                                            <div class="col-md-7 col-sm-7">
                                              @foreach($village as $p)
                                               <div class="col-md-4">
                                                  <div class="m-1"> 
                                                   <input type="checkbox" name="village[]"  value="{{$p['id']}}" class="flat" />  {{$p['name']}}
                                                  </div>
                                                </div>
                                              @endforeach      
                                            </div>
                                        </div>
                                        
                                        
                                        <div class="ln_solid">
                                            <div class="form-group">
                                                <div class="offset-md-3">
                                                    <button type='submit' class="btn btn-primary">Submit</button>
                                                    <button type='reset' class="btn btn-success">Reset</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /page content -->
@endsection

@section('script')

<script>
    function hideshow(){
			var password = document.getElementById("password1");
			var slash = document.getElementById("slash");
			var eye = document.getElementById("eye");
			
			if(password.type === 'password'){
				password.type = "text";
				slash.style.display = "block";
				eye.style.display = "none";
			}
			else{
				password.type = "password";
				slash.style.display = "none";
				eye.style.display = "block";
			}

		}
  $(document).ready(function(){
     // initialize a validator instance from the "FormValidator" constructor.
        // A "<form>" element is optionally passed as an argument, but is not a must
        var validator = new FormValidator({
            "events": ['blur', 'input', 'change']
        }, document.forms[0]);
        // on form "submit" event
        document.forms[0].onsubmit = function(e) {
            var submit = true,
                validatorResult = validator.checkAll(this);
            console.log(validatorResult);
            return !!validatorResult.valid;
        };
        // on form "reset" event
        document.forms[0].onreset = function(e) {
            validator.reset();
        };
        // stuff related ONLY for this demo page:
        $('.toggleValidationTooltips').change(function() {
            validator.settings.alerts = !this.checked;
            if (this.checked)
                $('form .alert').remove();
        }).prop('checked', false);

       
  });
</script>
@endsection
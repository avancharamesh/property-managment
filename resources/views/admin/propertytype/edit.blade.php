@extends('admin.master')
@section('title', 'PropertyType Update | Admin')
@section('content')
<!-- page content -->

<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Update <small>Property Type</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <a href="{{url('admin/property-type-list')}}" class="btn btn-sm bg-green">
               <i class="fa fa-reply" aria-hidden="true"></i>
             </a>

           </ul>
           <div class="clearfix"></div>
         </div>
         @include('flash-message')
         <div class="x_content">
          <form class="" action="{{url('admin/property-type-update',$property_type->id)}}" method="post" novalidate enctype="multipart/form-data">
            @csrf
            <!-- <span class="section">Update Property Type</span> -->

            <div class="field item form-group">
              <label class="col-form-label col-md-3 col-sm-3  label-align">Select Village<span class="required">*</span></label>
              <div class="col-md-6 col-sm-6">
               <select class="form-control select2" name="village_id" required="required">
                <option value="">Select</option>
                @foreach($village as $cat_list)
                <option @if($cat_list->id==$property_type->village_id) selected @endif value="{{$cat_list->id}}">{{$cat_list->name}}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="field item form-group">
            <label class="col-form-label col-md-3 col-sm-3  label-align">Select Sector<span class="required">*</span></label>
            <div class="col-md-6 col-sm-6">
              <select class="form-control" name="sector_id" required="required" id="sector_id">
                <option selected value="">Select</option> 
                  @foreach($sector as $sec_list)
                   <option @if($sec_list->id==$property_type->sector_id) selected @endif  value="{{$sec_list->id}}">{{$sec_list->name}}</option> 
                @endforeach

              </select>
            </div>
          </div>

          <div class="field item form-group">
            <label class="col-form-label col-md-3 col-sm-3  label-align">Select SubSector<span class="required">*</span></label>
            <div class="col-md-6 col-sm-6">
              <select class="form-control select2" name="subsector_id" required="required" id="subsector_id">
                <option selected value="">Select</option> 
                 @foreach($subsector as $sub_list)
                   <option @if($sub_list->id==$property_type->subsector_id) selected @endif  value="{{$sub_list->id}}">{{$sub_list->name}}</option> 
                @endforeach

              </select>
            </div>
          </div>
          <div class="field item form-group">
            <label class="col-form-label col-md-3 col-sm-3  label-align">Property Name<span class="required">*</span></label>
            <div class="col-md-6 col-sm-6">
              <input  id="name" class="form-control" name="name" value="{{$property_type->name}}"  required="required" type="text" /></div>
            </div>
              
              <div class="field item form-group">
              <label class="col-form-label col-md-3 col-sm-3  label-align">Profile Image (jpeg,bmp,png,gif)<span class="required">*</span></label>
              <div class="col-md-6 col-sm-6">
                <input class="form-control" name="image"  type="file"  onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])" />
              </div>
            </div>
             <div class="field item form-group">
              <label class="col-form-label col-md-3 col-sm-3  label-align">Image Preview<span class="required">*</span></label>
              <div class="field item form-group">
                <label class="col-form-label col-md-3 col-sm-3  label-align">Image Preview<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6">
                  @if($property_type->image)
                  <img src="{{ URL::to('uploads/properties',$property_type->image) }}" alt="Preview" width="100px" height="150px" id="blah">
                  @else
                  <img src="{{asset('assets/production/images/avatar-default-icon.png')}}" alt="Preview" width="100px" height="150px" id="blah">
                  @endif
                </div>
              </div>
            </div>
            <div class="field item form-group">
              <label class="col-form-label col-md-3 col-sm-3  label-align">Description<span class="required">*</span></label>
              <div class="col-md-6 col-sm-6">
                <textarea required="required" value="{{$property_type->description}}" name='description'>{{$property_type->description}}</textarea></div>
              </div>
              <div class="field item form-group">
                <label class="col-form-label col-md-3 col-sm-3  label-align">Status<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6">
                 <select class="form-control" name="status" required="required">
                  <option value="">Select Status</option>
                  <option value="1" @if ($property_type->status=='1') selected @endif>Active</option>
                  <option value="0"@if ($property_type->status=='0') selected @endif>In-Active</option>
                </select>    
              </div>
            </div>
            <div id="documentDetails">
            </div>

          </div>


          <div class="ln_solid">
            <div class="form-group">
              <div class="offset-md-3">
                <button type='submit' class="btn btn-primary">Submit</button>
                <button type='reset' class="btn btn-success">Reset</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<!-- /page content -->
@endsection

@section('script')

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
 $('.select2').select2();
</script>
<script>


  $(document).ready(function(){
     // initialize a validator instance from the "FormValidator" constructor.
        // A "<form>" element is optionally passed as an argument, but is not a must
        var validator = new FormValidator({
          "events": ['blur', 'input', 'change']
        }, document.forms[0]);
        // on form "submit" event
        document.forms[0].onsubmit = function(e) {
          var submit = true,
          validatorResult = validator.checkAll(this);
          console.log(validatorResult);
          return !!validatorResult.valid;
        };
        // on form "reset" event
        document.forms[0].onreset = function(e) {
          validator.reset();
        };
        // stuff related ONLY for this demo page:
        $('.toggleValidationTooltips').change(function() {
          validator.settings.alerts = !this.checked;
          if (this.checked)
            $('form .alert').remove();
        }).prop('checked', false);

        
      });

  $(document).ready(function() {
    $('select[name="village_id"]').on('change', function() {
      var village_id = $(this).val();
      if(village_id) {
        $.ajax({
          url: '{{url("admin/property-type-sector")}}/'+village_id,
          type: "GET",
          dataType: "json",
          success:function(data) {
            $('select[name="sector_id"]').empty();
            $('select[name="sector_id"]').append('<option value=""> Select </option>');
            for (var i = 0; i < data.length; ++i) {
              $('select[name="sector_id"]').append('<option value="'+ data[i].id +'">'+ data[i].name +'</option>');

            }
                        // $('select[name="subcategory_id"]').empty();
                        // $.each(data, function(key, value) {
                        //     $('select[name="subcategory_id"]').append('<option value="'+ data[key].id +'">'+ value +'</option>');
                        // });


                      }
                    });
      }else{
        $('select[name="sector_id"]').empty();
      }
    });
  });

  $(document).ready(function() {
    $('select[name="sector_id"]').on('click', function() {
      var sector_id = $(this).val();
      if(sector_id) {
        $.ajax({
          url: '{{URL::to("admin/property-type-subsector")}}/'+sector_id,
          type: "GET",
          dataType: "json",
          success:function(data) {
            $('select[name="subsector_id"]').empty();
            $('select[name="subsector_id"]').append('<option value=""> Select </option>');
            for (var i = 0; i < data.length; ++i) {
              $('select[name="subsector_id"]').append('<option value="'+ data[i].id +'">'+ data[i].name +'</option>');

            }
                        // $('select[name="subcategory_id"]').empty();
                        // $.each(data, function(key, value) {
                        //     $('select[name="subcategory_id"]').append('<option value="'+ data[key].id +'">'+ value +'</option>');
                        // });


                      }
                    });
      }else{
        $('select[name="subsector_id"]').empty();
      }
    });
  });

</script>
@endsection
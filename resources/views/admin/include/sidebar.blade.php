<div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="{{url('admin/dashboard')}}" class="site_title"><i class="fa fa-building"></i> <span>Property Management</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
              @if(Auth::user()->profile_image)
                <img src="{{asset('uploads/user-profile')}}/{{Auth::user()->profile_image}}" alt="..." class="img-circle profile_img">
              @else
                <img src="{{asset('assets/production/images/img.jpg')}}" alt="..." class="img-circle profile_img">
              @endif
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>{{ ucfirst(Auth::user()->name) }}</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />
<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                {{-- <h3>General</h3> --}}
                <ul class="nav side-menu">
                  <li><a href="{{url('admin/dashboard')}}">
                       <i class="fa fa-home"></i> Dashboard <span class="fa fa-chevron-down"></span>
                       </a>
                  </li>
                  @if (Auth::user()->hasRole('super-admin'))
                  <li><a><i class="fa fa-users"></i> User RP Management <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{url('admin/permission-list')}}">Permission</a></li>
                      <li><a href="{{url('admin/role-list')}}">Role</a></li>
                      <li><a href="{{url('admin/role-permission-list')}}">Role Permission</a></li>
                      <li><a href="{{url('admin/user-village-list')}}">User Village</a></li>
                      <li><a href="{{url('admin/user-sector-list')}}">User Sector</a></li>
                      <li><a href="{{url('admin/user-subsector-list')}}">User SubSector</a></li>
                      <li><a href="{{ url('admin/user-properties-list')}}">User Properties</a></li>
                      <li><a href="{{url('admin/user-role-list')}}"> User  Role</a></li>
                      <li><a href="{{url('admin/label-list')}}"> Label Name</a></li>
                    </ul>
                  </li>
                  @endif
                  @if (Auth::user()->hasRole('super-admin'))
                  <li><a><i class="fa fa-user"></i> User Management<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                       <li><a href="{{url('admin/user-list')}}"> User List</a></li>
                    </ul>
                  </li>
                  @endif
                  @if (Auth::user()->hasRole('super-admin'))
                  <li><a><i class="fa fa-list-alt"></i> Village<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                       <li><a href="{{url('admin/village-list')}}">Village List</a></li>
                    </ul>
                  </li>

                   <li><a><i class="fa fa-list-alt"></i>Sectors<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                       <li><a href="{{url('admin/sector-list')}}">Sector List</a></li>
                    </ul>
                  </li>
                  
                   <li><a><i class="fa fa-list-alt"></i> SubSector<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                       <li><a href="{{url('admin/subsector-list')}}">SubSector List</a></li>
                    </ul>
                  </li>

                   <li><a><i class="fa fa-list-alt"></i> PropertyType<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                       <li><a href="{{url('admin/property-type-list')}}">PropertyType List</a></li>
                    </ul>
                  </li>

                  <li><a><i class="fa fa-list-alt"></i> DocumentType<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                       <li><a href="{{url('admin/document-type-list')}}">DocumentType List</a></li>
                    </ul>
                  </li>
                  @endif
                  <li><a><i class="fa fa-list-alt"></i> Property Details<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                       <li><a href="{{url('admin/properties-details-create')}}">Property Details Create</a></li>
                    </ul>
                  </li>
                   

                   <li><a><i class="fa fa-list-alt"></i>Reports<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                       <li><a href="{{url('admin/properties-details-list')}}">Reports List</a></li>
                    </ul>
                  </li>


                </ul>
              </div>


            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
           <!--  <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div> -->
            <!-- /menu footer buttons -->

            </div>
        </div>

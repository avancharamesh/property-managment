<!-- top navigation -->
<div class="top_nav">
          <div class="nav_menu">
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
              <nav class="nav navbar-nav">
              <ul class=" navbar-right">
                <li class="nav-item dropdown open" style="padding-left: 15px;">
                  <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                  @if(Auth::user()->profile_image)
                    <img src="{{asset('uploads/user-profile')}}/{{Auth::user()->profile_image}}" alt="...">
                  @else 
                    <img src="{{asset('assets/production/images/img.jpg')}}" alt="..." >
                  @endif{{ ucfirst(Auth::user()->name) }}
                  </a>
                  <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item"  href="{{url('admin/change-profile')}}"> Profile</a>
              
                     <a class="dropdown-item"  href="{{url('admin/change-password')}}">Change Password</a>
                    <a class="dropdown-item"  href="{{url('admin/logout')}}"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                  </div>
                </li>

                
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
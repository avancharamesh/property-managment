@extends('admin.master')
@section('title', 'PropertyDetails List | Admin')
@section('content')


<style type="text/css">
       thead tr {
            background-color:#ff9999;/* #2A3F54; */
            color:#FFFFFF;
            height: 50px;
            vertical-align:middle;
            text-align: center;
        }
          tfoot tr {
            background-color:#ff9999; /*#2A3F54;*/
            color:#FFFFFF;
            height: 50px;
            vertical-align:middle;
            text-align: center;
        }
  </style>

<meta name="csrf-token" content="{{ csrf_token() }}">
<script  src="../../Jquery/prettify.js"></script>
<link href="/RetailSmart/Jquery/jquery.multiselect.css" rel="stylesheet"/>

 
<div class="right_col" role="main">
   <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>List <small>PropertyDetails</small></h2>

                    <ul class="nav navbar-right panel_toolbox">
                    {{-- @if (Auth::user()->can('permission-create')) --}}
                      <a href="{{url('admin/label-create')}}" class="bg-green btn-sm"><i class="fa fa-plus" aria-hidden="true"></i></a>
                    {{-- @endif --}}
                   </ul>
                                      
                    <div class="clearfix"></div>
                  </div>
                  @include('flash-message')
                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">
                    <table id="datatable-category" class="table table-striped table-bordered" style="width:100%">
                      <thead>
                        <tr>
                          <th>#SNO.</th>
                          <th>Village</th>
                          <th>Sector</th>
                          <th>SubSector</th>
                          <th>Property Type</th>
                          <th>name</th>
                          <th>Action</th>
                        </tr>
                      </thead>


                      <tbody>
                                @php $i = 1; @endphp
                                    @foreach ($label_list as $lists)
                                    <tr>
                                      <td>{{ $i++ }}</td>
                                      <td>{{$lists->village}}</td>
                                      <td>{{$lists->sector}}</td>
                                      <td>{{ $lists->sub_sector }}</td>
                                      <td>{{$lists->property_type}}</td>
                                      <td>{{$lists->property_name}}</td>
                                      <td><a href="{{ route('label.edit',$lists->id ) }}" class="bg-green btn-sm"><i class="fa fa-pencil"></i></td>

                                     
                      </tbody>

                      <tfoot>
                        <tr>
                         <th>#SNO.</th>
                          <th>Village</th>
                          <th>Sector</th>
                          <th>SubSector</th>
                          <th>Property Type</th>
                          <th>name</th>
                          <th>Action</th>
                        </tr>
                        @endforeach
                      </tfoot>

                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>
</div>
@endsection


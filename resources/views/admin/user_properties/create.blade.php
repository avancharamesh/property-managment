@extends('admin.master')
@section('title', 'User Properties Add | Admin')
@section('content')
<!-- page content -->
<div class="right_col" role="main">
                <div class="">
                    <div class="clearfix"></div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Add <small>User Properties</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <a href="{{url('admin/user-properties-list')}}" class="btn btn-sm bg-green">
                                             <i class="fa fa-reply" aria-hidden="true"></i>
                                        </a>
                                        
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                @include('flash-message')
                                <div class="x_content">
                                    <form class="" action="{{url('admin/user-properties-store')}}" method="post" novalidate>
                                     @csrf
                                        <!-- <span class="section">Create User Property</span> -->
                                        <div class="field item form-group">
                                            <label class="col-form-label col-md-2 col-sm-2">User<span class="required">*</span></label>
                                            <div class="col-md-7 col-sm-7">
                                                <select class="form-control" name="user" required="required" >
                                                        <option value=""> *** SELECT ROLE *** </option>
                                                        @foreach($user as $r)
                                                         <option value="{{$r->id}}"> {{$r->name}} </option>
                                                        @endforeach
                                                </select>
                                            </div>
                                        </div>
                            
                                       <div class="field item form-group">
                                            <label class="col-form-label col-md-2 col-sm-2">SubSector<span class="required">*</span></label>
                                            <div class="col-md-7 col-sm-7">
                                              
                                               <!-- <div class="col-md-4">
                                                  <div class="m-1"> 
 -->                                                   <span id="myfiles"></span>
                                                  <!-- </div>
                                                </div>
 -->                                                    
                                            </div>
                                        </div>
                                        
                                        <div class="ln_solid">
                                            <div class="form-group">
                                                <div class="offset-md-3">
                                                    <button type='submit' class="btn btn-primary">Submit</button>
                                                    <button type='reset' class="btn btn-success">Reset</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /page content -->
@endsection

@section('script')

<script>
    function hideshow(){
			var password = document.getElementById("password1");
			var slash = document.getElementById("slash");
			var eye = document.getElementById("eye");
			
			if(password.type === 'password'){
				password.type = "text";
				slash.style.display = "block";
				eye.style.display = "none";
			}
			else{
				password.type = "password";
				slash.style.display = "none";
				eye.style.display = "block";
			}

		}
  $(document).ready(function(){
     // initialize a validator instance from the "FormValidator" constructor.
        // A "<form>" element is optionally passed as an argument, but is not a must
        var validator = new FormValidator({
            "events": ['blur', 'input', 'change']
        }, document.forms[0]);
        // on form "submit" event
        document.forms[0].onsubmit = function(e) {
            var submit = true,
                validatorResult = validator.checkAll(this);
            console.log(validatorResult);
            return !!validatorResult.valid;
        };
        // on form "reset" event
        document.forms[0].onreset = function(e) {
            validator.reset();
        };
        // stuff related ONLY for this demo page:
        $('.toggleValidationTooltips').change(function() {
            validator.settings.alerts = !this.checked;
            if (this.checked)
                $('form .alert').remove();
        }).prop('checked', false);

       
  });

   $(document).ready(function(){

    $('select[name="user"]').on('change',function(){

         var user_id = $(this).val();
                    if(user_id) {
                      $.ajax({
                        url: '{{url("admin/user/create-property/list")}}/'+user_id,
                        type: "GET",
                        dataType: "json",
                        success:function(data) {
                          $('select[name="user_id"]').empty();
                          $('#myfiles').empty();
                            for (var i = 0; i < data.length; ++i) {
                       
                                $('#myfiles').append('<div class="col-md-4"><div class="m-1"> <input type="checkbox" name="property[]"  value="'+data[i].id+'" class="flat" />'+ data[i].village_name +' - '+ data[i].sector_name +' - '+ data[i].subsector_name +' - '+data[i].name +'</div></div>');
                            }

                      }
                    });
                    }else{
                      $('#myfiles').empty();
                    }
       
    });

  });

</script>
@endsection
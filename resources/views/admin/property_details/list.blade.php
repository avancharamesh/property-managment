@extends('admin.master')
@section('title', 'PropertyDetails List | Admin')
@section('content')

<meta name="csrf-token" content="{{ csrf_token() }}">
<script  src="../../Jquery/prettify.js"></script>
<link href="/RetailSmart/Jquery/jquery.multiselect.css" rel="stylesheet"/>
<style>
  .buttons-csv{
    background-color:#1ABB9C;
    color:white;
    padding: 10px 22px;
     margin: 4px 2px;
    border:1px solid  !important; 
  }
  .buttons-pdf{
    background-color:#1ABB9C;
    color:white;
    padding: 10px 22px;
    margin: 4px 2px;
    border:1px solid  !important; 
  }
  .buttons-excel{
    background-color:#1ABB9C;
    color:white;
    padding: 10px 22px;
    margin: 4px 2px;
    border:1px solid  !important;
  }
  
  #datatable-category_filter{
    float: right;
  }
  .dataTables_length,.dataTables_filter {
    margin-left: 10px;
    float: right;
}

</style>


<div class="right_col" role="main">
   <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>List <small>PropertyDetails</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                    {{-- @if (Auth::user()->can('permission-create')) --}}
                      <!-- <a href="{{url('admin/properties-details-create')}}" class="bg-green btn-sm"><i class="fa fa-plus" aria-hidden="true"></i></a> -->
                    {{-- @endif --}}
                   </ul>
                    <div class="clearfix"></div>
                  </div>
                  @include('flash-message')
                     <div class="card">
                          <div class="card-body">
                              <div class="form-group">
                                  <label><strong>Filter By Document :</strong></label>
                                  <select id='status' class="form-control" style="width: 200px">
                                      <option value="" <?php if($status=="") echo "selected" ?>>--Select Type--</option>
                                      <option value="0" <?php if($status=="0") echo "selected" ?>>Non Documented</option>
                                      <option value="1" <?php if($status=="1") echo "selected" ?>>Partially Documented</option>
                                      <option value="2" <?php if($status=="2") echo "selected" ?>>Documented</option>
                                      <option value="3" <?php if($status=="3") echo "selected" ?>>Litigated</option>
                                      <option value="4" <?php if($status=="4") echo "selected" ?>>Non Litigated</option>
                                      
                                    
                                  </select>
                              </div>
                          </div>
                      </div>
                    <table id="datatable-category" class="table table-striped table-bordered" style="width:100%">
                      <thead>
                        <tr>
                          <th>#SNO.</th>
                          <th>Village</th>
                          <th>Sector</th>
                          <th>SubSector</th>
                          <th>Property Type</th>
                          <th>name</th>
                          <th>address</th>
                          <th>Mobile</th>
                           <th>Survey Number</th>
                           <th>Link1</th>
                           <th>Link2</th>
                           <th>Link3</th>
                           <th>Link4</th>
                          <th>Document Number</th>
                          <th>Custodain Property</th>
                          <th>Sold Year</th>
                          <th>Purchased Year</th>
                          <th>Is_document</th>
                          <th>Is_litigated</th>
                          <th>Uploaded Documents</th>
                          <th>DocumentType</th>
                          <th>Documents</th>
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                      </thead>


                      <tbody>
                        
                       
                      </tbody>

                      <tfoot>
                        <tr>
                         <th>#SNO.</th>
                          <th>Village</th>
                          <th>Sector</th>
                          <th>SubSector</th>
                          <th>Property Type</th>
                          <th>name</th>
                          <th>address</th>
                          <th>Mobile</th>
                          <th>Survey Number</th>
                          <th>Link1</th>
                           <th>Link2</th>
                           <th>Link3</th>
                           <th>Link4</th>
                          <th>Document Number</th>
                          <th>Custodain Property</th>
                          <th>Sold Year</th>
                          <th>Purchased Year</th>
                          <th>Is_document</th>
                          <th>Is_litigated</th>
                          <th>Uploaded Documents</th>
                          <th>DocumentType</th>
                          <th>Documents</th>
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                      </tfoot>

                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>
</div>
@endsection

@section('script')
<!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
 -->
 <!-- <script type="text/javascript" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script> -->
<script>
  
  $(document).ready(function(){
    var table = $("#datatable-category").DataTable({
                "dom": '<"pull-left"f><"pull-right"l>tip',
                "lengthMenu":[10, 25, 50, 100,200,500,1000],
                dom: 'Blfrtip',
                buttons: [
                     'csv','excel'
                     ], 

                responsive: true,
                processing: true,
                serverSide: true,
                "scrollX": true,
                // ajax: "{{ URL::to('admin/properties-details-lists') }}",
                ajax: {
                        url: "{{ URL::to('admin/properties-details-lists') }}",
                        data: function (d) {
                              d.status = $('#status').val();
                          }
                      },


                type: "GET",
                columns: [
                    {"data": 'DT_RowIndex',orderable: false, searchable: false},

                    {data: 'village',name: 'village'},
                    {data: 'sector', name: 'sector'},
                    {data: 'subsector',name: 'subsector'},
                    {data: 'property_type', name: 'property_type'},
                    {data: 'name', name: 'name'},
                    {data: 'address',name: 'address'},
                    {data: 'contact',name: 'contact'},
                    {data: 'survey_number',name: 'survey_number'},
                    {data: 'link1',name:'link1'},
                    {data: 'link2',name:'link2'},
                    {data: 'link3',name:'link3'},
                    {data: 'link4',name:'link4'},
                    {data: 'document_number',name: 'document_number'},
                    {data: 'custodian_property',name: 'custodian_property'},
                    {data: 'sold_year',name: 'sold_year'},
                    {data: 'year_of_purchased',name:'year_of_purchased'},
                    {data: 'is_document',name: 'is_document'},
                    {data: 'is_litigated',name:'is_litigated'},
                    {data: 'uploaded_documents',name: 'uploaded_documents'},
                    {data: 'document_type',name:'document_type'},
                    {data: 'document',name:'document'},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

          $('#status').change(function(){
            table.draw();
          });
  });
</script>
@endsection

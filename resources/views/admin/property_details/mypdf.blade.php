<!DOCTYPE html>
<html>

<head>
    <style>
        body {
            margin: 0;
            padding: 0;
            background-color: #FAFAFA;
            font: 12pt "Tahoma";
        }
        
        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }
        
        /*.page {
            width: 21cm;
            min-height: 29.7cm;
            padding: 2cm;
            margin: 1cm auto;
            border: 1px #D3D3D3 solid;
            border-radius: 5px;
            background: white;
            box-shadow: 0 0 2spx rgba(0, 0, 0, 0.1);
        }*/
        
        .subpage {
            padding: 0cm;
            border: 10px orangered solid;
            height: 286mm;
            outline: 1cm #FFEAEA solid;
        }
        
        .head {
            color: seagreen;
        }
        
        @page {
            size: A4;
            margin: 0;
        }
        
        @media print {
            .page {
                margin: 0;
                border: initial;
                border-radius: initial;
                width: initial;
                min-height: initial;
                box-shadow: initial;
                background: initial;
                page-break-after: always;
            }
        }

        table {
  border-collapse: collapse;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 16px;
}
border-right: : 1px solid black;
}
.td-head{
	width: 30%;
}
.td-head1{
	width: 2%;
}
</style>
</head>

<body>
    <div class="book">
        <div class="page">
            <div class="subpage">
                <center>
                    <h1 class="head">Property Management</h1>

                </center>
                <hr>
                <center>
                	<h3 class="head"><u>Property Details</u></h3>
                </center>

                <table>
                	<tr>
                		<td class="td-head">Village Name</td>
                		<td class="td-head1">|</td>
                		<td>{{$village}}</td>
                	</tr>
                </table>
                <table>
                	<tr>
                		<td class="td-head">Sector Name</td>
                		<td class="td-head1">|</td>
                		<td>{{$sector}}</td>
                	</tr>
                </table>
                <table>
                	<tr>
                		<td class="td-head">Subsector Name</td>
                		<td class="td-head1">|</td>
                		<td>{{$subsector}}</td>
                	</tr>
                </table>
                <table>
                	<tr>
                		<td class="td-head">PropertyType Name</td>
                		<td class="td-head1">|</td>
                		<td>{{$property_type}}</td>
                	</tr>
                </table>
                <table>
                	<tr>
                		<td class="td-head">Property Name</td>
                		<td class="td-head1">|</td>
                		<td>{{$property_details->name}}</td>
                	</tr>
                </table>
                <table>
                	<tr>
                		<td class="td-head">Address</td>
                		<td class="td-head1">|</td>
                		<td>{{$property_details->address}}</td>
                	</tr>
                </table>
                <table>
                	<tr>
                		<td class="td-head">Mobile</td>
                		<td class="td-head1">|</td>
                		<td>{{$property_details->contact}}</td>
                	</tr>
                </table>
                <table>
                	<tr>
                		<td class="td-head">Survey Number</td>
                		<td class="td-head1">|</td>
                		<td>{{$property_details->survey_number}}</td>
                	</tr>
                	</table>
                	<table>
                	<tr>
                		<td class="td-head">Document Number</td>
                		<td class="td-head1">|</td>
                		<td>{{$property_details->document_number}}</td>
                	</tr>
                </table>
                <table>
                	<tr>
                		<td class="td-head">Custodain Property</td>
                		<td class="td-head1">|</td>
                		<td>{{$property_details->custodian_property}}</td>
                	</tr>
                </table>
                <table>
                	<tr>
                	<td class="td-head">Sold Year</td>
                		<td class="td-head1">|</td>
                		<td>{{$property_details->sold_year}}</td>
                	</tr>
                </table>
                <table>
                	<tr>
                	<td class="td-head">Purchased Year</td>
                		<td class="td-head1">|</td>
                		<td>{{$property_details->year_of_purchased}}</td>
                	</tr>
                </table>
                <table>
                	<tr>
                	<td class="td-head">Documents name</td>
                		<td class="td-head1">|</td>
                		<td>{{$document->document_type}}</td>
                	</tr>
                </table>
                @if($property_details->is_litigated==1)
                <table>
                    <tr>
                    <td class="td-head">Litigated</td>
                        <td class="td-head1">|</td>
                        <td>Yes</td>
                    </tr>
                </table>
                @endif
                 @if($property_details->is_litigated==0)
                <table>
                    <tr>
                    <td class="td-head">Litigated</td>
                        <td class="td-head1">|</td>
                        <td>No</td>
                    </tr>
                </table>
                @endif
                @if($property_details->is_document==1)
                <table>
                    <tr>
                    <td class="td-head">Documented</td>
                        <td class="td-head1">|</td>
                        <td>Yes</td>
                    </tr>
                </table>
                @endif
                  @if($property_details->is_document==0)
                <table>
                    <tr>
                    <td class="td-head">Documented</td>
                        <td class="td-head1">|</td>
                        <td>No</td>
                    </tr>
                </table>
                @endif
                 <!-- <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12">
			         <h1>Village - <span class="text-success">{{$village}} </span> </h1>
				</div>
				<div class="col-md-12 col-lg-12 col-sm-12">
					<h1>Sector - <span class="text-success">{{$sector}} </span> </h1>
				</div>
				<div class="col-md-12 col-lg-12 col-sm-12">
					<h1>Subsector - <span class="text-success">{{$subsector}} </span> </h1>
				</div>
				<div class="col-md-12 col-lg-12 col-sm-12">
				   <h1>PropertyType - <span class="text-success"> {{$property_type}} </span> </h1>
				</div> -->
			</div>
            </div>
        </div>
    </div>
</body>

</html>







<!-- <center>

		<div class="col-md-12 col-lg-12 col-sm-12">
			<h1>Village - <span class="text-success">{{$village}} </span> </h1>
		</div>
		<div class="col-md-12 col-lg-12 col-sm-12">
			<h1>Sector - <span class="text-success">{{$sector}} </span> </h1>
		</div>
		<div class="col-md-12 col-lg-12 col-sm-12">
			<h1>Subsector - <span class="text-success">{{$subsector}} </span> </h1>
		</div>
		<div class="col-md-12 col-lg-12 col-sm-12">
		   <h1>PropertyType - <span class="text-success"> {{$property_type}} </span> </h1>
		</div>
	</center> -->
@extends('admin.master')
@section('title', 'PropertyDetails Add | Admin')
@section('content')
<!-- page content -->



<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Add <small>PropertyDetails</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <a href="{{url('admin/properties-details-list')}}" class="btn btn-sm bg-green">
               <i class="fa fa-reply" aria-hidden="true"></i>
             </a>

           </ul>
           <div class="clearfix"></div>
         </div>
         <!-- @include('flash-message') -->
         @if ($errors->any())
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>    
            <strong>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
              
            </strong>
        </div>
        @endif
         <div class="x_content">
          <form class="" action="{{url('admin/properties-details-store')}}" method="post"  enctype="multipart/form-data">
            @csrf
            <!-- <span class="section">Create PropertyDetails</span> -->

            <div class="field item form-group">
              <label class="col-form-label col-md-3 col-sm-3  label-align">{{$label[0]  ->village}}<span class="required">*</span></label>
              <div class="col-md-6 col-sm-6">
               <select class="form-control select2" name="village_id" required="required">
                <option value="">Select</option>
                @foreach($village as $cat_list)
                <option value="{{$cat_list->id}}">{{$cat_list->name}}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="field item form-group">
            <label class="col-form-label col-md-3 col-sm-3  label-align">{{$label[0] ->sector}}<span class="required">*</span></label>
            <div class="col-md-6 col-sm-6">
              <select class="form-control " name="sector_id" required="required" id="sector_id">
                <option value="">Select</option> 

              </select>
            </div>
          </div>

          <div class="field item form-group">
            <label class="col-form-label col-md-3 col-sm-3  label-align">{{$label[0] ->sub_sector}}<span class="required" style="color: red">*</span></label>
            <div class="col-md-6 col-sm-6">
              <select class="form-control " name="subsector_id"  id="sector_id">
                <option value="">Select</option> 

              </select>
            </div>
          </div>

          <div class="field item form-group">
            <label class="col-form-label col-md-3 col-sm-3  label-align">{{$label[0] ->property_type}}<span class="required">*</span></label>
            <div class="col-md-6 col-sm-6">
              <select class="form-control select2" name="property_type_id" required="required" id="property_type_id">
                <option value="">Select</option> 

              </select>
            </div>
          </div>
          <div class="field item form-group">
            <label class="col-form-label col-md-3 col-sm-3  label-align" id="change1">{{$label[0] ->property_name}}<span class="required">*</span></label>
            <div class="col-md-6 col-sm-6">
              <input  id="name" class="form-control" name="name"  required="required" type="text" /></div>
            </div>
            <div class="field item form-group">
              <label class="col-form-label col-md-3 col-sm-3  label-align">Mobile Number<span class="required">*</span></label>
              <div class="col-md-6 col-sm-6">
                <input  id="mobile" class="form-control" name="contact"  required="required" type="tel" /></div>
              </div>

              <div class="field item form-group">
                <label class="col-form-label col-md-3 col-sm-3  label-align">Address<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6">
                  <textarea required="required" name='address'></textarea></div>
                </div>
                <div class="field item form-group">
                  <label class="col-form-label col-md-3 col-sm-3  label-align">Status<span class="required">*</span></label>
                  <div class="col-md-6 col-sm-6">
                   <select class="form-control" name="status" required="required">
                    <option value="">Select Status</option>
                    <option value="1">Active</option>
                    <option value="0">In-Active</option>
                  </select>    
                </div>
              </div>
              
              <div class="row add-field">
                <label class="col-form-label col-md-3 col-sm-3  label-align ">Select Documented/ Non-Documented<span class="required">*</span></label>
                <div class="col-md-3 col-sm-3">
                  <input type="radio" id="radioPrimary3" value="1" name="is_document" required="required">
                  <label for="radioPrimary3">
                    Document
                  </label>
                </div>
                <div class="col-md-3 col-sm-3">
                  <input type="radio" id="radioPrimary3" value="0" name="is_document" required="required">
                  <label for="radioPrimary3">
                    Non Document
                  </label>
                </div>
              </div>
              <div class="row">
                <label class="col-form-label col-md-3 col-sm-3  label-align">Select Litigated/ Non-Litigated<span class="required">*</span></label>
                <div class="col-md-3 col-sm-3">
                  <input type="radio" id="radioPrimary31" value="1" name="is_litigated" class="required" required="required">
                  <label for="radioPrimary3">
                    Litigated
                  </label>
                </div>
                <div class="col-md-3 col-sm-3">
                  <input type="radio" id="radioPrimary4" value="0" name="is_litigated" >
                  <label for="radioPrimary4">
                    Non Litigated
                  </label>
                </div>
              </div>
              <!-- <div class="row">
                <label class="col-form-label col-md-3 col-sm-3  label-align">Upload Documnet<span class="required">*</span></label>
                <div class="col-md-3 col-sm-3">
                  <label >
                    Document Type
                  </label>
                </div>
                <div class="col-md-3 col-sm-3">
                  <label>
                    Documnet
                  </label>
                </div>
              </div>
              <div class="row">
                <label class="col-form-label col-md-3 col-sm-3  label-align">
                  <p id="addRow" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i></p>
                </label>
                <div class="col-md-3 col-sm-3">
                  <select class="form-control" name="document_type[]"  required="required">
                  <option value="">Select Document</option>
                    <?php foreach($document as $doc){?>
                      <option value="<?php echo $doc->name;?>"><?php echo $doc->name;?></option>
                    <?php }?>
                  </select> 
                </div> 
                <div class="col-md-3 col-sm-3">
                  <input type="file" class="form-control" name="document[]"  id="myid" required>       
                </div>
              </div> -->
             <!--  <div class="row">
                <label class="col-form-label col-md-3 col-sm-3  label-align">
                  <p id="removeRow" type="button" class="btn btn-danger"><i class="fa fa-trash"></i></p>
                </label>
                <div class="col-md-3 col-sm-3">
                  <select class="form-control" name="document_type[]"  required="required">
                  <option value="">Select Document</option>
                    <?php foreach($document as $doc){?>
                      <option value="<?php echo $doc->name;?>"><?php echo $doc->name;?></option>
                    <?php }?>
                  </select> 
                </div>
                <div class="col-md-3 col-sm-3">
                  <input type="file" class="form-control" name="document[]"  id="myid" required>       
                </div>
              </div> -->

              <div id="documentDetails">
              </div>

              <div class="field item form-group">
                <label class="col-form-label col-md-3 col-sm-3  label-align">Survey Number<span class="required"></span></label>
                <div class="col-md-6 col-sm-6">
                  <input  id="name" class="form-control" name="survey_number" type="text" /></div>
                </div>
                <div class="field item form-group">
                  <label class="col-form-label col-md-3 col-sm-3  label-align">Document Number<span class="required"></span></label>
                  <div class="col-md-6 col-sm-6">
                    <input  id="name" class="form-control" name="document_number" type="text" /></div>
                  </div>
                  <div class="field item form-group">
                    <label class="col-form-label col-md-3 col-sm-3  label-align">Custodian Property<span class="required"></span></label>
                    <div class="col-md-6 col-sm-6">
                      <input  id="name" class="form-control" name="custodian_property" type="text" /></div>
                    </div>
                    <div class="field item form-group">
                      <label  class="col-form-label col-md-3 col-sm-3  label-align">Purchased Year<span class="required"></span></label>
                      <div class="col-md-6 col-sm-6">
                        <input   id="purchased" class="form-control"  name="year_of_purchased" type="text" /></div>
                      </div>
                      <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">Sold Year<span class="required"></span></label>
                        <div class="col-md-6 col-sm-6">
                          <input  id="sold_year" onchange="soldYear()" class="form-control" name="sold_year" type="text" /></div>
                        </div>
                        <div class="field item form-group">
                          <label class="col-form-label col-md-3 col-sm-3  label-align">Link1<span class="required"></span></label>
                          <div class="col-md-6 col-sm-6">
                            <input  id="name" class="form-control" name="link1" type="text" /></div>
                          </div>
                          <div class="field item form-group">
                            <label class="col-form-label col-md-3 col-sm-3  label-align">Link2<span class="required"></span></label>
                            <div class="col-md-6 col-sm-6">
                              <input  id="name" class="form-control" name="link2" type="text" /></div>
                            </div>
                            <div class="field item form-group">
                              <label class="col-form-label col-md-3 col-sm-3  label-align">Link3<span class="required"></span></label>
                              <div class="col-md-6 col-sm-6">
                                <input  id="name" class="form-control" name="link3" type="text" /></div>
                              </div>
                              <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Link4<span class="required"></span></label>
                                <div class="col-md-6 col-sm-6">
                                  <input  id="name" class="form-control" name="link4" type="text" /></div>
                                </div>
                              </div>
                              <div class="ln_solid">
                                <div class="form-group">
                                  <div class="offset-md-3">
                                    <button type='submit' onclick="myFunction()" class="btn btn-primary">Submit</button>
                                    <button type='reset' class="btn btn-success">Reset</button>
                                  </div>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /page content -->
                @endsection

                @section('script')

                <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
                <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
                <script>
                 $('.select2').select2();
               </script>
               <script>

                var n = 1;
                var doc_type = [];
                $(document).ready(function(){
               // initialize a validator instance from the "FormValidator" constructor.
                  // A "<form>" element is optionally passed as an argument, but is not a must
                  var validator = new FormValidator({
                    "events": ['blur', 'input', 'change']
                  }, document.forms[0]);
                  // on form "submit" event
                  document.forms[0].onsubmit = function(e) {
                    var submit = true,
                    validatorResult = validator.checkAll(this);
                    console.log(validatorResult);
                    return !!validatorResult.valid;
                  };
                  // on form "reset" event
                  document.forms[0].onreset = function(e) {
                    validator.reset();
                  };
                  // stuff related ONLY for this demo page:
                  $('.toggleValidationTooltips').change(function() {
                    validator.settings.alerts = !this.checked;
                    if (this.checked)
                      $('form .alert').remove();
                  }).prop('checked', false);

                  
                });

                $(document).ready(function() {
                  $('select[name="village_id"]').on('change', function() {
                    var village_id = $(this).val();
                    if(village_id) {
                      $.ajax({
                        url: '{{url("admin/properties-details-sector")}}/'+village_id,
                        type: "GET",
                        dataType: "json",
                        success:function(data) {
                          $('select[name="sector_id"]').empty();
                          $('select[name="sector_id"]').append('<option  value=""> Select </option>');
                          for (var i = 0; i < data.length; ++i) {
                            $('select[name="sector_id"]').append('<option  value="'+ data[i].id +'">'+ data[i].name +'</option>');

                          }
                        // $('select[name="subcategory_id"]').empty();
                        // $.each(data, function(key, value) {
                        //     $('select[name="subcategory_id"]').append('<option value="'+ data[key].id +'">'+ value +'</option>');
                        // });


                      }
                    });
                    }else{
                      $('select[name="sector_id"]').empty();
                    }
                  });
                });

                $(document).ready(function() {
                  $('select[name="sector_id"]').on('click', function() {
                    var sector_id = $(this).val();
                    if(sector_id) {
                      $.ajax({
                        url: '{{URL::to("admin/properties-details-subsector")}}/'+sector_id,
                        type: "GET",
                        dataType: "json",
                        success:function(data) {
                          $('select[name="subsector_id"]').empty();
                          $('select[name="subsector_id"]').append('<option value=""> Select </option>');
                          for (var i = 0; i < data.length; ++i) {
                            $('select[name="subsector_id"]').append('<option value="'+ data[i].id +'">'+ data[i].name +'</option>');

                          }
                        // $('select[name="subcategory_id"]').empty();
                        // $.each(data, function(key, value) {
                        //     $('select[name="subcategory_id"]').append('<option value="'+ data[key].id +'">'+ value +'</option>');
                        // });


                      }
                    });
                    }else{
                      $('select[name="subsector_id"]').empty();
                    }
                  });
                });

                $(document).ready(function() {
                  $('select[name="subsector_id"]').on('click', function() {
                    var subsector_id = $(this).val();
                    if(subsector_id) {
                      $.ajax({
                        url: '{{URL::to("admin/properties-details-propertytype")}}/'+subsector_id,
                        type: "GET",
                        dataType: "json",
                        success:function(data) {
                          $('select[name="property_type_id"]').empty();
                          $('select[name="property_type_id"]').append('<option value=""> Select </option>');

                          for (var i = 0; i < data.length; ++i) {
                            $('select[name="property_type_id"]').append('<option value="'+ data[i].id +'">'+ data[i].name +'</option>');

                          }
                        // $('select[name="subcategory_id"]').empty();
                        // $.each(data, function(key, value) {
                        //     $('select[name="subcategory_id"]').append('<option value="'+ data[key].id +'">'+ value +'</option>');
                        // });


                      }
                    });
                    }else{
                      $('select[name="property_type_id"]').empty();
                    }
                  });
                });


                var documentDetailsField = `<div class="row">
                <label class="col-form-label col-md-3 col-sm-3  label-align">Upload Documnet<span class="required">*</span></label>
                <div class="col-md-3 col-sm-3">
                  <label >
                    Document Type
                  </label>
                </div>
                <div class="col-md-3 col-sm-3">
                  <label>
                    Documnet
                  </label>
                </div>
              </div>
              <div class="row">
                <label class="col-form-label col-md-3 col-sm-3  label-align">
                  <p id="addRow" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i></p>
                </label>

                <div class="col-md-3 col-sm-3">
                  <select  class="form-control checkdocument" name="document_type[]"  required="required">
                  <option value="">Select Document</option>
                    <?php foreach($document as $doc){?>
                      <option value="<?php echo $doc->name;?>"><?php echo $doc->name;?></option>
                    <?php }?>
                  </select> 
                </div>
                <div class="col-md-3 col-sm-3">
                  <input type="file" class="form-control" name="document[]"  id="myid" required="required">       
                </div>
              </div>
              <div id="document">
              </div>
 
                `;
                
                $(document).ready(function(){
                 $('input[name="is_document"]').on('change', function() {
                  if($(this).val() == 1){
                    $('#documentDetails').append(documentDetailsField);
                    
                    var max = <?php echo $no_of_document; ?>;
                    $('#addRow').on('click',function(){
                      if(n < max){
                         $('#document').append('<div class="row" id="inputFormRow"><label class="col-form-label col-md-3 col-sm-3  label-align"><p id="removeRow" type="button" class="btn btn-danger"><i class="fa fa-trash"></i></p></label><div id="delDoc" class="col-md-3 col-sm-3"><select  class="form-control descendants" name="document_type[]" id="documentcheck"  required="required"><option value="">Select Document</option><?php foreach($document as $doc){?>
                      <option value="<?php echo $doc->name;?>"><?php echo $doc->name;?></option><?php }?></select> </div><div class="col-md-3 col-sm-3"><input type="file" class="form-control" name="document[]"  id="myid" required></div></div>');

                         n = n +1;

                      }else{
                        alert("Maximum Upload doumnt Limit Full");
                      }
                      
                      
                    });
                  }else{
                    $('#documentDetails').empty();
                    doc_type = [];
                    n=1;
                  }
                })
               });

                $(document).on('click', '#removeRow', function () {
                  
                 var my = $(this).parent().siblings('#delDoc').children().val();
                 doc_type.pop(my);
                 
                  $(this).closest('#inputFormRow').remove();
                  n = n -1;

                });

                function soldYear(){
                 var purchase = $('#purchased').val();
                 var sold = $('#sold_year').val();
                 if(purchase>sold){
                  alert('Sold Year Must Be grater Than purchased Year ');
                  $('#purchased').val('');
                  $('#sold_year').val('');
                }
              }
              $('form').on('submit', function(e){$(this).validate();return false;});
              // function myFunction() {
              //   var x = document.getElementById("radioPrimary31");
              //   x.checked = true;
              // }
                
                $(document).ready(function(){
                  $(document).on('change', '.checkdocument',function(){
                        if(jQuery.inArray($(this).val(), doc_type) !== -1){
                        $(this).val(''); 
                       
                     }else{
                      doc_type.push($(this).val());
                       $(this).attr('readonly', 'readonly');
                       $(this).children('option').hide();
                       
                     }
                  });
                });
                $(document).on('change','#documentcheck',function(){
                        
                      if(jQuery.inArray($(this).val(), doc_type) !== -1){
                        $(this).val(''); 
                       
                     }else{
                      doc_type.push($(this).val());
                       $(this).attr('readonly', 'readonly');
                       $(this).children('option').hide();
                     }
                        
                      
                    });
              
                
              
            </script>
            @endsection
@extends('admin.master')
@section('title', 'PropertyDetails show | Admin')
@section('content')
<!-- page content -->

<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Show <small>PropertyDetails</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <a href="{{url('admin/properties-details-list')}}" class="btn btn-sm bg-green">
               <i class="fa fa-reply" aria-hidden="true"></i>
             </a>

           </ul>
           <div class="clearfix"></div>
         </div>
         @include('flash-message')
         <div class="x_content">
          <form class="" action="{{url('admin/properties-details-update',$property_details->id)}}" method="post" novalidate enctype="multipart/form-data">
            @csrf
            <!-- <span class="section">Show PropertyDetails</span> -->

           <div class="field item form-group">
              <label class="col-form-label col-md-3 col-sm-3  label-align">Select Village<span class="required">*</span></label>
              <div class="col-md-6 col-sm-6">
               <select class="form-control select2"  disabled name="village_id"  required="required">
                <option value="">Select</option>
                @foreach($village as $cat_list)
                <option  value="{{$cat_list->id}}">{{$cat_list->name}}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="field item form-group">
            <label class="col-form-label col-md-3 col-sm-3  label-align">Select SubSector<span class="required">*</span></label>
            <div class="col-md-6 col-sm-6">
              <select class="form-control select2" disabled name="subsector_id"  id="sector_id">
                <option value="">Select</option> 
                @foreach($subsector as $sub_list)
                <option value="{{$sub_list->id}}">{{$sub_list->name}}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="field item form-group">
            <label class="col-form-label col-md-3 col-sm-3  label-align">Select Sector<span class="required">*</span></label>
            <div class="col-md-6 col-sm-6">
              <select class="form-control select2" disabled name="sector_id" required="required" id="sector_id">
                <option value="">Select</option> 

              </select>
            </div>
          </div>

          <div class="field item form-group">
            <label class="col-form-label col-md-3 col-sm-3  label-align">Select PropertyType<span class="required">*</span></label>
            <div class="col-md-6 col-sm-6">
              <select class="form-control select2" disabled name="property_type_id" required="required" id="property_type_id">
                <option value="">Select</option> 

              </select>
            </div>
          </div>
          <div class="field item form-group">
            <label class="col-form-label col-md-3 col-sm-3  label-align">Property Name<span class="required">*</span></label>
            <div class="col-md-6 col-sm-6">
              <input  id="name" class="form-control" disabled name="name" value="{{$property_details->name}}"  required="required" type="text" /></div>
            </div>
            <div class="field item form-group">
              <label class="col-form-label col-md-3 col-sm-3  label-align">Mobile Number<span class="required">*</span></label>
              <div class="col-md-6 col-sm-6">
                <input  id="mobile" class="form-control" disabled  name="contact" value="{{$property_details->contact}}"  required="required" type="number" /></div>
              </div>

              <div class="field item form-group">
                <label class="col-form-label col-md-3 col-sm-3  label-align">Address<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6">
                  <textarea required="required" disabled name='address' value="{{$property_details->address}}">{{$property_details->address}}</textarea></div>
                </div>
                <div class="field item form-group">
                  <label class="col-form-label col-md-3 col-sm-3  label-align">Status<span class="required">*</span></label>
                  <div class="col-md-6 col-sm-6">
                   <select selected class="form-control" disabled name="status" required="required">
                    <option  value+="">Select Status</option>
                    <option value="1" @if ($property_details->status=='1') selected @endif>Available</option>
                    <option value="0" @if ($property_details->status=='0') selected @endif>NotAvailable</option>
                  </select>    
                </div>
              </div>
              <div class="row add-field">
                <label class="col-form-label col-md-3 col-sm-3  label-align ">Select Documented/ Non-Documented<span class="required">*</span></label>
                <div class="col-md-3 col-sm-3">
                  <input type="radio" id="radioPrimary3" value="1" disabled name="is_document" required="required" @if ($property_details->is_document=='1') checked @endif >
                  <label for="radioPrimary3">
                    Document
                  </label>
                </div>
                <div class="col-md-3 col-sm-3">
                  <input type="radio" id="radioPrimary3" value="0" disabled name="is_document" required="required" @if ($property_details->is_document=='0') checked @endif>
                  <label for="radioPrimary3">
                    Non Document
                  </label>
                </div>
              </div>
              <div class="row">
                <label class="col-form-label col-md-3 col-sm-3  label-align">Select Litigated/ Non-Litigated<span class="required">*</span></label>
                <div class="col-md-3 col-sm-3">
                  <input type="radio" id="radioPrimary3" value="1" disabled name="is_litigated" required="required" @if ($property_details->is_litigated=='1') checked @endif >
                  <label for="radioPrimary3">
                    Litigated
                  </label>
                </div>
                <div class="col-md-3 col-sm-3">
                  <input type="radio" id="radioPrimary3" value="0" disabled name="is_litigated" required="required" @if ($property_details->is_litigated=='0') checked @endif >
                  <label for="radioPrimary3">
                    Non Litigated
                  </label>
                </div>
              </div>

              <div id="documentDetails">
              </div>

            </div>


            <div class="ln_solid">
              <div class="form-group">
                <div class="offset-md-3">
                 <!--  <button type='submit' disabled class="btn btn-primary">Submit</button>
                  <button type='reset' disabled class="btn btn-success">Reset</button> -->
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- /page content -->
@endsection

@section('script')

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
 $('.select2').select2();
</script>
<script>


  $(document).ready(function(){
     // initialize a validator instance from the "FormValidator" constructor.
        // A "<form>" element is optionally passed as an argument, but is not a must
        var validator = new FormValidator({
          "events": ['blur', 'input', 'change']
        }, document.forms[0]);
        // on form "submit" event
        document.forms[0].onsubmit = function(e) {
          var submit = true,
          validatorResult = validator.checkAll(this);
          console.log(validatorResult);
          return !!validatorResult.valid;
        };
        // on form "reset" event
        document.forms[0].onreset = function(e) {
          validator.reset();
        };
        // stuff related ONLY for this demo page:
        $('.toggleValidationTooltips').change(function() {
          validator.settings.alerts = !this.checked;
          if (this.checked)
            $('form .alert').remove();
        }).prop('checked', false);

        
      });

  $(document).ready(function() {
    $('select[name="village_id"]').on('change', function() {
      var village_id = $(this).val();
      if(village_id) {
        $.ajax({
          url: '{{url("admin/properties-details-sector")}}/'+village_id,
          type: "GET",
          dataType: "json",
          success:function(data) {
            $('select[name="sector_id"]').empty();
            for (var i = 0; i < data.length; ++i) {
              $('select[name="sector_id"]').append('<option value="'+ data[i].id +'">'+ data[i].name +'</option>').val("option:selected");

            }
                        // $('select[name="subcategory_id"]').empty();
                        // $.each(data, function(key, value) {
                        //     $('select[name="subcategory_id"]').append('<option value="'+ data[key].id +'">'+ value +'</option>');
                        // });


                      }
                    });
      }else{
        $('select[name="sector_id"]').empty();
      }
    });
  });

  $(document).ready(function() {
    $('select[name="village_id"]').on('change', function() {
      var sector_id = $(this).val();
      if(sector_id) {
        $.ajax({
          url: '{{URL::to("admin/properties-details-propertytype")}}/'+sector_id,
          type: "GET",
          dataType: "json",
          success:function(data) {
            $('select[name="property_type_id"]').empty();
            for (var i = 0; i < data.length; ++i) {
              $('select[name="property_type_id"]').append('<option value="'+ data[i].id +'">'+ data[i].name +' </option>');

            }
                        // $('select[name="subcategory_id"]').empty();
                        // $.each(data, function(key, value) {
                        //     $('select[name="subcategory_id"]').append('<option value="'+ data[key].id +'">'+ value +'</option>');
                        // });


                      }
                    });
      }else{
        $('select[name="property_type_id"]').empty();
      }
    });
  });


var documentDetailsField = `<div id="docDetetail">
                 <div class="field item form-group">
                  <label class="col-form-label col-md-3 col-sm-3  label-align">Document Type<span class="required">*</span></label>
                  <div class="col-md-6 col-sm-6">
                   <select class="form-control" name="document_type" required="required">
                    <option value="">Select Document</option>
                    <option value="pancard">pancard</option>
                    <option value="adharcard">Adhar card</option>
                    <option value="property_documents">PropertyDocuments</option>
                  </select>    
                </div>
              </div>
              <div class="field item form-group">
                  <label class="col-form-label col-md-3 col-sm-3  label-align">Document(mimes:jpeg,jpg,bmp,png,gif)<span class="required">*</span></label>
                  <div class="col-md-6 col-sm-6">
                    <input type="file" class="form-control" name="document" required>    
                </div>
              </div>
              </div>`;

  $(document).ready(function(){
   $('input[name="is_document"]').on('change', function() {
    if($(this).val() == 1){
      $('#documentDetails').append(documentDetailsField);
    }else{
      $('#docDetetail').remove();
    }
  })
 });
</script>
@endsection
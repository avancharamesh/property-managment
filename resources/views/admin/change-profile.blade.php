@extends('admin.master')
@section('title', 'Profile | Admin')
@section('content')
<!-- page content -->
<div class="right_col" role="main">
   <div class="col-md-12 col-sm-12">
   <div class="x_panel">
                                <div class="x_title">
                                    <h2>Edit <small>Profile</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <a href="{{url('admin/dashboard')}}" class="btn btn-sm bg-green">
                                             <i class="fa fa-reply" aria-hidden="true"></i>
                                        </a>
                                        
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                @include('flash-message')
                        
                                <form action="{{url('admin/change-profile')}}" method="post" enctype="multipart/form-data" novalidate>
                                    @csrf
                        
                                        <div class="field item form-group">
                                            <label class="col-form-label col-md-3 col-sm-3  label-align">Name<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6">
                                                <input value="{{ Auth::user()->name }}" class="form-control" data-validate-length-range="3"  name="name"  required="required" />
                                            </div>
                                        </div>
                            
                                        <div class="field item form-group">
                                            <label class="col-form-label col-md-3 col-sm-3  label-align">email<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6">
                                                <input value="{{ Auth::user()->email }}" class="form-control" name="email" class='email' required="required" type="email" /></div>
                                        </div>
                                        <div class="field item form-group">
                                            <label class="col-form-label col-md-3 col-sm-3  label-align">Profile Image (jpeg,bmp,png,gif)<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6">
                                                <input class="form-control" name="profile_image"  type="file"   onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])" />
                                            </div>
                                        </div>
                                        <div class="field item form-group">
                                            <label class="col-form-label col-md-3 col-sm-3  label-align">Image Preview<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6">
                                                @if(Auth::user()->profile_image)
                                                  <img src="{{asset('uploads/user-profile')}}/{{Auth::user()->profile_image}}" alt="Preview" width="100px" height="150px" id="blah">
                                                @else
                                                  <img src="{{asset('assets/production/images/avatar-default-icon.png')}}" alt="Preview" width="100px" height="150px" id="blah">
                                                @endif
                                            </div>
                                        </div>
                                        
                                        
                                        <div class="ln_solid">
                                            <div class="form-group">
                                                <div class="offset-md-3">
                                                    <button type='submit' class="btn btn-primary">Submit</button>
                                                    <button type='reset' class="btn btn-success">Reset</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>                      
   </div>
</div>
            <!-- /page content -->
@endsection

@section('script')

<script>
    function hideshow(){
			var password = document.getElementById("password1");
			var slash = document.getElementById("slash");
			var eye = document.getElementById("eye");
			
			if(password.type === 'password'){
				password.type = "text";
				slash.style.display = "block";
				eye.style.display = "none";
			}
			else{
				password.type = "password";
				slash.style.display = "none";
				eye.style.display = "block";
			}

		}
  $(document).ready(function(){
     // initialize a validator instance from the "FormValidator" constructor.
        // A "<form>" element is optionally passed as an argument, but is not a must
        var validator = new FormValidator({
            "events": ['blur', 'input', 'change']
        }, document.forms[0]);
        // on form "submit" event
        document.forms[0].onsubmit = function(e) {
            var submit = true,
                validatorResult = validator.checkAll(this);
            console.log(validatorResult);
            return !!validatorResult.valid;
        };
        // on form "reset" event
        document.forms[0].onreset = function(e) {
            validator.reset();
        };
        // stuff related ONLY for this demo page:
        $('.toggleValidationTooltips').change(function() {
            validator.settings.alerts = !this.checked;
            if (this.checked)
                $('form .alert').remove();
        }).prop('checked', false);
  });
</script>
@endsection
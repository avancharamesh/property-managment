@extends('admin.master')
@section('title', 'Dashboard | Admin')
@section('content')
<!-- page content -->
<style type="text/css">
 .tile_count .tile_stats_count {
  padding: 0 20px 0 80px;
  padding-top: 0px;
  padding-right: 20px;
  padding-bottom: 0px;
  padding-left: 80px;
  text-overflow: ellipsis;
  white-space: nowrap;
  position: relative;
}
.col-md-3 {
  -ms-flex: 0 0 25%;
  flex: 0 0 25%;
  max-width: 20%;
}
.col, .col-1, .col-10, .col-11, .col-12, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-auto, .col-lg, .col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-auto, .col-md, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-3, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-auto, .col-sm, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-auto, .col-xl, .col-xl-1, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-auto {
  position: relative;
  width: 50%;
  padding-right: 1px;
  padding-left: 1px;
}


</style>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<div class="right_col" role="main">
  <!-- top tiles -->
  <div class="row" style="display: inline-block;" >
    <div class="tile_count">
               @php
            $status = ''
            @endphp
              <a href="{{ URL::to('admin/properties-details-list',$status) }}" >
                <div id="total_properties"  class="col-md-3 col-sm-3  tile_stats_count  bg-success text-white p-2 ml-5 mr-5">
                <span  class="count_top"><i class="fa fa-file"></i> Total Properties</span>
                <div class="count">{{$dashboard[0]->total_properties}}</div>
                <!-- <span class="count_bottom"><i class="green">4% </i> From last Week</span>-->
              </div>
              @php
            $status = 2
            @endphp
              </a>
              <a href="{{ URL::to('admin/properties-details-list',$status) }}">
              <div class="col-md-3 col-sm-3  tile_stats_count bg-primary text-white p-2 ml-5 mr-5">
                <span class="count_top"><i class="fa fa-file"></i> Fully Documented</span>
                <div class="count">{{$dashboard[0]->document_count}}</div>
                <!-- <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>3% </i> From last Week</span> -->
              </div>
               </a>
                @php
               $status = 1
               @endphp
               <a href="{{ URL::to('admin/properties-details-list',$status) }}">
              <div class="col-md-3 col-sm-3  tile_stats_count bg-info text-white p-2 ml-5 mr-5">
                <span class="count_top"><i class="fa fa-file"></i> Partially Documented</span>
                <div class="count">{{$dashboard[0]->partail_document_count}}</div>
                <!-- <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span> -->
              </div>
              </a>
              @php
               $status = 0
               @endphp
               <a href="{{ URL::to('admin/properties-details-list',$status) }}">
               <div class="col-md-3 col-sm-4  tile_stats_count bg-danger text-white p-2 m-5">
                <span class="count_top"><i class="fa fa-file"></i> Non Documented</span>
                <div class="count">{{$dashboard[0]->non_document_count}}</div>
                <!-- <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span> -->
              </div>
              </a>
              @php
              $status = 3
              @endphp
              <a href="{{ URL::to('admin/properties-details-list',$status) }}">
              <div class="col-md-3 col-sm-3  tile_stats_count bg-warning text-white p-2 m-5">
                <span class="count_top"><i class="fa fa-file"></i> Litigated</span>
                <div class="count">{{$dashboard[0]->litigate_count}}</div>
                <!-- <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i>12% </i> From last Week</span> -->
              </div>
               </a>
               @php
               $status = 4
               @endphp
               <a href="{{ URL::to('admin/properties-details-list',$status) }}">
               <div class="col-md-3 col-sm-3  tile_stats_count bg-dark text-white p-2 m-5">
                <span class="count_top"><i class="fa fa-file"></i> Non Litigated</span>
                <div class="count">{{$dashboard[0]->nonlitigate_count}}</div>
                <!--  <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span> -->
              </div>
              </a>
             
              <div class="col-lg-5 m-2" style="position: relative;
              color: #A29896;
              font-size: 70px;
              margin: 100px auto;
              page-break-after: always;"  id="chart">
            </div>
            <div class="col-lg-5 m-2" style="position: relative;
            color: #A29896;
            font-size: 70px;
            margin: 100px auto;
            page-break-after: always;" 
            id="piechart"></div>
          </div>

        </div>


        <!-- /top tiles -->


        <br />
      </div>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
      <!-- /page content -->
      <script type="text/javascript">
// Load google charts
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
// Draw the chart and set the chart values
function drawChart() {
  var data = google.visualization.arrayToDataTable([
['Task', 'Hours per Day'],
  <?php echo $chartData;?>
  ]);

  // Optional; add a title and set the width and height of the chart
  var options = {'title':'Properties Chart','is3D':true, 'width':600, 'height':400, 'scrollX':true};

  // Display the chart inside the <div> element with id="piechart"
  var mychart = new google.visualization.PieChart(document.getElementById('piechart'));
  mychart.draw(data, options);
}
</script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(chart);

// Draw the chart and set the chart values
function chart() {
  var mydata = google.visualization.arrayToDataTable([
    ['Task', 'Hours per Day'],
    <?php echo $mychartData;?>
    ]);

  // Optional; add a title and set the width and height of the chart
  var myoptions = {'title':'Properties Chart','is3D':true, 'width':600, 'height':400,'scrollX':true};

  // Display the chart inside the <div> element with id="piechart"
  var mychart = new google.visualization.PieChart(document.getElementById('chart'));
  mychart.draw(mydata, myoptions);
}
</script>
<!-- <script type="text/javascript">
  $(document).ready(function(){

    $('#total_properties').on('click',function(){
      $.ajax({
           type:'GET',
           url:"{{ URL::to('admin/properties-details-list') }}",
           
          
        });
    });
  });
</script> -->
@endsection
